package cn.syqcode.freshstore.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BeanOrder {
    public static final String[] tableTitles={"订单编码","用户编码","原始金额","结算金额","使用优惠券编号","要求送达时间","配送地址编号","订单状态"};

    private static final long serialVersionUID = 1L;
    private String ord_id;
    private String user_id;
    private double ord_oldmoney;
    private double ord_newmoney;
    private String coupon_id;
    private long ord_time;
    private String addr_id;
    private String ord_status;

    public String getOrd_id() {
        return ord_id;
    }

    public void setOrd_id(String ord_id) {
        this.ord_id = ord_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public double getOrd_oldmoney() {
        return ord_oldmoney;
    }

    public void setOrd_oldmoney(double ord_oldmoney) {
        this.ord_oldmoney = ord_oldmoney;
    }

    public double getOrd_newmoney() {
        return ord_newmoney;
    }

    public void setOrd_newmoney(double ord_newmoney) {
        this.ord_newmoney = ord_newmoney;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public long getOrd_time() {
        return ord_time;
    }

    public void setOrd_time(long ord_time) {
        this.ord_time = ord_time;
    }

    public String getAddr_id() {
        return addr_id;
    }

    public void setAddr_id(String addr_id) {
        this.addr_id = addr_id;
    }

    public String getOrd_status() {
        return ord_status;
    }

    public void setOrd_status(String ord_status) {
        this.ord_status = ord_status;
    }

    public String getCell(int col){
        if(col==0) return ord_id;
        else if(col==1) return user_id;
        else if(col==2) return String.valueOf(ord_oldmoney);
        else if(col==3) return String.valueOf(ord_newmoney);
        else if (col==4) return coupon_id;
        else if (col==5){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(ord_time);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else if (col==6) return addr_id;
        else if (col==7) return ord_status;
        else return "";
    }
}
