package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.model.OrderDetail;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserTakeOrderManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
   // private JButton btnSearch = new JButton("查询");
    private JButton btnModf = new JButton("修改");
    private JButton btnDel = new JButton("删除");
    private JButton btnBuy = new JButton("下单");
    private JButton btnNew = new JButton("刷新");

 //   private JTextField edtSearch = new JTextField(15);

    private Object tblTitle[]=new OrderDetail().tableTitles;
    private Object tblData[][];

    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable orderTable=new JTable(tablmod);
    List<OrderDetail> orderDetails=null;

    private void reloadOrderDetailTable() throws BaseException {
        List<String> ord_id=FreshStoreUtil.orderManager.searchOrderIdByUserId("未下单",BeanUser.currentLoginUser.getUser_id());
        if (ord_id.size()==0) {tblData =new Object[0][this.tblTitle.length];}
        else {
            orderDetails=FreshStoreUtil.orderManager.showOrderDetail(ord_id.get(0));
            tblData =new Object[orderDetails.size()][this.tblTitle.length];
            for(int i=0;i<orderDetails.size();i++){
                for (int j=0;j<this.tblTitle.length;j++) {
                    tblData[i][j] = orderDetails.get(i).getCell(j);
                }
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.orderTable.validate();
        this.orderTable.repaint();
    }

    private final JScrollPane scroll = new JScrollPane();

    public FrmUserTakeOrderManager(Frame f, String s, boolean b) throws BaseException{
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
       // edtSearch.setText("<-请输入订单编号->");
       // toolBar.add(edtSearch);
       // toolBar.add(btnSearch);
        toolBar.add(btnModf);
        toolBar.add(btnDel);
        toolBar.add(btnBuy);
        toolBar.add(btnNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadOrderDetailTable();
        this.getContentPane().add(new JScrollPane(this.orderTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        //this.btnSearch.addActionListener(this);
        this.btnModf.addActionListener(this);
        this.btnDel.addActionListener(this);
        this.btnBuy.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
//        if (e.getSource()==this.btnSearch){
//
//        }
        if (e.getSource()==this.btnModf){
            int i=this.orderTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FrmUserGoodsManager_ModfGoods dlg=new FrmUserGoodsManager_ModfGoods(this,"修改数量",true,Integer.parseInt(orderTable.getValueAt(i, 0).toString()));
                dlg.setVisible(true);
                this.reloadOrderDetailTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnDel){
            int i=this.orderTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FreshStoreUtil.orderDetailManager.delOrderDetail(Integer.parseInt(orderTable.getValueAt(i, 0).toString()));
                this.reloadOrderDetailTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnBuy){
            List<BeanOrder> orders= null;
            try {
                orders = FreshStoreUtil.orderManager.loadByStatus("未下单");
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            if (orders.size()==0){
                return;
            }
            BeanOrder b=orders.get(0);
            try {
                //FreshStoreUtil.orderManager.buyOrder(b);
                FrmUserTakeOrderManager_buy dlg=new FrmUserTakeOrderManager_buy(this,"商品清单",true,b);
                dlg.setVisible(true);
                //JOptionPane.showMessageDialog(null,  "已完成下单","提示",JOptionPane.INFORMATION_MESSAGE);
                this.reloadOrderDetailTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadOrderDetailTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
