package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.Evaluation;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IEvaluationManager {
    public List<Evaluation> loadAll() throws BaseException;

    public void addEvaluation(Evaluation evaluation) throws BaseException;

    public void delEvaluation(int no) throws BaseException;

    public List<Evaluation> loadByGoods(String goods_id) throws BaseException;

    public List<Evaluation> loadByUserId(String userid) throws BaseException;
}
