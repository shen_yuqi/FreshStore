package cn.syqcode.freshstore.model;

public class BeanCut {
    private double cut1;
    private double cut2;
    private double cut3;
    private double cut4;
    private double cut;

    public double getCut1() {
        return cut1;
    }

    public void setCut1(double cut1) {
        this.cut1 = cut1;
    }

    public double getCut2() {
        return cut2;
    }

    public void setCut2(double cut2) {
        this.cut2 = cut2;
    }

    public double getCut3() {
        return cut3;
    }

    public void setCut3(double cut3) {
        this.cut3 = cut3;
    }

    public double getCut4() {
        return cut4;
    }

    public void setCut4(double cut4) {
        this.cut4 = cut4;
    }

    public double getCut() {
        return cut;
    }

    public void setCut(double cut) {
        this.cut = cut;
    }
}
