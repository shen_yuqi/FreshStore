package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.MenuFormManager;
import cn.syqcode.freshstore.model.MenuForm;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmMenuManager_AddForm_Modf extends JDialog implements ActionListener {
    private MenuForm menuForm=null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelGoodsId = new JLabel("商品编码：");
    private JLabel lableDesc = new JLabel("描述: ");

    private JTextField edtGoodsId= new JTextField(22);
    private JTextArea edtDesc = new JTextArea(7,30);

    public FrmMenuManager_AddForm_Modf(JDialog f, String s, boolean b,int no) throws BaseException {
        super(f, s, b);
        //滚动条自动出现
        JScrollPane scroll = new JScrollPane(edtDesc);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        //自动换行
        edtDesc.setLineWrap(true);
        MenuForm menuForm = (new MenuFormManager()).searchByNo(no);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelGoodsId);
        edtGoodsId.setText(menuForm.getGoods_id());
        workPane.add(edtGoodsId);
        edtDesc.setText(menuForm.getRemarks());
        workPane.add(lableDesc);
        workPane.add(scroll);

        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(320, 260);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            menuForm=new MenuForm();
            String id = this.edtGoodsId.getText();
            String desc = this.edtDesc.getText();
            menuForm.setMenu_id(id);
            menuForm.setGoods_id(id);
            menuForm.setRemarks(desc);

            try {
                (new MenuFormManager()).addMenuForm(menuForm);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.menuForm=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public MenuForm getMenuForm() {
        return menuForm;
    }
}
