package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface ICouponManager {
    public List<BeanCoupon> loadAll() throws BaseException;

    public void addCoupon(BeanCoupon beanCoupon) throws BaseException;

    public void ModfCoupon(BeanCoupon beanCoupon) throws BaseException;

    public void delCoupon(int id) throws BaseException;

    public List<BeanCoupon> loadAllGroupId() throws BaseException;
}
