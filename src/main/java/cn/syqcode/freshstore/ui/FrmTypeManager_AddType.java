package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmTypeManager_AddType extends JDialog implements ActionListener  {
    private BeanFreshType type=null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelName = new JLabel("类别名称：");
    private JLabel lableDesc = new JLabel("类别描述: ");

    private JTextField edtName = new JTextField(22);
    private JTextArea edtDesc = new JTextArea(7,30);

    public FrmTypeManager_AddType(JDialog f, String s, boolean b) {
        super(f, s, b);
        //滚动条自动出现
        JScrollPane scroll = new JScrollPane(edtDesc);
        //scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        //自动换行
        edtDesc.setLineWrap(true);

        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelName);
        workPane.add(edtName);
        workPane.add(lableDesc);
        ///workPane.add(edtDesc);
        workPane.add(scroll);

        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(320, 260);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            type=new BeanFreshType();
            String name = this.edtName.getText();
            String desc = this.edtDesc.getText();

            type.setType_name(name);
            type.setType_desc(desc);

            try {
                (new FreshTypeManager()).addFreshType(type);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.type=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanFreshType getFreshType() {
        return type;
    }
}
