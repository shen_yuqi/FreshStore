package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.CouponManager;
import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.model.Discount;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FrmDiscountManager_Add extends JDialog implements ActionListener {
    private BeanDiscount discount=null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelContent = new JLabel("内容：");
    private JLabel lableSum = new JLabel("适用商品数量：");
    private JLabel labelCut = new JLabel("折扣：");
    private JLabel labelStart = new JLabel("开始日期：");
    private JLabel labelEnd = new JLabel("结束日期:");

    private JTextField edtContent = new JTextField(20);
    private JTextField edtSum = new JTextField(20);
    private JTextField edtCut = new JTextField(20);
    private JTextField edtStart = new JTextField(20);
    private JTextField edtEnd = new JTextField(20);

    public FrmDiscountManager_Add(JDialog f, String s, boolean b) {
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelContent);
        workPane.add(edtContent);
        workPane.add(lableSum);
        workPane.add(edtSum);
        workPane.add(labelCut);
        workPane.add(edtCut);
        workPane.add(labelStart);
        workPane.add(edtStart);
        workPane.add(labelEnd);
        workPane.add(edtEnd);

        workPane.setLayout(null);

        labelContent.setLocation(20, 20); edtContent.setLocation(120,20);
        lableSum.setLocation(20, 40); edtSum.setLocation(120,40);
        labelCut.setLocation(20, 60); edtCut.setLocation(120,60);
        labelStart.setLocation(20, 80); edtStart.setLocation(120,80);
        labelEnd.setLocation(20,100);edtEnd.setLocation(120,100);

        labelContent.setSize(100, 14); edtContent.setSize(200,20);
        lableSum.setSize(100, 14); edtSum.setSize(200,20);
        labelCut.setSize(100, 14);edtCut.setSize(200,20);
        labelStart.setSize(100, 14);edtStart.setSize(200,20);
        labelEnd.setSize(100,14);edtEnd.setSize(200,20);
        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(350, 270);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            discount=new BeanDiscount();
            String content = this.edtContent.getText();
            double sum=0;
            if (!"".equals(this.edtSum.getText())){
                sum = Double.parseDouble(this.edtSum.getText());
            }
            double cut=0;
            if(!"".equals(this.edtCut.getText())){
                cut = Double.parseDouble(this.edtCut.getText());
            }
            long start=0;
            if (!"".equals(this.edtStart.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtStart.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                start = ts;
            }
            long end = 0;
            if (!"".equals(this.edtEnd.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtEnd.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                end = ts;
            }

            discount.setDiscount_content(content);
            discount.setDiscount_limits(sum);
            discount.setDiscount_detail(cut);
            discount.setDiscount_startdate(start);
            discount.setDiscount_enddate(end);
            try {
                FreshStoreUtil.discountManager.addDiscount(discount);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.discount=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanDiscount getDiscount(){return discount;}
}
