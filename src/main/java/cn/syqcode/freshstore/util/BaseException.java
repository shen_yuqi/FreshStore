package cn.syqcode.freshstore.util;

public class BaseException  extends Exception {
    public BaseException(String msg){
        super(msg);
    }
}
