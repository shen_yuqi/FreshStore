package cn.syqcode.freshstore;

import cn.syqcode.freshstore.control.*;
import cn.syqcode.freshstore.itf.*;

public class FreshStoreUtil {
    public static IUserManager userManager=new UserManager();
    public static ISystemUserManager systemUserManager=new SystemUserManager();
    public static IDiscountManager discountManager=new DiscountManager();
    public static IDiscountGoodsManager discountGoodsManager=new DiscountGoodsManager();
    public static IGoodsManager goodsManager=new GoodsManager();
    public static ISalesProManager salesProManager=new SalesProManager();
    public static IMenuFormManager menuFormManager=new MenuFormManager();
    public static IMenuManager menuManager=new MenuManager();
    public static IOrderManager orderManager=new OrderManager();
    public static IUserOrderManager userOrderManager=new UserOrderManager();
    public static IOrderDetailManager orderDetailManager=new OrderDetailManager();
    public static IPurchaseDetail purchaseDetail=new PurchaseDetail();
    public static IUserAddrManager userAddrManager=new UserAddrManager();
    public static ICouponManager couponManager=new CouponManager();
    public static IEvaluationManager evaluationManager=new EvaluationManager();
    public static ICutManager cutManager=new CutManager();
}
