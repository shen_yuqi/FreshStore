package cn.syqcode.freshstore.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

//满折信息表
public class BeanDiscount {
    public static final String[] tableTitles={"满折编号","内容","适用商品数量","折扣","起始日期","结束日期"};
    private static final long serialVersionUID = 1L;
    private String discount_id;
    private String discount_content;
    private double discount_limits;
    private double discount_detail;
    private long discount_startdate;
    private long discount_enddate;

    public String getDiscount_id() {
        return discount_id;
    }

    public void setDiscount_id(String discount_id) {
        this.discount_id = discount_id;
    }

    public String getDiscount_content() {
        return discount_content;
    }

    public void setDiscount_content(String discount_content) {
        this.discount_content = discount_content;
    }

    public double getDiscount_limits() {
        return discount_limits;
    }

    public void setDiscount_limits(double discount_limits) {
        this.discount_limits = discount_limits;
    }

    public double getDiscount_detail() {
        return discount_detail;
    }

    public void setDiscount_detail(double discount_detail) {
        this.discount_detail = discount_detail;
    }

    public long getDiscount_startdate() {
        return discount_startdate;
    }

    public void setDiscount_startdate(long discount_startdate) {
        this.discount_startdate = discount_startdate;
    }

    public long getDiscount_enddate() {
        return discount_enddate;
    }

    public void setDiscount_enddate(long discount_enddate) {
        this.discount_enddate = discount_enddate;
    }

    public String getCell(int col){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(col==0) return discount_id;
        else if (col==1) return discount_content;
        else if(col==2) return String.valueOf(discount_limits);
        else if(col==3) return String.valueOf(discount_detail);
        else if (col==4) {
            Date date = new Date(discount_startdate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else if (col==5){
            Date date = new Date(discount_enddate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else return "";
    }

}
