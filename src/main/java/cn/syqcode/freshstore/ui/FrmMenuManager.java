package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.GoodsManager;
import cn.syqcode.freshstore.control.MenuFormManager;
import cn.syqcode.freshstore.control.MenuManager;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.model.MenuForm;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmMenuManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加");
    private JButton btnModf = new JButton("修改");
    private JButton btnDelete = new JButton("删除");
    private JButton btnSearch = new JButton("查询");
    private JButton btnAddForm = new JButton("菜单推荐表");
    private JButton btnNew = new JButton("刷新");

    private JTextField edtSearch = new JTextField(15);

    private Object tblTitle[]=new BeanMenu().tableTitles;
    private Object tblData[][];

    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable menusTable=new JTable(tablmod);
    List<BeanMenu> menus=null;

    private final JScrollPane scroll = new JScrollPane();

    private void reloadMenuTable() throws BaseException{
        List<BeanMenu> beanMenus=(new MenuManager()).loadAll();
        tblData =new Object[beanMenus.size()][5];
        for(int i=0;i<beanMenus.size();i++){
            for (int j=0;j<BeanMenu.tableTitles.length;j++){
                tblData[i][j]=beanMenus.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.menusTable.validate();
        this.menusTable.repaint();
    }
    private void reloadMenuTableByName(String name) throws BaseException{
        List<BeanMenu> beanMenus=(new MenuManager()).loadByMenuName(name);
        tblData =new Object[beanMenus.size()][5];
        for(int i=0;i<beanMenus.size();i++){
            for (int j=0;j<BeanMenu.tableTitles.length;j++){
                tblData[i][j]=beanMenus.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.menusTable.validate();
        this.menusTable.repaint();
    }

    public FrmMenuManager(Frame f, String s, boolean b) throws BaseException{
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnModf);
        toolBar.add(btnDelete);
        toolBar.add(edtSearch);
        toolBar.add(btnSearch);
        toolBar.add(btnAddForm);
        toolBar.add(btnNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadMenuTable();
        this.getContentPane().add(new JScrollPane(this.menusTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnModf.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.btnSearch.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.btnAddForm.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmMenuManager_Add dlg=new FrmMenuManager_Add(this,"添加菜谱",true);
            dlg.setVisible(true);
            if(dlg.getMenu()!=null){//刷新表格
                try {
                    this.reloadMenuTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnModf){
            FrmMenuManager_Modf dlg= null;
            int i=this.menusTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择需要修改的菜谱","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            String id=menusTable.getValueAt(i, 0).toString();
            try {
                dlg = new FrmMenuManager_Modf(this,"修改菜谱信息",true,id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
            if(dlg.getMenu()!=null){//刷新表格
                try {
                    this.reloadMenuTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnDelete){
            int i=this.menusTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择菜谱","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除该菜谱吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                List<BeanMenu> menus= null;
                String menu_id=this.menusTable.getValueAt(i,0).toString();
                try {
                    (new MenuManager()).delMenu(menu_id);
                    this.reloadMenuTable();
                } catch (BaseException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else if (e.getSource()==this.btnSearch){
            try {
                this.reloadMenuTableByName(this.edtSearch.getText());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnAddForm){
            FrmMenuManager_AddForm dlg= null;
            int i=this.menusTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择菜谱","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            String id=menusTable.getValueAt(i, 0).toString();
            try {
                dlg = new FrmMenuManager_AddForm(this,"添加推荐菜谱",true,id);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadMenuTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
