package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.OrderDetail;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FrmUserGoodsManager_ModfGoods extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("关闭");

    private JLabel labelSum = new JLabel("数量：");

    private JTextField edtSum = new JTextField(22);

    OrderDetail orderDetail=null;
    public FrmUserGoodsManager_ModfGoods(FrmUserTakeOrderManager f, String s, boolean b, int no) throws BaseException {
        super(f, s, b);
        orderDetail = FreshStoreUtil.orderDetailManager.searchOrderDetailByNo(no);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelSum);
        edtSum.setText(String.valueOf(orderDetail.getOrddetail_quantity()));
        workPane.add(edtSum);
        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(350, 150);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        btnOk.addActionListener(this);
        btnCancel.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btnCancel) {
            this.setVisible(false);
        }
        else if(e.getSource()==this.btnOk){
            try {
                FreshStoreUtil.orderDetailManager.ModfQuantity(orderDetail.getNo(),Double.parseDouble(edtSum.getText()));
                JOptionPane.showMessageDialog(null,  "修改成功","提示",JOptionPane.INFORMATION_MESSAGE);
                this.setVisible(false);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
