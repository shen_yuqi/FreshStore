package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanAddr;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserTakeOrderManager_buy_Addr extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnOk = new JButton("选择");

    private Object tblTitle[]= BeanAddr.tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable addrTable=new JTable(tablmod);
    List<BeanAddr> addrs=null;
    String id;
    private void reloadAddrTable() throws BaseException {
        try{
            addrs = FreshStoreUtil.userAddrManager.loadAll();
            tblData =new Object[addrs.size()][BeanAddr.tableTitles.length];
            for(int i=0;i<addrs.size();i++){
                for (int j=0;j<BeanAddr.tableTitles.length;j++){
                    tblData[i][j]=addrs.get(i).getCell(j);
                }
            }
            tablmod.setDataVector(tblData,tblTitle);
            this.addrTable.validate();
            this.addrTable.repaint();
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public FrmUserTakeOrderManager_buy_Addr(FrmUserTakeOrderManager_buy f, String s, boolean b,String ord_id) throws BaseException {
        super(f, s, b);
        id=ord_id;
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnOk);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadAddrTable();
        this.getContentPane().add(new JScrollPane(this.addrTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(400, 300);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnOk.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this.btnOk){
            int i=this.addrTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择地址","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FreshStoreUtil.orderManager.ModfOrderAddr(addrTable.getValueAt(i,0).toString(),id);
                this.setVisible(false);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
