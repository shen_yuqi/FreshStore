package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IUserGoodsManager {
    public List<BeanGoods> loadAll() throws BaseException;

    public List<BeanGoods> loadBySalesPro() throws BaseException;

    public List<BeanGoods> loadByPriceDesc() throws BaseException;

    public List<BeanGoods> loadByPriceAsc() throws BaseException;

    public List<BeanGoods> loadByType(String type_name) throws BaseException;

    public List<BeanGoods> searchByName(String name) throws BaseException;

    public List<BeanMenu> showMenu(BeanGoods beanGoods) throws BaseException;
}
