package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.PurchaseDetail;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmPurchaseDetailManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("采购商品");
    private JButton btnModf = new JButton("修改采购信息");
    private JButton btnDetail = new JButton("查看详细");
    private JButton btnDetailByID = new JButton("详细查询");

    private JButton btnSearch = new JButton("查询");

    private JComboBox cmbSearch= new JComboBox(new String[] {"","下单", "在途", "入库"});

    private JTextField edtSearch = new JTextField(15);

    private Object tblTitle[]={"采购单编码","食材编码","采购数量","状态"};
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable purchaseTable=new JTable(tablmod);
    List<BeanPurchaseDetail> purchases=null;
    private void reloadPurchaseTable() throws BaseException {
        try{
            int n=this.cmbSearch.getSelectedIndex();
            if(n>=1){
                purchases = (new PurchaseDetail()).loadByStatu((String) cmbSearch.getSelectedItem());
            }else{
                purchases = (new PurchaseDetail()).loadAll();
            }

            tblData =new Object[purchases.size()][4];
            for(int i=0;i<purchases.size();i++){
                tblData[i][0]=purchases.get(i).getPurchase_id();
                tblData[i][1]=purchases.get(i).getGoods_id();
                tblData[i][2]=purchases.get(i).getPurchase_quantity();
                tblData[i][3]=purchases.get(i).getPurchase_status();
            }
            tablmod.setDataVector(tblData,tblTitle);
            this.purchaseTable.validate();
            this.purchaseTable.repaint();
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public FrmPurchaseDetailManager(Frame f, String s, boolean b) throws BaseException {
        super(f, s, b);

        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnModf);
        toolBar.add(btnDetail);
        toolBar.add(edtSearch);
        toolBar.add(btnDetailByID);
        toolBar.add(cmbSearch);
        toolBar.add(btnSearch);
        edtSearch.setText("<-请输入采购单号->");

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadPurchaseTable();
        this.getContentPane().add(new JScrollPane(this.purchaseTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnModf.addActionListener(this);
        this.btnDetail.addActionListener(this);
        this.btnSearch.addActionListener(this);
        this.btnDetailByID.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmPurchaseDetailManager_Add dlg=new FrmPurchaseDetailManager_Add(this,"采购商品",true);
            dlg.setVisible(true);
            if(dlg.getPurchase()!=null){//刷新表格
                try {
                    this.reloadPurchaseTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnModf){
            FrmPurchaseDetailManager_Modf dlg= null;
            int i=this.purchaseTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择需要修改的采购单","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                String id=purchases.get(i).getPurchase_id();
                dlg = new FrmPurchaseDetailManager_Modf(this,"修改采购信息",true,id);
            } catch (DbException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
            if(dlg.getPurchase()!=null){//刷新表格
                try {
                    this.reloadPurchaseTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnDetail){
            int i=this.purchaseTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择需要查询的采购表","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            List<BeanPurchaseDetail> purchase= null;
            try {
                purchase = (new PurchaseDetail()).loadAll();
                String id=this.purchaseTable.getValueAt(i,0).toString();
                FrmPurchaseDetailManager_Detail dlg=new FrmPurchaseDetailManager_Detail(this,"采购表详细",true,id);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnDetailByID){
            FrmPurchaseDetailManager_Detail dlg= null;
            try {
                dlg = new FrmPurchaseDetailManager_Detail(this,"采购表详细",true,edtSearch.getText());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==this.btnSearch){
            try {
                this.reloadPurchaseTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
