package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.MenuFormManager;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.model.Discount;
import cn.syqcode.freshstore.model.MenuForm;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmDiscountManager_AddGoods extends JDialog implements ActionListener {
    String id;
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加");
    private JButton btnDelete = new JButton("删除");
    private JTextField edtAdd = new JTextField(20);
    private Object tblTitle[]=Discount.tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable discountTable=new JTable(tablmod);
    List<Discount> discountForms=null;
    private void reloadDiscountTable(String id) throws BaseException {
        discountForms= FreshStoreUtil.discountGoodsManager.loadByDiscountId(id);
        tblData =new Object[discountForms.size()][Discount.tableTitles.length];
        for(int i=0;i<discountForms.size();i++){
            for (int j=0;j<Discount.tableTitles.length;j++){
                tblData[i][j]=discountForms.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.discountTable.validate();
        this.discountTable.repaint();
    }
    public FrmDiscountManager_AddGoods(FrmDiscountManager f, String s, boolean b, String discount_id) throws BaseException{
        super(f,s,b);
        id=discount_id;
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(edtAdd);
        toolBar.add(btnAdd);
        toolBar.add(btnDelete);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadDiscountTable(discount_id);
        this.getContentPane().add(new JScrollPane(this.discountTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(400, 300);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            Discount discount=new Discount();
            BeanDiscount beanDiscount= null;
            try {
                beanDiscount = FreshStoreUtil.discountManager.searchByDiscountId(id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            discount.setGoods_id(this.edtAdd.getText());
            discount.setDiscount_id(id);
            discount.setDiscount_enddate(beanDiscount.getDiscount_enddate());
            discount.setDiscount_startdate(beanDiscount.getDiscount_startdate());

            try {
                FreshStoreUtil.discountGoodsManager.addDiscountGoods(discount);
            } catch (BaseException e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
            try {
                this.reloadDiscountTable(id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if(e.getSource()==this.btnDelete){
            int i=this.discountTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定将该商品从满折信息中删除吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                int no= 0;
                try {
                    no = FreshStoreUtil.discountGoodsManager.searchNoById(this.discountTable.getValueAt(i,1).toString(),this.discountTable.getValueAt(i,0).toString());
                    this.reloadDiscountTable(id);
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
                try {
                    FreshStoreUtil.discountGoodsManager.delDiscountGoods(no);
                    this.reloadDiscountTable(id);
                } catch (BaseException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
