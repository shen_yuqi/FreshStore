package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IMenuManager {
    public List<BeanMenu> loadAll() throws BaseException;

    public void addMenu(BeanMenu beanMenu) throws BaseException;

    public void ModfMenu(BeanMenu beanMenu) throws BaseException;

    public void delMenu(String id) throws BaseException;

    public List<BeanMenu> loadByGoodsId(String goods_id) throws BaseException;

    public BeanMenu searchByMenuId(String id) throws BaseException;

    public List<BeanMenu> loadByMenuName(String name) throws BaseException;
}
