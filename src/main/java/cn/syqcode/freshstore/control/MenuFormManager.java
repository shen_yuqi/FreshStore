package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.itf.IMenuFormManager;
import cn.syqcode.freshstore.itf.IMenuManager;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.model.MenuForm;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MenuFormManager implements IMenuFormManager {

    @Override
    public List<MenuForm> loadAll() throws BaseException {
        List<MenuForm> result= new ArrayList<MenuForm>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM menuform where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                MenuForm b=new MenuForm();
                b.setNo(rs.getInt(1));
                b.setGoods_id(rs.getString(2));
                b.setMenu_id(rs.getString(3));
                b.setRemarks(rs.getString(4));
                b.setDeleteflag(rs.getLong(5));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addMenuForm(MenuForm menuForm) throws BaseException {
        if ("".equals(menuForm.getGoods_id())){
            throw new BaseException("该商品不存在，请重新输入。");
        }
        if ("".equals(menuForm.getMenu_id())){
            throw new BaseException("该菜谱不存在，请重新输入。");
        }
        if ("".equals(menuForm.getRemarks())){
            throw new BaseException("请对菜谱进行描述。");
        }
        if (FreshStoreUtil.goodsManager.searchGoodsById(menuForm.getGoods_id())==null){
            throw new BaseException("该商品不存在");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into menuform(goods_id,menu_id,remarks,deleteflag) values (?,?,?,0)";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,menuForm.getGoods_id());
            pst.setString(2,menuForm.getMenu_id());
            pst.setString(3,menuForm.getRemarks());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void ModfMenuForm(MenuForm menuForm) throws BaseException {

    }

    @Override
    public void delMenuForm(int no) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update menuform set deleteflag=? where no = ?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setInt(2,no);
            pst.execute();
            pst.close();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<MenuForm> loadAllByMenuId(String id) throws BaseException {
        List<MenuForm> result= new ArrayList<MenuForm>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM menuform where deleteflag=0 and menu_id =?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                MenuForm b=new MenuForm();
                b.setNo(rs.getInt(1));
                b.setGoods_id(rs.getString(2));
                b.setMenu_id(rs.getString(3));
                b.setRemarks(rs.getString(4));
                b.setDeleteflag(rs.getLong(5));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public MenuForm searchByNo(int no) throws BaseException {
        MenuForm result= new MenuForm();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM menuform where deleteflag=0 and no=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setInt(1,no);
            java.sql.ResultSet rs=pst.executeQuery();
            if(rs.next()) {
                result.setNo(rs.getInt(1));
                result.setGoods_id(rs.getString(2));
                result.setMenu_id(rs.getString(3));
                result.setRemarks(rs.getString(4));
                result.setDeleteflag(rs.getLong(5));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
