package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IUserMenuManager {
    public List<BeanMenu> loadAll() throws BaseException;

    public List<BeanMenu> searchByName(String name) throws BaseException;

    public List<BeanGoods> showGoods(String menu_id) throws BaseException;

}
