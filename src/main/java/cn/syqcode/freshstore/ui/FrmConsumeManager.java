package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.PurchaseDetail;
import cn.syqcode.freshstore.control.UserManager;
import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class FrmConsumeManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnSum = new JButton("按单数降序查询");
    private JButton btnMoney = new JButton("按消费金额降序查询");
    private JButton btnDetail = new JButton("查看详细");
    private JButton btmNew = new JButton("刷新");

    private Object tblTitle[]={"用户编码","单数","总消费","总优惠"};
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable consumeTable=new JTable(tablmod);

    private void reloadConsumeTable() throws BaseException {
        try{
            List<BeanUser> user = (new UserManager()).loadAll();
//加载函数
            Connection conn=null;
            try {
                conn = DBUtil.getConnection();
                String sql="SELECT COUNT(DISTINCT user_id)\n" +
                        "FROM tbl_order\n" +
                        "WHERE ord_status NOT LIKE '退货' AND ord_status NOT LIKE '未下单'";
                java.sql.PreparedStatement pst=conn.prepareStatement(sql);
                java.sql.ResultSet rs=pst.executeQuery();
                rs.next();
                tblData =new Object[rs.getInt(1)][4];
                rs.close();
                pst.close();

                sql="SELECT user_id,COUNT(ord_id),SUM(ord_newmoney),SUM(ord_oldmoney)-SUM(ord_newmoney)\n" +
                        "FROM tbl_order\n" +
                        "WHERE ord_status NOT LIKE '退货' AND ord_status NOT LIKE '未下单'\n" +
                        "GROUP BY user_id";
                pst=conn.prepareStatement(sql);
                rs=pst.executeQuery();
                int i=0;
                while (rs.next()){
                    tblData[i][0]=rs.getString(1);
                    tblData[i][1]=rs.getInt(2);
                    tblData[i][2]=rs.getDouble(3);
                    tblData[i][3]=rs.getDouble(4);
                    i++;
                }
                tablmod.setDataVector(tblData,tblTitle);
                this.consumeTable.validate();
                this.consumeTable.repaint();
            }catch (SQLException e) {
                e.printStackTrace();
                throw new DbException(e);
            }
            finally{
                if(conn!=null)
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
            }
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void reloadConsumeTableBySum() throws BaseException {
        try{
            List<BeanUser> user = (new UserManager()).loadAll();
//加载函数
            Connection conn=null;
            try {
                conn = DBUtil.getConnection();
                String sql="SELECT COUNT(DISTINCT user_id)\n" +
                        "FROM tbl_order\n" +
                        "WHERE ord_status NOT LIKE '退货' AND ord_status NOT LIKE '未下单'\n";
                java.sql.PreparedStatement pst=conn.prepareStatement(sql);
                java.sql.ResultSet rs=pst.executeQuery();
                rs.next();
                tblData =new Object[rs.getInt(1)][4];
                rs.close();
                pst.close();

                sql="SELECT user_id,COUNT(ord_id),SUM(ord_newmoney),SUM(ord_oldmoney)-SUM(ord_newmoney)\n" +
                        "FROM tbl_order\n" +
                        "WHERE ord_status NOT LIKE '退货' AND ord_status NOT LIKE '未下单'\n" +
                        "GROUP BY user_id\n" +
                        "ORDER BY COUNT(ord_id) DESC";
                pst=conn.prepareStatement(sql);
                rs=pst.executeQuery();
                int i=0;
                while (rs.next()){
                    tblData[i][0]=rs.getString(1);
                    tblData[i][1]=rs.getInt(2);
                    tblData[i][2]=rs.getDouble(3);
                    tblData[i][3]=rs.getDouble(4);
                    i++;
                }
                tablmod.setDataVector(tblData,tblTitle);
                this.consumeTable.validate();
                this.consumeTable.repaint();
            }catch (SQLException e) {
                e.printStackTrace();
                throw new DbException(e);
            }
            finally{
                if(conn!=null)
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
            }
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void reloadConsumeTableByMenoy() throws BaseException {
        try{
            List<BeanUser> user = (new UserManager()).loadAll();
//加载函数
            Connection conn=null;
            try {
                conn = DBUtil.getConnection();
                String sql="SELECT COUNT(user_id)\n" +
                        "FROM tbl_order\n" +
                        "WHERE ord_status NOT LIKE '退货'\n" +
                        "GROUP BY user_id\n";
                java.sql.PreparedStatement pst=conn.prepareStatement(sql);
                java.sql.ResultSet rs=pst.executeQuery();
                rs.next();
                tblData =new Object[rs.getInt(1)][4];
                rs.close();
                pst.close();

                sql="SELECT user_id,COUNT(ord_id),SUM(ord_newmoney),SUM(ord_oldmoney)-SUM(ord_newmoney)\n" +
                        "FROM tbl_order\n" +
                        "WHERE ord_status NOT LIKE '退货'\n" +
                        "GROUP BY user_id\n" +
                        "ORDER BY SUM(ord_newmoney) DESC";
                pst=conn.prepareStatement(sql);
                rs=pst.executeQuery();
                int i=0;
                while (rs.next()){
                    tblData[i][0]=rs.getString(1);
                    tblData[i][1]=rs.getInt(2);
                    tblData[i][2]=rs.getDouble(3);
                    tblData[i][3]=rs.getDouble(4);
                    i++;
                }
                tablmod.setDataVector(tblData,tblTitle);
                this.consumeTable.validate();
                this.consumeTable.repaint();
            }catch (SQLException e) {
                e.printStackTrace();
                throw new DbException(e);
            }
            finally{
                if(conn!=null)
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
            }
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public FrmConsumeManager(Frame f, String s, boolean b) throws BaseException {
        super(f, s, b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnSum);
        toolBar.add(btnMoney);
        toolBar.add(btnDetail);
        toolBar.add(btmNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadConsumeTable();
        this.getContentPane().add(new JScrollPane(this.consumeTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnSum.addActionListener(this);
        this.btnSum.addActionListener(this);
        this.btnDetail.addActionListener(this);
        this.btmNew.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnSum){
            try {
                this.reloadConsumeTableBySum();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if(e.getSource()==this.btnMoney){
            try {
                this.reloadConsumeTableByMenoy();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if(e.getSource()==this.btnDetail){
            int i=this.consumeTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择需要查询的用户","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                String id=this.consumeTable.getValueAt(i, 0).toString();
                FrmConsumeManager_Detail dlg=new FrmConsumeManager_Detail(this,"消费明细",true,id);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btmNew){
            try {
                this.reloadConsumeTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
