package cn.syqcode.freshstore.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

//满折商品关联表
public class Discount {
    public static final String[] tableTitles={"满折编号","商品编号","起始日期","结束日期"};
    private static final long serialVersionUID = 1L;
    private int no;
    private String goods_id;
    private String discount_id;
    private long discount_startdate;
    private long discount_enddate;

    public void setNo(int no) {
        this.no = no;
    }

    public int getNo() {
        return no;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getDiscount_id() {
        return discount_id;
    }

    public void setDiscount_id(String discount_id) {
        this.discount_id = discount_id;
    }

    public long getDiscount_startdate() {
        return discount_startdate;
    }

    public void setDiscount_startdate(long discount_startdate) {
        this.discount_startdate = discount_startdate;
    }

    public long getDiscount_enddate() {
        return discount_enddate;
    }

    public void setDiscount_enddate(long discount_enddate) {
        this.discount_enddate = discount_enddate;
    }

    public String getCell(int col){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(col==0) return discount_id;
        else if (col==1) return goods_id;
        else if (col==2) {
            Date date = new Date(discount_startdate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else if (col==3){
            Date date = new Date(discount_enddate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else return "";
    }
}
