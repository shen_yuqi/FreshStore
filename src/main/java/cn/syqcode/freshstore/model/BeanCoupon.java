package cn.syqcode.freshstore.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

//优惠券
public class BeanCoupon {
    public int cnt=0;
    public static final String[] tableTitles={"优惠券","内容","适用金额","免减金额","起始日期","结束日期"};
    private static final long serialVersionUID = 1L;
    private int id;
    private String coupon_id;
    private String user_id;
    private String coupon_content;
    private double coupon_cut;
    private double coupon_limits;
    private long coupon_startdate;
    private long coupon_enddate;

    public int getCnt(){
        this.cnt++;
        return cnt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_content() {
        return coupon_content;
    }

    public void setCoupon_content(String coupon_content) {
        this.coupon_content = coupon_content;
    }

    public double getCoupon_cut() {
        return coupon_cut;
    }

    public void setCoupon_cut(double coupon_cut) {
        this.coupon_cut = coupon_cut;
    }

    public double getCoupon_limits() {
        return coupon_limits;
    }

    public void setCoupon_limits(double coupon_limits) {
        this.coupon_limits = coupon_limits;
    }

    public long getCoupon_startdate() {
        return coupon_startdate;
    }

    public void setCoupon_startdate(long coupon_startdate) {
        this.coupon_startdate = coupon_startdate;
    }

    public long getCoupon_enddate() {
        return coupon_enddate;
    }

    public void setCoupon_enddate(long coupon_enddate) {
        this.coupon_enddate = coupon_enddate;
    }

    public String getCell(int col){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(col==0) return String.valueOf(id);
        else if (col==1) return coupon_content;
        else if(col==2) return String.valueOf(coupon_limits);
        else if(col==3) return String.valueOf(coupon_cut);
        else if (col==4) {
            Date date = new Date(coupon_startdate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else if (col==5){
            Date date = new Date(coupon_enddate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else return "";
    }

}
