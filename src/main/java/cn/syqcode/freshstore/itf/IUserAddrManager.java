package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanAddr;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IUserAddrManager {
    public List<BeanAddr> loadAll() throws BaseException;

    public void ModfAddr(BeanAddr beanAddr) throws BaseException;

    public void addAddr(BeanAddr beanAddr) throws BaseException;

    public void delAddr(String addr_id) throws BaseException;

    public BeanAddr searchById(String addr_id) throws BaseException;

    public List<BeanAddr> loadByUserID(String user_id) throws BaseException;
}
