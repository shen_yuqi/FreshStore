package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanSalesPro;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FrmSalesProManager_Modf extends JDialog implements ActionListener {
    private BeanSalesPro salesPro =null;
    String id;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelSalesProId = new JLabel("促销编号：");
    private JLabel labelGoodsId = new JLabel("商品编号：");
    private JLabel lablePrice = new JLabel("促销价格：");
    private JLabel labelSum = new JLabel("促销数量：");
    private JLabel labelStart = new JLabel("开始日期：");
    private JLabel labelEnd = new JLabel("结束日期:");

    private JLabel edtSalesProId = new JLabel();

    private JTextField edtGoodsId = new JTextField(20);
    private JTextField edtPrice = new JTextField(20);
    private JTextField edtSum = new JTextField(20);
    private JTextField edtStart = new JTextField(20);
    private JTextField edtEnd = new JTextField(20);

    public FrmSalesProManager_Modf(JDialog f, String s, boolean b,String salesPro_id) throws BaseException {
        super(f,s,b);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date=new Date();
        String res;

        BeanSalesPro beanSalesPro= FreshStoreUtil.salesProManager.searchBySalesProId(salesPro_id);
        id=salesPro_id;
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelSalesProId);
        edtSalesProId.setText(salesPro_id);
        workPane.add(edtSalesProId);
        workPane.add(labelGoodsId);
        edtGoodsId.setText(beanSalesPro.getGoods_id());
        workPane.add(edtGoodsId);
        workPane.add(lablePrice);
        edtPrice.setText(String.valueOf(beanSalesPro.getSalespro_price()));
        workPane.add(edtPrice);
        workPane.add(labelSum);
        edtSum.setText(String.valueOf(beanSalesPro.getSalespro_quantity()));
        workPane.add(edtSum);
        workPane.add(labelStart);
        date = new Date(beanSalesPro.getSalespro_startdate());
        res = simpleDateFormat.format(date);
        edtStart.setText(res);
        workPane.add(edtStart);
        workPane.add(labelEnd);
        date = new Date(beanSalesPro.getSalespro_enddate());
        res = simpleDateFormat.format(date);
        edtEnd.setText(res);
        workPane.add(edtEnd);

        workPane.setLayout(null);

        labelSalesProId.setLocation(20,20);edtSalesProId.setLocation(100,20);
        labelGoodsId.setLocation(20, 40); edtGoodsId.setLocation(100,40);
        lablePrice.setLocation(20, 60); edtPrice.setLocation(100,60);
        labelSum.setLocation(20, 80); edtSum.setLocation(100,80);
        labelStart.setLocation(20, 100); edtStart.setLocation(100,100);
        labelEnd.setLocation(20,120);edtEnd.setLocation(100,120);

        labelSalesProId.setSize(80, 14); edtSalesProId.setSize(200,20);
        labelGoodsId.setSize(80, 14); edtGoodsId.setSize(200,20);
        lablePrice.setSize(80, 14); edtPrice.setSize(200,20);
        labelSum.setSize(80, 14);edtSum.setSize(200,20);
        labelStart.setSize(80, 14);edtStart.setSize(200,20);
        labelEnd.setSize(80,14);edtEnd.setSize(200,20);
        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(350, 250);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            salesPro=new BeanSalesPro();
            String goods_id = this.edtGoodsId.getText();
            double price=0;
            if (!"".equals(this.edtPrice.getText())){
                price = Double.parseDouble(this.edtPrice.getText());
            }
            double sum=0;
            if(!"".equals(this.edtSum.getText())){
                sum = Double.parseDouble(this.edtSum.getText());
            }
            long start=0;
            if (!"".equals(this.edtStart.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtStart.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                start = ts;
            }
            long end = 0;
            if (!"".equals(this.edtEnd.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtEnd.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                end = ts;
            }

            salesPro.setSalespro_id(id);
            salesPro.setGoods_id(goods_id);
            salesPro.setSalespro_price(price);
            salesPro.setSalespro_quantity(sum);
            salesPro.setSalespro_startdate(start);
            salesPro.setSalespro_enddate(end);
            try {
                FreshStoreUtil.salesProManager.ModfSalesPro(salesPro);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.salesPro=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanSalesPro getSalesPro(){return salesPro;}

}
