package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.itf.IUserManager;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserManager implements IUserManager {

    @Override
    public BeanUser login(String name, String pwd) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select user_name,user_password from tbl_user where deleteflag=0 and user_name=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,name);
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next()) throw new BusinessException("登陆账号不存在");
            rs.close();
            pst.close();
            sql="select user_name,user_password,user_id from tbl_user where deleteflag=0 and user_name=? and user_password=?";
            pst=conn.prepareStatement(sql);
            pst.setString(1,name);
            pst.setString(2,pwd);
            rs=pst.executeQuery();
            if(!rs.next()) throw new BusinessException("密码错误");
            BeanUser u= FreshStoreUtil.userManager.searchById(rs.getString(3));
            rs.close();
            pst.close();
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public BeanUser reg(BeanUser beanUser , String pwd) throws BaseException {
        if("".equals(beanUser.getUser_name()) || beanUser.getUser_name().length()>20){
            throw new BusinessException("登陆账号必须是1-20个字");
        }
        if("".equals(beanUser.getUser_password()) || pwd.length()>16){
            throw new BusinessException("账号密码必须是1-16个字");
        }
        if(!pwd.equals(beanUser.getUser_password())){
            throw new BusinessException("两次输入密码不一致");
        }
        if("".equals(beanUser.getUser_phone())||!Validator.isMobile(beanUser.getUser_phone()) ){
            throw new BusinessException("请输入正确的手机号码");
        }
        if("".equals(beanUser.getUser_email())|| !Validator.isEmail(beanUser.getUser_email())){
            throw new BusinessException("请输入正确的邮箱地址");
        }

        if("".equals(beanUser.getUser_city()) ){
            throw new BusinessException("请输入所在城市");
        }
        if(!"男".equals(beanUser.getUser_sex())&&!"女".equals(beanUser.getUser_sex())){
            throw new BusinessException("请选择性别");
        }
        if(!"是".equals(beanUser.getUser_isvip())&&!"否".equals(beanUser.getUser_isvip()) ){
            throw new BusinessException("请选择是否加入会员");
        }

        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql = "select * from tbl_user where deleteflag=0 and user_name = '"+beanUser.getUser_name()+"'";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if(rs.next()) throw new BusinessException("账号已经存在");
            rs.close();
            st.close();
            sql = "insert into tbl_user values(?,?,?,?,?,?,?,?,?,?,0)";
            PreparedStatement pst=conn.prepareStatement(sql);
            //生成唯一用户编码
            UUID uuid = UUID.randomUUID();
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,beanUser.getUser_name());
            pst.setString(3,beanUser.getUser_sex());
            pst.setString(4,beanUser.getUser_password());
            pst.setString(5,beanUser.getUser_phone());
            pst.setString(6,beanUser.getUser_email());
            pst.setString(7,beanUser.getUser_city());
            pst.setLong(8,System.currentTimeMillis());
            pst.setString(9,beanUser.getUser_isvip());
            if ("否".equals(beanUser.getUser_isvip())){
                pst.setLong(10,0);
            }else {
                pst.setLong(10,System.currentTimeMillis()+60*60*24*365*10000);
            }
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return null;
    }

    @Override
    public List<BeanUser> loadAll() throws BaseException {
        List<BeanUser> result=new ArrayList<BeanUser>();
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="SELECT * FROM tbl_user where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanUser b=new BeanUser();
                b.setUser_id(rs.getString(1));
                b.setUser_name(rs.getString(2));
                b.setUser_sex(rs.getString(3));
                b.setUser_password(rs.getString(4));
                b.setUser_phone(rs.getString(5));
                b.setUser_email(rs.getString(6));
                b.setUser_city(rs.getString(7));
                b.setUser_regdate(rs.getLong(8));
                b.setUser_isvip(rs.getString(9));
                b.setUser_vipend(rs.getLong(10));
                b.setDeleteflag(rs.getLong(11));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

        return result;
    }

    @Override
    public void ModfName(String userid,String name) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="update tbl_user set user_name=? where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,name);
            pst.setString(2,userid);
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void ModfPwd(String userid,String pwd1,String pwd2) throws BaseException {
        try {
            if(!pwd1.equals(pwd2)){
                throw new BaseException("两次密码不一致，请重新输入！");
            }
        }catch (BaseException e1){
            JOptionPane.showMessageDialog(null, e1.getMessage(), "错误", JOptionPane.ERROR_MESSAGE);
            return;
        }

        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="update tbl_user set user_password=? where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,pwd1);
            pst.setString(2,userid);
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

    @Override
    public void delUser(String user_id) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select * from tbl_user where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next()) throw new BusinessException("该账号不存在");
            rs.close();
            pst.close();
            sql="select * from tbl_order where user_id = ? and (ord_status='下单' or ord_status='配送')";
            pst=conn.prepareStatement(sql);
            pst.setString(1, user_id);
            rs=pst.executeQuery();
            if(!rs.next())
            {
                sql="update tbl_user set deleteflag=? where user_id=?";
                pst=conn.prepareStatement(sql);
                pst.setLong(1,System.currentTimeMillis());
                pst.setString(2, user_id);
                pst.execute();
                pst.close();
            }
            else
            {
                throw new BusinessException("该账号还有未完成的订单，无法删除");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

    @Override
    public void ModfUser(String user_id) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select * from tbl_user where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next()) throw new BusinessException("该账号不存在");
            rs.close();
            pst.close();
            sql="update tbl_user set user_password=? where deleteflag=0 and user_id=?";
            pst=conn.prepareStatement(sql);
            pst.setString(1, "123456");
            pst.setString(2, user_id);
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

    @Override
    public List<BeanUser> searchByName(String name) throws BaseException {
        List<BeanUser> result=new ArrayList<BeanUser>();
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="SELECT * FROM tbl_user where deleteflag=0 and user_name=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,name);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanUser b=new BeanUser();
                b.setUser_id(rs.getString(1));
                b.setUser_name(rs.getString(2));
                b.setUser_sex(rs.getString(3));
                b.setUser_password(rs.getString(4));
                b.setUser_phone(rs.getString(5));
                b.setUser_email(rs.getString(6));
                b.setUser_city(rs.getString(7));
                b.setUser_regdate(rs.getLong(8));
                b.setUser_isvip(rs.getString(9));
                b.setUser_vipend(rs.getLong(10));
                b.setDeleteflag(rs.getLong(11));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

        return result;
    }

    @Override
    public BeanUser searchById(String user_id) throws BaseException {
        BeanUser b =new BeanUser();
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="SELECT * FROM tbl_user where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                b.setUser_id(rs.getString(1));
                b.setUser_name(rs.getString(2));
                b.setUser_sex(rs.getString(3));
                b.setUser_password(rs.getString(4));
                b.setUser_phone(rs.getString(5));
                b.setUser_email(rs.getString(6));
                b.setUser_city(rs.getString(7));
                b.setUser_regdate(rs.getLong(8));
                b.setUser_isvip(rs.getString(9));
                b.setUser_vipend(rs.getLong(10));
                b.setDeleteflag(rs.getLong(11));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

        return b;
    }

    public List<BeanUser> searchById1(String user_id) throws BaseException{
        List<BeanUser> result =new ArrayList<>();
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="SELECT * FROM tbl_user where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanUser b=new BeanUser();
                b.setUser_id(rs.getString(1));
                b.setUser_name(rs.getString(2));
                b.setUser_sex(rs.getString(3));
                b.setUser_password(rs.getString(4));
                b.setUser_phone(rs.getString(5));
                b.setUser_email(rs.getString(6));
                b.setUser_city(rs.getString(7));
                b.setUser_regdate(rs.getLong(8));
                b.setUser_isvip(rs.getString(9));
                b.setUser_vipend(rs.getLong(10));
                b.setDeleteflag(rs.getLong(11));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

        return result;
    }
}
