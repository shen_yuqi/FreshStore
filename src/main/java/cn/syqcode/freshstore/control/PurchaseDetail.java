package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IPurchaseDetail;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PurchaseDetail implements IPurchaseDetail {
    @Override
    public List<BeanPurchaseDetail> loadAll() throws BaseException {
        List<BeanPurchaseDetail> result=new ArrayList<BeanPurchaseDetail>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM purchase_detail";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanPurchaseDetail pd=new BeanPurchaseDetail();
                pd.setPurchase_id(rs.getString(2));
                pd.setGoods_id(rs.getString(3));
                pd.setPurchase_quantity(rs.getInt(4));
                pd.setPurchase_status(rs.getString(5));
                result.add(pd);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

        return result;
    }

    @Override
    public void addPurchaseDetail(BeanPurchaseDetail beanPurchaseDetail) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select * from tbl_goods where goods_id = '"+beanPurchaseDetail.getGoods_id()+"' and deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()){
                rs.close();
                pst.close();
                sql="insert into purchase_detail(purchase_id,goods_id,purchase_quantity,purchase_status) values (?,?,?,?)";
                pst=conn.prepareStatement(sql);
                UUID uuid = UUID.randomUUID();
                String id = UUID.randomUUID().toString();
                pst.setString(1,id);
                pst.setString(2,beanPurchaseDetail.getGoods_id());
                pst.setDouble(3,beanPurchaseDetail.getPurchase_quantity());
                pst.setString(4,"下单");
                pst.execute();
                pst.close();
            }else {
                rs.close();
                pst.close();
                throw new BaseException("没有该商品，请重新输入！");
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }catch (BaseException e1){
            JOptionPane.showMessageDialog(null,  e1.getMessage(),"提示",JOptionPane.ERROR_MESSAGE);

        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

    @Override
    public void modfPurchaseDetail(BeanPurchaseDetail beanPurchaseDetail) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="update purchase_detail set purchase_quantity = ? , purchase_status=? where purchase_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setDouble(1,beanPurchaseDetail.getPurchase_quantity());
            pst.setString(2,beanPurchaseDetail.getPurchase_status());
            pst.setString(3,beanPurchaseDetail.getPurchase_id());
            pst.execute();
            pst.close();
            if ("入库".equals(beanPurchaseDetail.getPurchase_status())){
                sql="update tbl_goods set goods_quantity=goods_quantity+? where goods_id=?";
                pst=conn.prepareStatement(sql);
                pst.setDouble(1,beanPurchaseDetail.getPurchase_quantity());
                pst.setString(2,beanPurchaseDetail.getGoods_id());
                pst.execute();
                pst.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<BeanPurchaseDetail> loadByStatu(String statu) throws BaseException {
        List<BeanPurchaseDetail> result=new ArrayList<BeanPurchaseDetail>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM purchase_detail where purchase_status = '"+statu+"'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanPurchaseDetail pd=new BeanPurchaseDetail();
                pd.setPurchase_id(rs.getString(2));
                pd.setGoods_id(rs.getString(3));
                pd.setPurchase_quantity(rs.getInt(4));
                pd.setPurchase_status(rs.getString(5));
                result.add(pd);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

        return result;
    }

    @Override
    public BeanPurchaseDetail showDetail(String purchase_id) throws BaseException {
        BeanPurchaseDetail result=new BeanPurchaseDetail();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT b.purchase_id,a.goods_id,a.goods_name,a.goods_price,a.goods_vipprice,b.purchase_quantity,b.purchase_status\n" +
                    "FROM tbl_goods a,purchase_detail b\n" +
                    "WHERE a.goods_id = b.goods_id and  b.purchase_id='"+purchase_id+"'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            rs.next();
            BeanPurchaseDetail pd=new BeanPurchaseDetail();
            pd.setPurchase_id(rs.getString(2));
            pd.setGoods_id(rs.getString(3));
            pd.setPurchase_quantity(rs.getInt(4));
            pd.setPurchase_status(rs.getString(5));

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public String searchGoodsIdById(String purchase_id) throws BaseException {
        String result;
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select goods_id from purchase_detail where purchase_id='"+purchase_id+"'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            rs.next();
            result=rs.getString(1);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
