package cn.syqcode.freshstore;

import cn.syqcode.freshstore.ui.FrmMain;

import java.io.IOException;
import java.net.MalformedURLException;

public class FreshStoreStart {
    public static void main(String[] args) throws IOException {
        new FrmMain();
    }
}
