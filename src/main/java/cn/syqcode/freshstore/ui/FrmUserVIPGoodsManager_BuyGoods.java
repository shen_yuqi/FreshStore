package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;

public class FrmUserVIPGoodsManager_BuyGoods extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnBuy = new JButton("添加至购物车");
    private JButton btnCancel = new JButton("关闭");

    private JLabel labelGoodsName = new JLabel();
    private JLabel labelGoodsPrice = new JLabel();
    private JLabel labelGoodsVip = new JLabel();
    private JLabel labelQuantity = new JLabel();
    private JLabel labelSpecs = new JLabel();
    private JLabel labelDesc = new JLabel();
    private JLabel labelBuy = new JLabel();

    private JTextField edtbuy = new JTextField(22);

    String goods_id;
    BeanGoods beanGoods =null;
    double buysum=0;
    private int flag=0;

    public FrmUserVIPGoodsManager_BuyGoods(FrmUserVIPGoodsManager f, String s, boolean b, String goods_id) throws BaseException {
        super(f, s, b);
        beanGoods = FreshStoreUtil.goodsManager.searchGoodsById(goods_id);

        Connection conn = null;
        try {
            conn = DBUtil.getConnection();

            labelGoodsName.setText("商品名称：" + beanGoods.getGoods_name());
            labelGoodsPrice.setText("单价：" + beanGoods.getGoods_price());
            labelGoodsVip.setText("会员价：" + beanGoods.getGoods_vipprice());
            labelSpecs.setText("规格：" + beanGoods.getGoods_specs());
            labelDesc.setText("描述：" + beanGoods.getGoods_desc());
            labelQuantity.setText("库存:" + beanGoods.getGoods_quantity());
            labelBuy.setText("购买数量：");
            toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
            toolBar.add(btnBuy);
            toolBar.add(btnCancel);
            this.getContentPane().add(toolBar, BorderLayout.SOUTH);
            workPane.add(labelGoodsName);
            workPane.add(labelGoodsPrice);
            workPane.add(labelGoodsVip);
            workPane.add(labelSpecs);
            workPane.add(labelDesc);
            workPane.add(labelQuantity);
            workPane.add(labelBuy);
            workPane.add(edtbuy);

            workPane.setLayout(null);

            labelGoodsName.setLocation(20, 20);
            labelGoodsPrice.setLocation(20, 40);
            labelGoodsVip.setLocation(20, 60);
            labelSpecs.setLocation(20, 80);
            labelDesc.setLocation(20, 100);
            labelQuantity.setLocation(20, 120);
            labelBuy.setLocation(20,140); edtbuy.setLocation(100,140);

            labelGoodsName.setSize(110, 14);
            labelGoodsPrice.setSize(110, 14);
            labelGoodsVip.setSize(110, 14);
            labelSpecs.setSize(110, 14);
            labelDesc.setSize(110, 14);
            labelQuantity.setSize(110, 14);
            labelBuy.setSize(100,14); edtbuy.setSize(100,20);

            this.getContentPane().add(workPane, BorderLayout.CENTER);
            this.setSize(350, 250);
            // 屏幕居中显示
            double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
            double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
            this.setLocation((int) (width - this.getWidth()) / 2,
                    (int) (height - this.getHeight()) / 2);

            this.validate();

            btnBuy.addActionListener(this);
            btnCancel.addActionListener(this);
            this.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btnCancel) {
            this.setVisible(false);
        }
        else if(e.getSource()==this.btnBuy){
            try {
                if ("".equals(this.edtbuy.getText())){
                    throw new BaseException("请输入购买数量");
                }
                buysum=Double.parseDouble(this.edtbuy.getText());
                if (FreshStoreUtil.userOrderManager.addGoods(beanGoods.getGoods_id(), BeanUser.currentLoginUser.getUser_id(),buysum)){
                    JOptionPane.showMessageDialog(null,  "已添加至购物车","提示",JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
