package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.model.BeanAddr;
import cn.syqcode.freshstore.model.BeanCut;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserAddrManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加");
    private JButton btnModf = new JButton("修改");
    private JButton btnDelete = new JButton("删除");

    private Object tblTitle[]=BeanAddr.tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable addrTable=new JTable(tablmod);
    List<BeanAddr> addrs=null;
    private void reloadAddrTable() throws BaseException {
        try{
            addrs = FreshStoreUtil.userAddrManager.loadByUserID(BeanUser.currentLoginUser.getUser_id());
            tblData =new Object[addrs.size()][BeanAddr.tableTitles.length];
            for(int i=0;i<addrs.size();i++){
                for (int j=0;j<BeanAddr.tableTitles.length;j++){
                    tblData[i][j]=addrs.get(i).getCell(j);
                }
            }
            tablmod.setDataVector(tblData,tblTitle);
            this.addrTable.validate();
            this.addrTable.repaint();
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public FrmUserAddrManager(Frame f, String s, boolean b) throws BaseException {
        super(f, s, b);

        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnModf);
        toolBar.add(btnDelete);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadAddrTable();
        this.getContentPane().add(new JScrollPane(this.addrTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(400, 300);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnModf.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmUserAddrManager_Add dlg=new FrmUserAddrManager_Add(this,"添加收货地址",true);
            dlg.setVisible(true);
            if(dlg.getAddr()!=null){//刷新表格
                try {
                    this.reloadAddrTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnModf){
            int i=this.addrTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择地址","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                String id=addrTable.getValueAt(i,0).toString();
                FrmUserAddrManager_Modf dlg=new FrmUserAddrManager_Modf(this,"修改收货地址",true,id);
                dlg.setVisible(true);
                if(dlg.getAddr()!=null){//刷新表格
                    try {
                        this.reloadAddrTable();
                    } catch (BaseException ex) {
                        ex.printStackTrace();
                    }
                }
            } catch (BaseException ex) {
                ex.printStackTrace();
            }

        }
        else if(e.getSource()==this.btnDelete){
            int i=this.addrTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择收货地址","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除该收货地址吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                List<BeanFreshType> types= null;
                String id=addrTable.getValueAt(i,0).toString();
                try {
                    FreshStoreUtil.userAddrManager.delAddr(id);
                    this.reloadAddrTable();
                } catch (BaseException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
