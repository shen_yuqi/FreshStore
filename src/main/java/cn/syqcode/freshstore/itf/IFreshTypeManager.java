package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IFreshTypeManager {
    public List<BeanFreshType> loadAll() throws BaseException;

    public void addFreshType(BeanFreshType beanFreshType) throws BaseException;

    public void ModfFreshType(BeanFreshType beanFreshType,String oldName) throws BaseException;

    public void delFreshType(String type_id) throws BaseException;

    public String searchTypeById(String id) throws BaseException;

    public String searchTypeIdByName(String name) throws BaseException;
}
