package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.OrderDetail;
import cn.syqcode.freshstore.util.BaseException;

public interface IOrderDetailManager {
    public void addOrderDetail(OrderDetail orderDetail) throws BaseException;

    public OrderDetail searchOrderDetail(String goods_id, String ord_id) throws BaseException;

    public OrderDetail searchOrderDetailByNo(int no) throws BaseException;

    public void ModfQuantity(int no, double quantity) throws BaseException;

    public void delOrderDetail(int no) throws BaseException;
}
