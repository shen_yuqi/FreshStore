package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.ISystemUserManager;
import cn.syqcode.freshstore.model.BeanSystemUser;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class SystemUserManager implements ISystemUserManager {

    @Override
    public BeanSystemUser login(String name, String pwd) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select sys_name,sys_password from tbl_systemuser where sys_name=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,name);
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next()) throw new BusinessException("登陆账号不存在");
            rs.close();
            pst.close();
            sql="select sys_name,sys_password,sys_id from tbl_systemuser where sys_name=? and sys_password=?";
            pst=conn.prepareStatement(sql);
            pst.setString(1,name);
            pst.setString(2,pwd);
            rs=pst.executeQuery();
            if(!rs.next()) throw new BusinessException("密码错误");
            BeanSystemUser u=new BeanSystemUser();
            u.setSys_name(rs.getString(1));
            u.setSys_password(rs.getString(2));
            u.setSys_id(rs.getString(3));
            rs.close();
            pst.close();
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<BeanSystemUser> loadAll() throws BaseException {
        return null;
    }

    @Override
    public boolean AddSystemUser(BeanSystemUser beanSystemUser) throws BaseException {
        return false;
    }

    @Override
    public boolean ModfSystemUser(BeanSystemUser beanSystemUser) throws BaseException {
        return false;
    }

    @Override
    public boolean DelSystemUser(BeanSystemUser beanSystemUser) throws BaseException {
        return false;
    }
}
