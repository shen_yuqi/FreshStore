package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.control.GoodsManager;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class FrmGoodsManager_Modf extends JDialog implements ActionListener {
    private BeanGoods goods=null;
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelId = new JLabel("商品编码：");
    private JLabel labelName = new JLabel("商品名称：");
    private JLabel lablePrice = new JLabel("商品价格：");
    private JLabel labelVipPrice = new JLabel("会员价：");
    private JLabel labelQuantity = new JLabel("库存量：");
    private JLabel labelSpecs = new JLabel("规格:");
    private JLabel labelDesc = new JLabel("描述:");
    private JLabel labelType = new JLabel("商品类别:");

    private JTextField edtGoodsId = new JTextField(20);
    private JTextField edtGoodsName = new JTextField(20);
    private JTextField edtPrice = new JTextField(20);
    private JTextField edtVipPrice = new JTextField(20);
    private JTextField edtQuantity = new JTextField(20);
    private JTextField edtSpecs = new JTextField(20);
    private JTextField edtDesc = new JTextField(20);

    private JComboBox cmbType= new JComboBox();

    public FrmGoodsManager_Modf(JDialog f, String s, boolean b,String id) throws BaseException {
        super(f, s, b);
        BeanGoods beanGoods = (new GoodsManager()).searchGoodsById(id);
        int type_index=0;
        //获取类别名称
        List<BeanFreshType> types=null;
        try {
            types = (new FreshTypeManager()).loadAll();
            String[] strTypes=new String[types.size()+1];
            strTypes[0]="";
            for(int i=0;i<types.size();i++) {
                strTypes[i+1]=types.get(i).getType_name();
                if ((new FreshTypeManager()).searchTypeById(beanGoods.getType_id()).equals(strTypes[i+1])){
                    type_index=i+1;
                }
            }
            cmbType=new JComboBox(strTypes);
        } catch (BaseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        this.edtGoodsId.setText(beanGoods.getGoods_id());
        this.edtGoodsId.setEnabled(false);
        this.edtGoodsName.setText(beanGoods.getGoods_name());
        this.edtPrice.setText(String.valueOf(beanGoods.getGoods_price()));
        this.edtVipPrice.setText(String.valueOf(beanGoods.getGoods_vipprice()));
        this.edtQuantity.setText(String.valueOf(beanGoods.getGoods_quantity()));
        this.edtSpecs.setText(beanGoods.getGoods_specs());
        this.edtDesc.setText(beanGoods.getGoods_desc());
        cmbType.setSelectedIndex(type_index);

        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelId);
        workPane.add(edtGoodsId);
        workPane.add(labelName);
        workPane.add(edtGoodsName);
        workPane.add(labelType);
        workPane.add(cmbType);
        workPane.add(lablePrice);
        workPane.add(edtPrice);
        workPane.add(labelVipPrice);
        workPane.add(edtVipPrice);
        workPane.add(labelQuantity);
        workPane.add(edtQuantity);
        workPane.add(labelSpecs);
        workPane.add(edtSpecs);
        workPane.add(labelDesc);
        workPane.add(edtDesc);

        workPane.setLayout(null);

        labelId.setLocation(20, 20); edtGoodsId.setLocation(100,20);
        labelName.setLocation(20, 40); edtGoodsName.setLocation(100,40);
        labelType.setLocation(20, 60); cmbType.setLocation(100,60);
        lablePrice.setLocation(20, 80); edtPrice.setLocation(100,80);
        labelVipPrice.setLocation(20, 100); edtVipPrice.setLocation(100,100);
        labelQuantity.setLocation(20,120);edtQuantity.setLocation(100,120);
        labelSpecs.setLocation(20, 140); edtSpecs.setLocation(100,140);
        labelDesc.setLocation(20,160); edtDesc.setLocation(100,160);

        labelId.setSize(70, 14); edtGoodsId.setSize(200,20);
        labelName.setSize(70, 14); edtGoodsName.setSize(200,20);
        labelType.setSize(70, 14); cmbType.setSize(200,20);
        lablePrice.setSize(70, 14);edtPrice.setSize(200,20);
        labelVipPrice.setSize(70, 14);edtVipPrice.setSize(200,20);
        labelQuantity.setSize(70,14);edtQuantity.setSize(200,20);
        labelSpecs.setSize(70, 14);edtSpecs.setSize(200,20);
        labelDesc.setSize(70,14);edtDesc.setSize(200,20);
        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(350, 270);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            goods=new BeanGoods();
            String id=this.edtGoodsId.getText();
            String name = this.edtGoodsName.getText();
            String type = null;
            try {
                type = (new FreshTypeManager()).searchTypeIdByName((String) cmbType.getSelectedItem());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            double price = Double.parseDouble(this.edtPrice.getText());
            double vipPrice = Double.parseDouble(this.edtVipPrice.getText());
            double quantity = Double.parseDouble(this.edtQuantity.getText());
            String specs = this.edtSpecs.getText();
            String desc = this.edtDesc.getText();

            goods.setGoods_id(id);
            goods.setGoods_name(name);
            goods.setType_id(type);
            goods.setGoods_price(price);
            goods.setGoods_vipprice(vipPrice);
            goods.setGoods_quantity(quantity);
            goods.setGoods_specs(specs);
            goods.setGoods_desc(desc);

            try {
                (new GoodsManager()).ModfGoods(goods);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.goods=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanGoods getGoods(){return goods;}
}
