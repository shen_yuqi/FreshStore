package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IUserManager {
    public BeanUser login(String name , String pwd) throws BaseException;

    public BeanUser reg(BeanUser beanUser , String pwd) throws BaseException;

    public List<BeanUser> loadAll() throws BaseException;

    public void ModfName(String userid,String name) throws BaseException;

    public void ModfPwd(String userid,String pwd1,String pwd2) throws BaseException;

    //管理员功能
    public void delUser(String user_id) throws BaseException;

    public void ModfUser(String user_id) throws BaseException;

    public List<BeanUser> searchByName(String name) throws BaseException;

    public BeanUser searchById(String user_id) throws BaseException;

    public List<BeanUser> searchById1(String user_id) throws BaseException;

}
