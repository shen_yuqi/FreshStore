package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.UserManager;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmUserManager_AddUser extends JDialog implements ActionListener {
    private BeanUser user=null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");
    private JRadioButton man = new JRadioButton("男");
    private JRadioButton women = new JRadioButton("女");
    private JRadioButton isVip = new JRadioButton("是");
    private JRadioButton noVip = new JRadioButton("否");

    private JLabel labelUser = new JLabel("用户：   ");
    private JLabel lableSex = new JLabel("性别:   ");
    private JLabel labelPwd = new JLabel("密码：   ");
    private JLabel labelPwd2 = new JLabel("密码：   ");
    private JLabel labelPhone = new JLabel("联系方式:");
    private JLabel labelEmail = new JLabel("电子邮箱:");
    private JLabel labelCity = new JLabel("所在城市:");
    private JLabel labelVip = new JLabel("是否加入会员:");

    private JTextField edtUserId = new JTextField(22);
    private JTextField edtPhone = new JTextField(22);
    private JTextField edtEmail = new JTextField(22);
    private JTextField edtCity = new JTextField(22);
    private JPasswordField edtPwd = new JPasswordField(22);
    private JPasswordField edtPwd2 = new JPasswordField(22);
    public FrmUserManager_AddUser(JDialog f, String s, boolean b) {
        super(f, s, b);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelUser);
        workPane.add(edtUserId);
        workPane.add(labelPwd);
        workPane.add(edtPwd);
        workPane.add(labelPwd2);
        workPane.add(edtPwd2);
        workPane.add(labelPhone);
        workPane.add(edtPhone);
        workPane.add(labelEmail);
        workPane.add(edtEmail);
        workPane.add(labelCity);
        workPane.add(edtCity);
        workPane.add(lableSex);
        ButtonGroup group1 = new ButtonGroup();
        group1.add(man);
        group1.add(women);
        workPane.add(man);
        workPane.add(women);
        workPane.add(labelVip);
        ButtonGroup group2 = new ButtonGroup();
        group2.add(isVip);
        group2.add(noVip);
        workPane.add(isVip);
        workPane.add(noVip);
        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(320, 280);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            user=new BeanUser();
            String userid = this.edtUserId.getText();
            String pwd1 = new String(this.edtPwd.getPassword());
            String pwd2 = new String(this.edtPwd2.getPassword());
            String phone = this.edtPhone.getText();
            String city = this.edtCity.getText();
            String email = this.edtEmail.getText();

            user.setUser_city(city);
            user.setUser_email(email);
            user.setUser_password(pwd1);
            user.setUser_phone(phone);
            user.setUser_name(userid);

            if(this.man.isSelected()){
                user.setUser_sex("男");
            }else if(this.women.isSelected()){
                user.setUser_sex("女");
            }

            if(this.isVip.isSelected()){
                user.setUser_isvip("是");
            }else if(this.noVip.isSelected()){
                user.setUser_isvip("否");
            }
            try {
                (new UserManager()).reg(user,pwd2);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.user=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanUser getUser() {
        return user;
    }
}
