package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IPurchaseDetail {
    public List<BeanPurchaseDetail> loadAll() throws BaseException;

    public void addPurchaseDetail(BeanPurchaseDetail beanPurchaseDetail) throws BaseException;

    public void modfPurchaseDetail(BeanPurchaseDetail beanPurchaseDetail) throws BaseException;

    public List<BeanPurchaseDetail> loadByStatu(String statu) throws BaseException;

    public BeanPurchaseDetail showDetail(String purchase_id) throws BaseException;

    public String searchGoodsIdById(String purchase_id) throws BaseException;
}
