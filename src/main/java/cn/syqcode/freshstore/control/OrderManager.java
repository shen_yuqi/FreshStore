package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.itf.IOrderManager;
import cn.syqcode.freshstore.model.*;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderManager implements IOrderManager {
    @Override
    public List<BeanOrder> loadAll() throws BaseException {
        List<BeanOrder> result= new ArrayList<BeanOrder>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_order where ord_status not like '未下单'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanOrder b=new BeanOrder();
                b.setOrd_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setOrd_oldmoney(rs.getDouble(3));
                b.setOrd_newmoney(rs.getDouble(4));
                b.setCoupon_id(rs.getString(5));
                b.setOrd_time(rs.getLong(6));
                b.setAddr_id(rs.getString(7));
                b.setOrd_status(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public BeanOrder searchOrder(String order_id) throws BaseException {
        BeanOrder b= new BeanOrder();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_order where ord_id='"+order_id+"' and ord_status not like '未下单'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next()) {
                b=null;
                throw new BaseException("查无此订单。");
            }
            else  {
                b.setOrd_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setOrd_oldmoney(rs.getDouble(3));
                b.setOrd_newmoney(rs.getDouble(4));
                b.setCoupon_id(rs.getString(5));
                b.setOrd_time(rs.getLong(6));
                b.setAddr_id(rs.getString(7));
                b.setOrd_status(rs.getString(8));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }catch (BaseException e1){
            JOptionPane.showMessageDialog(null,  e1.getMessage(),"提示",JOptionPane.ERROR_MESSAGE);

        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return b;
    }

    //订单查询
    @Override
    public List<BeanOrder> loadByUserID(String user_id) throws BaseException {
        List<BeanOrder> result = new ArrayList<>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_order where user_id='"+user_id+"' and ord_status not like '未下单'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()) {
                rs.close();
                pst.close();
                sql = "select * from tbl_order where user_id = '"+user_id+"' and ord_status not like '未下单'";
                pst=conn.prepareStatement(sql);
                rs=pst.executeQuery();
                while (rs.next()){
                    BeanOrder b= new BeanOrder();
                    b.setOrd_id(rs.getString(1));
                    b.setUser_id(rs.getString(2));
                    b.setOrd_oldmoney(rs.getDouble(3));
                    b.setOrd_newmoney(rs.getDouble(4));
                    b.setCoupon_id(rs.getString(5));
                    b.setOrd_time(rs.getLong(6));
                    b.setAddr_id(rs.getString(7));
                    b.setOrd_status(rs.getString(8));
                    result.add(b);
                }

            }else{
                throw new BusinessException("该用户不存在");
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanOrder> loadByStatus(String statu) throws BaseException {
        List<BeanOrder> result = new ArrayList<>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_order where ord_status=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,statu);
            java.sql.ResultSet rs=pst.executeQuery();
            while (rs.next()){
                BeanOrder b= new BeanOrder();
                b.setOrd_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setOrd_oldmoney(rs.getDouble(3));
                b.setOrd_newmoney(rs.getDouble(4));
                b.setCoupon_id(rs.getString(5));
                b.setOrd_time(rs.getLong(6));
                b.setAddr_id(rs.getString(7));
                b.setOrd_status(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanOrder> loadByStatusUserID(String statu, String user_id) throws BaseException {
        List<BeanOrder> result = new ArrayList<>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_order where ord_status=? and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,statu);
            pst.setString(2,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while (rs.next()){
                BeanOrder b= new BeanOrder();
                b.setOrd_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setOrd_oldmoney(rs.getDouble(3));
                b.setOrd_newmoney(rs.getDouble(4));
                b.setCoupon_id(rs.getString(5));
                b.setOrd_time(rs.getLong(6));
                b.setAddr_id(rs.getString(7));
                b.setOrd_status(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void delOrder(BeanOrder beanOrder) throws BaseException {

    }

    @Override
    public void ModfOrderStatu(String ord_id,String statu) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_order set ord_status=? where ord_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,statu);
            pst.setString(2,ord_id);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<OrderDetail> showOrderDetail(String ord_id) throws BaseException {
        List<OrderDetail> result= new ArrayList<OrderDetail>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM orderdetail where ord_id = ? and deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,ord_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                OrderDetail b=new OrderDetail();
                b.setNo(rs.getInt(1));
                b.setOrd_id(rs.getString(3));
                b.setGoods_id(rs.getString(2));
                b.setDiscount_id(rs.getString(4));
                b.setDiscount_detail(rs.getDouble(7));
                b.setOrddetail_quantity(rs.getDouble(5));
                b.setGoods_price(rs.getDouble(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public OrderDetail ModfOrderDetail(OrderDetail orderDetail) throws BaseException {
        return null;
    }

    @Override
    public void delOrderDetail(int no) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update orderdetail set deleteflag=? where no=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setInt(2,no);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<String> searchOrderIdByUserId(String status,String user_id) throws BaseException {
        List<String> result=new ArrayList<>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT ord_id FROM tbl_order where user_id='"+user_id+"' and ord_status like ?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,status);
            java.sql.ResultSet rs=pst.executeQuery();
            while (rs.next()){
                String b;
                b=rs.getString(1);
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<OrderDetail> showOrderDetailNoEval(String ord_id) throws BaseException {
        List<OrderDetail> result= new ArrayList<OrderDetail>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * \n" +
                    "FROM orderdetail\n" +
                    "where ord_id = ? and deleteflag=0 and goods_id NOT IN(\n" +
                    "\tSELECT goods_id\n" +
                    "\tFROM evaluation\n" +
                    ")";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,ord_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                OrderDetail b=new OrderDetail();
                b.setNo(rs.getInt(1));
                b.setOrd_id(rs.getString(3));
                b.setGoods_id(rs.getString(2));
                b.setDiscount_id(rs.getString(4));
                b.setDiscount_detail(rs.getDouble(7));
                b.setOrddetail_quantity(rs.getDouble(5));
                b.setGoods_price(rs.getDouble(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void buyOrder(BeanOrder beanOrder) throws BaseException {
        Connection conn= null;
        double newmoney,allmoney=0,oldmoney=0;
        double cut1=0,cut2=0,cut3=0,cut4=0;
        String coupon_id;
        try{
            conn= DBUtil.getConnection();
            List<OrderDetail> orderDetail = FreshStoreUtil.orderManager.showOrderDetail(beanOrder.getOrd_id());
            for (int i=0;i<orderDetail.size();i++){
                newmoney=0;
                String discount_id;
                double discount_detail,salespro_price,salespro_quantity,price1;

                oldmoney=oldmoney+FreshStoreUtil.goodsManager.searchGoodsById(orderDetail.get(i).getGoods_id()).getGoods_price()*orderDetail.get(i).getOrddetail_quantity();
                if ("是".equals(BeanUser.currentLoginUser.getUser_isvip())){

                    price1=FreshStoreUtil.goodsManager.searchGoodsById(orderDetail.get(i).getGoods_id()).getGoods_vipprice();
                    newmoney=newmoney+price1*orderDetail.get(i).getOrddetail_quantity();
                    cut1=cut1+orderDetail.get(i).getOrddetail_quantity()*orderDetail.get(i).getGoods_price()-newmoney;
                }else {
                    price1=FreshStoreUtil.goodsManager.searchGoodsById(orderDetail.get(i).getGoods_id()).getGoods_price();
                    //oldmoney=oldmoney+price1*orderDetail.get(i).getOrddetail_quantity();
                    newmoney=newmoney+price1*orderDetail.get(i).getOrddetail_quantity();
                    cut1=cut1+0;
                }

                String sql="SELECT salespro_price,salespro_quantity\n" +
                        "FROM tbl_salespro\n" +
                        "WHERE goods_id=?";
                java.sql.PreparedStatement pst=conn.prepareStatement(sql);
                pst.setString(1,orderDetail.get(i).getGoods_id());
                java.sql.ResultSet rs=pst.executeQuery();
                if (rs.next()){
                    double sum;
                    salespro_price=rs.getDouble(1);
                    salespro_quantity=rs.getDouble(2);
                    rs.close();
                    pst.close();
                    if (salespro_quantity>=orderDetail.get(i).getOrddetail_quantity()){
                        sum=salespro_quantity-orderDetail.get(i).getOrddetail_quantity();
                        cut2=cut2+(price1-salespro_price)*orderDetail.get(i).getOrddetail_quantity();
                        newmoney=newmoney-(price1-salespro_price)*orderDetail.get(i).getOrddetail_quantity();
                    }else{
                        sum=0;
                        cut2=cut2+(price1-salespro_price)*salespro_quantity;
                        newmoney=newmoney-(price1-salespro_price)*salespro_quantity;
                    }
                    sql="update tbl_salespro set salespro_quantity=? where goods_id=?";
                    pst=conn.prepareStatement(sql);
                    pst.setDouble(1,sum);
                    pst.setString(2,orderDetail.get(i).getGoods_id());
                    pst.execute();
                    pst.close();
                }

                sql="SELECT b.discount_id,b.discount_detail,b.discount_limits\n" +
                        "FROM discount a,tbl_discount b\n" +
                        "WHERE goods_id =? AND a.discount_id=b.discount_id AND b.discount_startdate<? AND b.discount_enddate>?";
                pst=conn.prepareStatement(sql);
                pst.setString(1,orderDetail.get(i).getGoods_id());
                pst.setLong(2,System.currentTimeMillis());
                pst.setLong(3,System.currentTimeMillis());
                rs=pst.executeQuery();
                if (rs.next()&&orderDetail.get(i).getOrddetail_quantity()>=rs.getDouble(3)){
                    discount_id=rs.getString(1);
                    discount_detail=rs.getDouble(2);
                    rs.close();
                    pst.close();
                    cut3=cut3+newmoney*(1-discount_detail);
                    newmoney =newmoney*discount_detail;
                    sql="UPDATE orderdetail\n" +
                            "SET discount_id=?, discount_detail=?\n" +
                            "WHERE `no`=?";
                    pst=conn.prepareStatement(sql);
                    pst.setString(1,discount_id);
                    pst.setDouble(2,discount_detail);
                    pst.setInt(3,orderDetail.get(i).getNo());
                    pst.execute();
                    pst.close();
                }
                allmoney=allmoney+newmoney;
            }

            String sql="SELECT coupon_cut,coupon_id\n" +
                    "FROM tbl_coupon\n" +
                    "WHERE user_id=? AND coupon_limits<? AND coupon_cut>=ALL(\n" +
                    "\tSELECT coupon_cut\n" +
                    "\tFROM tbl_coupon\n" +
                    "\tWHERE user_id=? AND coupon_limits<?\n" +
                    ")";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,BeanUser.currentLoginUser.getUser_id());
            pst.setDouble(2,allmoney);
            pst.setString(3,BeanUser.currentLoginUser.getUser_id());
            pst.setDouble(4,allmoney);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()){
                cut4=rs.getDouble(1);
                coupon_id=rs.getString(2);
                rs.close();
                pst.close();

                sql="update tbl_order set coupon_id=?,ord_newmoney=?,ord_time=?,ord_oldmoney=? where ord_id=?";
                pst=conn.prepareStatement(sql);
                pst.setString(1,coupon_id);
                pst.setDouble(2,allmoney);
                pst.setLong(3,beanOrder.getOrd_time());
                pst.setDouble(4,oldmoney);
                pst.setString(5,beanOrder.getOrd_id());
                pst.execute();
                pst.close();
            }
            if (FreshStoreUtil.cutManager.searchByOrderID(beanOrder.getOrd_id())==null){
                sql="insert into tbl_cut values (?,?,?,?,?,?)";
                pst=conn.prepareStatement(sql);
                pst.setString(1,beanOrder.getOrd_id());
                pst.setDouble(2,cut1);
                pst.setDouble(3,cut2);
                pst.setDouble(4,cut3);
                pst.setDouble(5,cut4);
                pst.setDouble(6,cut1+cut2+cut3+cut4);
                pst.execute();
                pst.close();
            }else {
                sql="update tbl_cut set cut1=?,cut2=?,cut3=?,cut4=?,cut=? where ord_id=?";
                pst=conn.prepareStatement(sql);
                pst.setString(6,beanOrder.getOrd_id());
                pst.setDouble(1,cut1);
                pst.setDouble(2,cut2);
                pst.setDouble(3,cut3);
                pst.setDouble(4,cut4);
                pst.setDouble(5,cut1+cut2+cut3+cut4);
                pst.execute();
                pst.close();
            }


        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

    @Override
    public void ModfOrderAddr(String addr_id,String ord_id) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_order set addr_id=? where ord_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,addr_id);
            pst.setString(2,ord_id);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }
}
