package cn.syqcode.freshstore.control;

import cn.hutool.core.bean.BeanException;
import cn.hutool.db.sql.Order;
import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.itf.IUserOrderManager;
import cn.syqcode.freshstore.model.*;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserOrderManager implements IUserOrderManager {
    @Override
    public List<BeanOrder> loadAll(String user_id) throws BaseException {
        List<BeanOrder> result= new ArrayList<BeanOrder>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_order where user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanOrder b=new BeanOrder();
                b.setOrd_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setOrd_oldmoney(rs.getDouble(3));
                b.setOrd_newmoney(rs.getDouble(4));
                b.setCoupon_id(rs.getString(5));
                b.setOrd_time(rs.getLong(6));
                b.setAddr_id(rs.getString(7));
                b.setOrd_status(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanOrder> searchByStatus(String user_id, String status) throws BaseException {
        List<BeanOrder> result = new ArrayList<>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_order where ord_status=? and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,status);
            pst.setString(2,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while (rs.next()){
                BeanOrder b= new BeanOrder();
                b.setOrd_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setOrd_oldmoney(rs.getDouble(3));
                b.setOrd_newmoney(rs.getDouble(4));
                b.setCoupon_id(rs.getString(5));
                b.setOrd_time(rs.getLong(6));
                b.setAddr_id(rs.getString(7));
                b.setOrd_status(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    public BeanOrder searchByStatus2(String user_id, String status) throws BaseException {
        BeanOrder b = new BeanOrder();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_order where ord_status=? and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,status);
            pst.setString(2,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while (rs.next()){
                b.setOrd_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setOrd_oldmoney(rs.getDouble(3));
                b.setOrd_newmoney(rs.getDouble(4));
                b.setCoupon_id(rs.getString(5));
                b.setOrd_time(rs.getLong(6));
                b.setAddr_id(rs.getString(7));
                b.setOrd_status(rs.getString(8));

            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return b;
    }

    @Override
    public void ModfOrderStatus(String ord_id,String statu) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_order set ord_statu=? where ord_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,statu);
            pst.setString(2,ord_id);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void ModfOrder(BeanOrder beanOrder) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="UPDATE tbl_order\n" +
                    "SET ord_oldmoney=?,ord_newmoney=?,coupon_id=?,ord_time=?,addr_id=?,ord_status=?\n" +
                    "WHERE ord_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setDouble(1,beanOrder.getOrd_oldmoney());
            pst.setDouble(2,beanOrder.getOrd_newmoney());
            pst.setString(3,beanOrder.getCoupon_id());
            pst.setLong(4,beanOrder.getOrd_time());
            pst.setString(5,beanOrder.getAddr_id());
            pst.setString(6,beanOrder.getOrd_status());
            pst.setString(7,beanOrder.getOrd_id());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public boolean addGoods(String goods_id, String user_id,double buysum) throws BaseException {
        BeanGoods beanGoods= FreshStoreUtil.goodsManager.searchGoodsById(goods_id);
        try {
            if(buysum>beanGoods.getGoods_quantity()){
                throw new BaseException("库存不够，添加购物车失败。");
            }
        }catch (BaseException ex){
            JOptionPane.showMessageDialog(null,  ex.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            return false;
        }


        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT *\n" +
                    "FROM tbl_order\n" +
                    "WHERE user_id=? AND ord_status = '未下单'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next()){
                rs.close();
                pst.close();
                this.addOrder(user_id);
                double oldmoney=0;
                if ("是".equals(BeanUser.currentLoginUser.getUser_isvip())){
                    oldmoney=beanGoods.getGoods_vipprice()*buysum;
                }else{
                    oldmoney=beanGoods.getGoods_price()*buysum;
                }
                BeanOrder beanOrder=this.searchByStatus2(user_id,"未下单");
                beanOrder.setOrd_oldmoney(oldmoney);
                this.ModfOrder(beanOrder);
                OrderDetail orderDetail=new OrderDetail();
                orderDetail.setOrd_id(beanOrder.getOrd_id());
                orderDetail.setGoods_id(beanGoods.getGoods_id());
                orderDetail.setOrddetail_quantity(buysum);
                orderDetail.setGoods_price(beanGoods.getGoods_price());
                orderDetail.setDiscount_detail(1);
                orderDetail.setDiscount_id("0");
                orderDetail.setDeleteflag(0);
                FreshStoreUtil.orderDetailManager.addOrderDetail(orderDetail);
            }else{
                rs.close();
                pst.close();
                double oldmoney=0;
                if ("是".equals(BeanUser.currentLoginUser.getUser_isvip())){
                    oldmoney=beanGoods.getGoods_vipprice()*buysum;
                }else{
                    oldmoney=beanGoods.getGoods_price()*buysum;
                }
                BeanOrder beanOrder=this.searchByStatus2(user_id,"未下单");
                beanOrder.setOrd_oldmoney(oldmoney);
                this.ModfOrder(beanOrder);
                OrderDetail orderDetail=new OrderDetail();
                orderDetail.setOrd_id(beanOrder.getOrd_id());
                orderDetail.setGoods_id(beanGoods.getGoods_id());
                orderDetail.setOrddetail_quantity(buysum);
                orderDetail.setGoods_price(beanGoods.getGoods_price());
                orderDetail.setDiscount_detail(1);
                orderDetail.setDiscount_id("0");
                orderDetail.setDeleteflag(0);
                FreshStoreUtil.orderDetailManager.addOrderDetail(orderDetail);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return true;
    }

    @Override
    public void addOrder(String user_id) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into tbl_order values(?,?,0,0,0,0,0,'未下单')";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,user_id);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }
}
