package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IOrderDetailManager;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.model.OrderDetail;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class OrderDetailManager implements IOrderDetailManager {
    @Override
    public void addOrderDetail(OrderDetail orderDetail) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            OrderDetail d=new OrderDetail();
            d=searchOrderDetail(orderDetail.getGoods_id(),orderDetail.getOrd_id());
            if(d!=null){
                String sql="UPDATE orderdetail\n" +
                        "SET goods_id=?,ord_id=?,discount_id=?,orddetail_quantity=?,goods_price=?,discount_detail=?,deleteflag=?\n" +
                        "WHERE `no`=?";
                java.sql.PreparedStatement pst=conn.prepareStatement(sql);
                pst.setString(1,orderDetail.getGoods_id());
                pst.setString(2,orderDetail.getOrd_id());
                pst.setString(3,orderDetail.getDiscount_id());
                pst.setDouble(4,d.getOrddetail_quantity()+orderDetail.getOrddetail_quantity());
                pst.setDouble(5,orderDetail.getGoods_price());
                pst.setDouble(6,orderDetail.getDiscount_detail());
                pst.setLong(7,orderDetail.getDeleteflag());
                pst.setInt(8,d.getNo());
                pst.execute();
            }else{
                String sql="INSERT INTO orderdetail(goods_id,ord_id,discount_id,orddetail_quantity,goods_price,discount_detail,deleteflag)\n" +
                        "VALUES(?,?,?,?,?,?,?)";
                java.sql.PreparedStatement pst=conn.prepareStatement(sql);
                pst.setString(1,orderDetail.getGoods_id());
                pst.setString(2,orderDetail.getOrd_id());
                pst.setString(3,orderDetail.getDiscount_id());
                pst.setDouble(4,orderDetail.getOrddetail_quantity());
                pst.setDouble(5,orderDetail.getGoods_price());
                pst.setDouble(6,orderDetail.getDiscount_detail());
                pst.setLong(7,orderDetail.getDeleteflag());
                pst.execute();
            }

        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    public OrderDetail searchOrderDetail(String goods_id, String ord_id) throws BaseException {
        OrderDetail result=null;
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from orderdetail where goods_id=? and ord_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,goods_id);
            pst.setString(2,ord_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()){
                result=new OrderDetail();
                result.setNo(rs.getInt(1));
                result.setGoods_id(rs.getString(2));
                result.setOrd_id(rs.getString(3));
                result.setDiscount_id(rs.getString(4));
                result.setOrddetail_quantity(rs.getDouble(5));
                result.setGoods_price(rs.getDouble(6));
                result.setDiscount_detail(rs.getDouble(7));
                result.setDeleteflag(rs.getLong(8));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public OrderDetail searchOrderDetailByNo(int no) throws BaseException {
        OrderDetail result=null;
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from orderdetail where no=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setInt(1,no);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()){
                result=new OrderDetail();
                result.setNo(rs.getInt(1));
                result.setGoods_id(rs.getString(2));
                result.setOrd_id(rs.getString(3));
                result.setDiscount_id(rs.getString(4));
                result.setOrddetail_quantity(rs.getDouble(5));
                result.setGoods_price(rs.getDouble(6));
                result.setDiscount_detail(rs.getDouble(7));
                result.setDeleteflag(rs.getLong(8));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void ModfQuantity(int no, double quantity) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update orderdetail set orddetail_quantity=? where `no`=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setDouble(1,quantity);
            pst.setInt(2,no);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void delOrderDetail(int no) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update orderdetail set deleteflag=? where `no`=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setInt(2,no);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }
}
