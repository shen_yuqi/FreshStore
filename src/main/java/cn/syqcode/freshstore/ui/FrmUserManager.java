package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.UserManager;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class FrmUserManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加用户");
    private JButton btnResetPwd = new JButton("重置密码");
    private JButton btnDelete = new JButton("删除用户");
    private JButton btnSearch = new JButton("查询");
    private JButton btnAddr = new JButton("收货地址");
    private JButton btnNew = new JButton("刷新");

    private JComboBox cmbSearch= new JComboBox(new String[] {"", "按用户账号查询", "按用户编码查询"});

    private JTextField edtSearch = new JTextField(15);

    //private Map<String,BeanUser> userMap_name=new HashMap<String,BeanUser>();

    private Object tblTitle[]={"      账号      ","      性别      ","      电话      ", "      电子邮箱      ",
            "      所在城市      ","      注册时间      ","      是否会员      ","      会员到期      "};
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable userTable=new JTable(tablmod);

    public void FitTableColumns(JTable myTable) {
        JTableHeader header = myTable.getTableHeader();
        int rowCount = myTable.getRowCount();
        Enumeration columns = myTable.getColumnModel().getColumns();
        while (columns.hasMoreElements()) {
            TableColumn column = (TableColumn) columns.nextElement();
            int col = header.getColumnModel().getColumnIndex(column.getIdentifier());
            int width = (int) myTable.getTableHeader().getDefaultRenderer()
                    .getTableCellRendererComponent(myTable, column.getIdentifier()
                            , false, false, -1, col).getPreferredSize().getWidth();
            for (int row = 0; row < rowCount; row++) {
                int preferedWidth = (int) myTable.getCellRenderer(row, col).getTableCellRendererComponent(myTable,
                        myTable.getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth();
                width = Math.max(width, preferedWidth);
            }
            header.setResizingColumn(column); // 此行很重要
            column.setWidth(width + myTable.getIntercellSpacing().width);
        }
    }

    List<BeanUser> users=null;

    private void reloadUserTable(){
        try {
            int n=this.cmbSearch.getSelectedIndex();
            if(n==1){
                users=(new UserManager()).searchByName(this.edtSearch.getText());
            }else if(n==2){
                users=(new UserManager()).searchById1(this.edtSearch.getText());
            }else{
                users=(new UserManager()).loadAll();
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            tblData =new Object[users.size()][8];
            for(int i=0;i<users.size();i++){
                tblData[i][0]=users.get(i).getUser_name();
                tblData[i][1]=users.get(i).getUser_sex();
                tblData[i][2]=users.get(i).getUser_phone();
                tblData[i][3]=users.get(i).getUser_email();
                tblData[i][4]=users.get(i).getUser_city();
                tblData[i][5]=simpleDateFormat.format(new Date(users.get(i).getUser_regdate()));
                tblData[i][6]=users.get(i).getUser_isvip();
                tblData[i][7]=simpleDateFormat.format(new Date(users.get(i).getUser_vipend()));
            }
            tablmod.setDataVector(tblData,tblTitle);
            FitTableColumns(userTable);
            this.userTable.validate();
            this.userTable.repaint();
        } catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void reloadUserTable1(){
        try {

            users=(new UserManager()).loadAll();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            tblData =new Object[users.size()][8];
            for(int i=0;i<users.size();i++){
                tblData[i][0]=users.get(i).getUser_name();
                tblData[i][1]=users.get(i).getUser_sex();
                tblData[i][2]=users.get(i).getUser_phone();
                tblData[i][3]=users.get(i).getUser_email();
                tblData[i][4]=users.get(i).getUser_city();
                tblData[i][5]=simpleDateFormat.format(new Date(users.get(i).getUser_regdate()));
                tblData[i][6]=users.get(i).getUser_isvip();
                tblData[i][7]=simpleDateFormat.format(new Date(users.get(i).getUser_vipend()));
            }
            tablmod.setDataVector(tblData,tblTitle);
            FitTableColumns(userTable);
            this.userTable.validate();
            this.userTable.repaint();
        } catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public FrmUserManager(Frame f, String s, boolean b) {
        super(f, s, b);

        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnResetPwd);
        toolBar.add(this.btnDelete);
        toolBar.add(cmbSearch);
        toolBar.add(edtSearch);
        toolBar.add(btnSearch);
        toolBar.add(btnAddr);
        toolBar.add(btnNew);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadUserTable();
        this.getContentPane().add(new JScrollPane(this.userTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAddr.addActionListener(this);
        this.btnAdd.addActionListener(this);
        this.btnResetPwd.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.btnSearch.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmUserManager_AddUser dlg=new FrmUserManager_AddUser(this,"添加账号",true);
            dlg.setVisible(true);
            if(dlg.getUser()!=null){//刷新表格
                this.reloadUserTable();
            }
        }
        else if(e.getSource()==this.btnResetPwd){
            int i=this.userTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择账号","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定重置密码吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                try {
                    List<BeanUser> users=(new UserManager()).loadAll();
                    String userid=users.get(i).getUser_id();
                    try {
                        (new UserManager()).ModfUser(userid);
                        JOptionPane.showMessageDialog(null,  "密码重置完成","提示",JOptionPane.INFORMATION_MESSAGE);
                    } catch (BaseException e1) {
                        JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                    }
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }

            }
        }
        else if(e.getSource()==this.btnDelete){
            int i=this.userTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择账号","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除账号吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                List<BeanUser> users= null;
                try {
                    users = (new UserManager()).loadAll();
                    String userid=users.get(i).getUser_id();
                    try {
                        (new UserManager()).delUser(userid);
                        this.reloadUserTable();
                    } catch (BaseException e1) {
                        JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                    }
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if (e.getSource()==this.btnSearch){
            if(this.cmbSearch.getSelectedIndex()==0){
                JOptionPane.showMessageDialog(null,  "请选择查询方式","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.reloadUserTable();
        }
        else if (e.getSource()==this.btnAddr){
            int i=this.userTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择账号","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                users = (new UserManager()).loadAll();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            String userid=users.get(i).getUser_id();
            FrmUserManager_Addr dlg= null;
            try {
                dlg = new FrmUserManager_Addr(this,"收货地址",true,userid);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==this.btnNew){
            this.reloadUserTable1();
        }
    }
}
