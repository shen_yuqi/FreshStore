package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IEvaluationManager;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.model.Evaluation;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EvaluationManager implements IEvaluationManager {
    @Override
    public List<Evaluation> loadAll() throws BaseException {
        List<Evaluation> result= new ArrayList<Evaluation>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM evaluation where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                Evaluation b=new Evaluation();
                b.setNo(rs.getInt(1));
                b.setUser_id(rs.getString(2));
                b.setGoods_id(rs.getString(3));
                b.setEvaluation_content(rs.getString(4));
                b.setEvaluation_date(rs.getLong(5));
                b.setEvaluation_rank(rs.getString(6));
                b.setEvaluation_picture(rs.getString(7));
                b.setDeleteflag(rs.getLong(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addEvaluation(Evaluation evaluation) throws BaseException {
        if ("".equals(evaluation.getEvaluation_content())){
            throw new BusinessException("评价内容不能为空");
        }
        if ("".equals(evaluation.getEvaluation_rank())){
            throw new BusinessException("请对商品进行星级评价");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into evaluation(user_id,goods_id,evaluation_content,evaluation_date,evaluation_rank,evaluation_picture,deleteflag) values (?,?,?,?,?,?,0)";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,evaluation.getUser_id());
            pst.setString(2,evaluation.getGoods_id());
            pst.setString(3,evaluation.getEvaluation_content());
            pst.setDouble(4,System.currentTimeMillis());
            pst.setString(5,evaluation.getEvaluation_rank());
            pst.setString(6,evaluation.getEvaluation_picture());
            pst.execute();
            pst.close();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void delEvaluation(int no) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update evaluation set deleteflag=? where no=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setInt(2,no);
            pst.execute();
            pst.close();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<Evaluation> loadByGoods(String goods_id) throws BaseException {
        List<Evaluation> result= new ArrayList<Evaluation>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM evaluation where deleteflag=0 and goods_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,goods_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                Evaluation b=new Evaluation();
                b.setNo(rs.getInt(1));
                b.setUser_id(rs.getString(2));
                b.setGoods_id(rs.getString(3));
                b.setEvaluation_content(rs.getString(4));
                b.setEvaluation_date(rs.getLong(5));
                b.setEvaluation_rank(rs.getString(6));
                b.setEvaluation_picture(rs.getString(7));
                b.setDeleteflag(rs.getLong(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<Evaluation> loadByUserId(String userid) throws BaseException {
        List<Evaluation> result= new ArrayList<Evaluation>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM evaluation where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,userid);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                Evaluation b=new Evaluation();
                b.setNo(rs.getInt(1));
                b.setUser_id(rs.getString(2));
                b.setGoods_id(rs.getString(3));
                b.setEvaluation_content(rs.getString(4));
                b.setEvaluation_date(rs.getLong(5));
                b.setEvaluation_rank(rs.getString(6));
                b.setEvaluation_picture(rs.getString(7));
                b.setDeleteflag(rs.getLong(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
