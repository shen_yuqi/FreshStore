package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmDiscountManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加");
    private JButton btnDelete = new JButton("删除");
    private JButton btnNew = new JButton("刷新");
    private JButton btnGoods = new JButton("添加满折商品");

    private Object tblTitle[]= BeanDiscount.tableTitles;
    private Object tblData[][];
    List<BeanDiscount> discount=null;
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable discountTable=new JTable(tablmod);

    private void reloadDiscountTable() throws BaseException {
        discount= FreshStoreUtil.discountManager.loadAll();
        tblData =new Object[discount.size()][BeanDiscount.tableTitles.length];
        for(int i=0;i<discount.size();i++){
            for (int j=0;j<BeanDiscount.tableTitles.length;j++){
                tblData[i][j]=discount.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.discountTable.validate();
        this.discountTable.repaint();
    }

    public FrmDiscountManager(Frame f, String s, boolean b) throws BaseException {
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnDelete);
        toolBar.add(btnNew);
        toolBar.add(btnGoods);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadDiscountTable();
        this.getContentPane().add(new JScrollPane(this.discountTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.btnGoods.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmDiscountManager_Add dlg=new FrmDiscountManager_Add(this,"添加满折信息",true);
            dlg.setVisible(true);
            if(dlg.getDiscount()!=null){//刷新表格
                try {
                    this.reloadDiscountTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnDelete){
            int i=this.discountTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择要删除的满折活动","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除该满折活动吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                String id=discountTable.getValueAt(i,0).toString();
                try {
                    FreshStoreUtil.discountManager.delDiscount(id);
                    this.reloadDiscountTable();
                } catch (BaseException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadDiscountTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnGoods){
            int i=this.discountTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择满折活动","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            String id=discountTable.getValueAt(i,0).toString();
            FrmDiscountManager_AddGoods dlg= null;
            try {
                dlg = new FrmDiscountManager_AddGoods(this,"添加满折活动的商品",true,id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);

        }
    }
}
