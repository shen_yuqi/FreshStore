package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.ICouponManager;
import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CouponManager implements ICouponManager {
    @Override
    public List<BeanCoupon> loadAll() throws BaseException {
        List<BeanCoupon> result= new ArrayList<BeanCoupon>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_coupon where coupon_enddate>?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanCoupon b=new BeanCoupon();
                b.setId(rs.getInt(1));
                b.setCoupon_id(rs.getString(2));
                b.setUser_id(rs.getString(3));
                b.setCoupon_content(rs.getString(4));
                b.setCoupon_limits(rs.getDouble(5));
                b.setCoupon_cut(rs.getDouble(6));
                b.setCoupon_startdate(rs.getLong(7));
                b.setCoupon_enddate(rs.getLong(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addCoupon(BeanCoupon beanCoupon) throws BaseException {
        if ("".equals(beanCoupon.getCoupon_content())){
            throw new BusinessException("请输入优惠券内容。");
        }
        if (beanCoupon.getCoupon_limits()<=0){
            throw new BusinessException("请输入正确的优惠券适用金额。");
        }
        if (beanCoupon.getCoupon_cut()<=0){
            throw new BusinessException("请输入正确的优惠券的满减金额。");
        }
        if (beanCoupon.getCoupon_startdate()==0){
            throw new BusinessException("请输入优惠券开始日期。");
        }
        if (beanCoupon.getCoupon_enddate()==0){
            throw new BusinessException("请输入优惠券结束日期。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT COUNT(user_id)\n" +
                    "FROM tbl_user\n" +
                    "WHERE deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            rs.next();
            int n=rs.getInt(1);
            rs.close();
            pst.close();
            sql = "SELECT MAX(id)\n" +
                    "FROM tbl_coupon";
            pst=conn.prepareStatement(sql);
            rs=pst.executeQuery();
            rs.next();
            int no = rs.getInt(1)+1;
            List<BeanUser> beanUsers=(new UserManager()).loadAll();
            for (int i=0;i<n;i++){
                sql="insert into tbl_coupon values (?,?,?,?,?,?,?,?)";
                pst=conn.prepareStatement(sql);
                String id = UUID.randomUUID().toString();
                pst.setInt(1,no);
                pst.setString(2,id);
                pst.setString(3,beanUsers.get(i).getUser_id());
                pst.setString(4,beanCoupon.getCoupon_content());
                pst.setDouble(5,beanCoupon.getCoupon_limits());
                pst.setDouble(6,beanCoupon.getCoupon_cut());
                pst.setLong(7,beanCoupon.getCoupon_startdate());
                pst.setLong(8,beanCoupon.getCoupon_enddate());
                pst.execute();
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void ModfCoupon(BeanCoupon beanCoupon) throws BaseException {
        if ("".equals(beanCoupon.getCoupon_content())){
            throw new BusinessException("请输入优惠券内容。");
        }
        if (beanCoupon.getCoupon_limits()<=0){
            throw new BusinessException("请输入正确的优惠券适用金额。");
        }
        if (beanCoupon.getCoupon_cut()<=0){
            throw new BusinessException("请输入正确的优惠券的满减金额。");
        }
        if (beanCoupon.getCoupon_startdate()==0){
            throw new BusinessException("请输入优惠券开始日期。");
        }
        if (beanCoupon.getCoupon_enddate()==0){
            throw new BusinessException("请输入优惠券结束日期。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_coupon set coupon_content=?,coupon_limits=?,coupon_cut=?,coupon_startdate=?,coupon_enddate=? where id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,beanCoupon.getCoupon_content());
            pst.setDouble(2,beanCoupon.getCoupon_limits());
            pst.setDouble(3,beanCoupon.getCoupon_cut());
            pst.setLong(4,beanCoupon.getCoupon_startdate());
            pst.setLong(5,beanCoupon.getCoupon_enddate());
            pst.setInt(6,beanCoupon.getId());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void delCoupon(int id) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_coupon set coupon_enddate=? where id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setInt(2,id);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<BeanCoupon> loadAllGroupId() throws BaseException {
        List<BeanCoupon> result= new ArrayList<BeanCoupon>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT id,coupon_content,coupon_cut,coupon_limits,coupon_startdate,coupon_enddate\n" +
                    "FROM tbl_coupon \n" +
                    "WHERE coupon_enddate>?\n" +
                    "GROUP BY id";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanCoupon b=new BeanCoupon();
                b.setId(rs.getInt(1));
                b.setCoupon_content(rs.getString(2));
                b.setCoupon_limits(rs.getDouble(4));
                b.setCoupon_cut(rs.getDouble(3));
                b.setCoupon_startdate(rs.getLong(5));
                b.setCoupon_enddate(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
