package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.control.MenuManager;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class FrmMenuManager_Modf extends JDialog implements ActionListener {
    private BeanMenu menu=null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelId = new JLabel("菜谱编号:");
    private JLabel labelName = new JLabel("菜谱名称：");
    private JLabel lableMaterial = new JLabel("菜谱用料：");
    private JLabel labelStep = new JLabel("步骤：");
    private JLabel labelPicture = new JLabel("图片：");
    private JLabel labelIdDetail = new JLabel();

    private JTextField edtMenuName = new JTextField(20);
    private JTextField edtMaterial = new JTextField(20);
    private JTextField edtStep = new JTextField(20);
    private JTextField edtPicture = new JTextField(20);

    private JComboBox cmbMenu= new JComboBox();

    public FrmMenuManager_Modf(JDialog f, String s, boolean b,String id) throws BaseException {
        super(f, s, b);
        BeanMenu beanMenu = (new MenuManager()).searchByMenuId(id);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelId);
        labelIdDetail.setText(id);
        labelIdDetail.setEnabled(false);
        workPane.add(labelIdDetail);
        workPane.add(labelName);
        edtMenuName.setText(beanMenu.getMenu_name());
        workPane.add(edtMenuName);
        workPane.add(lableMaterial);
        edtMaterial.setText(beanMenu.getMenu_material());
        workPane.add(edtMaterial);
        workPane.add(labelStep);
        edtStep.setText(beanMenu.getMenu_step());
        workPane.add(edtStep);
        workPane.add(labelPicture);
        edtPicture.setText(beanMenu.getMenu_picture());
        workPane.add(edtPicture);

        workPane.setLayout(null);

        labelId.setLocation(20, 20); labelIdDetail.setLocation(100,17);
        labelName.setLocation(20, 40); edtMenuName.setLocation(100,40);
        lableMaterial.setLocation(20, 60); edtMaterial.setLocation(100,60);
        labelStep.setLocation(20, 80); edtStep.setLocation(100,80);
        labelPicture.setLocation(20, 100); edtPicture.setLocation(100,100);

        labelId.setSize(70, 14); labelIdDetail.setSize(200,20);
        labelName.setSize(70, 14); edtMenuName.setSize(200,20);
        lableMaterial.setSize(70, 14); edtMaterial.setSize(200,20);
        labelStep.setSize(70, 14);edtStep.setSize(200,20);
        labelPicture.setSize(70, 14);edtPicture.setSize(200,20);

        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(350, 270);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            menu=new BeanMenu();
            String name = this.edtMenuName.getText();
            String material = this.edtMaterial.getText();
            String step = this.edtStep.getText();
            String picture = this.edtPicture.getText();
            menu.setMenu_id(labelIdDetail.getText());
            menu.setMenu_name(name);
            menu.setMenu_material(material);
            menu.setMenu_step(step);
            menu.setMenu_picture(picture);

            try {
                (new MenuManager()).ModfMenu(menu);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.menu=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanMenu getMenu(){return menu;}
}


