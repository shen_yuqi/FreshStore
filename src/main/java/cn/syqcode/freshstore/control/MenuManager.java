package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IMenuManager;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MenuManager implements IMenuManager {
    @Override
    public List<BeanMenu> loadAll() throws BaseException {
        List<BeanMenu> result= new ArrayList<BeanMenu>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_menu where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanMenu b=new BeanMenu();
                b.setMenu_id(rs.getString(1));
                b.setMenu_name(rs.getString(2));
                b.setMenu_material(rs.getString(3));
                b.setMenu_step(rs.getString(4));
                b.setMenu_picture(rs.getString(5));
                b.setDeleteflag(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addMenu(BeanMenu beanMenu) throws BaseException {
        if ("".equals(beanMenu.getMenu_name())){
            throw new BusinessException("请输入菜谱名称");
        }
        if ("".equals(beanMenu.getMenu_material())){
            throw new BusinessException("请输入菜谱用料");
        }
        if ("".equals(beanMenu.getMenu_step())){
            throw new BusinessException("请输入菜谱步骤");
        }
        if ("".equals(beanMenu.getMenu_picture())){
            throw new BusinessException("请输入上传菜品图片");
        }

        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into tbl_menu values (?,?,?,?,?,0)";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            UUID uuid = UUID.randomUUID();
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,beanMenu.getMenu_name());
            pst.setString(3,beanMenu.getMenu_material());
            pst.setString(4,beanMenu.getMenu_step());
            pst.setString(5,beanMenu.getMenu_picture());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

    @Override
    public void ModfMenu(BeanMenu beanMenu) throws BaseException {
        if ("".equals(beanMenu.getMenu_name())){
            throw new BusinessException("请输入菜谱名称");
        }
        if ("".equals(beanMenu.getMenu_material())){
            throw new BusinessException("请输入菜谱用料");
        }
        if ("".equals(beanMenu.getMenu_step())){
            throw new BusinessException("请输入菜谱步骤");
        }
        if ("".equals(beanMenu.getMenu_picture())){
            throw new BusinessException("请输入上传菜品图片");
        }

        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_menu set menu_name=?,menu_material=?,menu_step=?,menu_picture=? where menu_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,beanMenu.getMenu_name());
            pst.setString(2,beanMenu.getMenu_material());
            pst.setString(3,beanMenu.getMenu_step());
            pst.setString(4,beanMenu.getMenu_picture());
            pst.setString(5,beanMenu.getMenu_id());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }


    @Override
    public void delMenu(String id) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from menuform where menu_id=? and deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,id);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()) throw new BusinessException("该菜谱在推荐菜单中，无法删除。");
            sql = "update tbl_menu set deleteflag=? where menu_id='"+id+"'";
            pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    public List<BeanMenu> loadByGoodsId(String goods_id) throws BaseException{
        List<BeanMenu> result= new ArrayList<BeanMenu>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT a.*\n" +
                    "FROM tbl_menu a,menuform b\n" +
                    "WHERE a.deleteflag=0 AND a.menu_id = b.menu_id AND b.goods_id = ?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,goods_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanMenu b=new BeanMenu();
                b.setMenu_id(rs.getString(1));
                b.setMenu_name(rs.getString(2));
                b.setMenu_material(rs.getString(3));
                b.setMenu_step(rs.getString(4));
                b.setMenu_picture(rs.getString(5));
                b.setDeleteflag(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public BeanMenu searchByMenuId(String id) throws BaseException {
        BeanMenu b= new BeanMenu();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_menu where deleteflag=0 and menu_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                b.setMenu_id(rs.getString(1));
                b.setMenu_name(rs.getString(2));
                b.setMenu_material(rs.getString(3));
                b.setMenu_step(rs.getString(4));
                b.setMenu_picture(rs.getString(5));
                b.setDeleteflag(rs.getLong(6));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return b;
    }

    @Override
    public List<BeanMenu> loadByMenuName(String name) throws BaseException {
        List<BeanMenu> result= new ArrayList<BeanMenu>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_menu where deleteflag=0 and menu_name like '%"+name+"%'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanMenu b=new BeanMenu();
                b.setMenu_id(rs.getString(1));
                b.setMenu_name(rs.getString(2));
                b.setMenu_material(rs.getString(3));
                b.setMenu_step(rs.getString(4));
                b.setMenu_picture(rs.getString(5));
                b.setDeleteflag(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
