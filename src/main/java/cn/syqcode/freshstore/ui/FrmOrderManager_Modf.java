package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.OrderManager;
import cn.syqcode.freshstore.control.PurchaseDetail;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.model.OrderDetail;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmOrderManager_Modf extends JDialog implements ActionListener {
    String order_id;
    private BeanOrder order=null;
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JComboBox cmbStatus= new JComboBox(new String[] {"下单", "配送", "送达","退货"});

    public FrmOrderManager_Modf(JDialog f, String s, boolean b,String id) throws BaseException {
        super(f,s,b);
        order_id =id;
        BeanOrder beanOrder=(new OrderManager()).searchOrder(id);
        if("下单".equals(beanOrder.getOrd_status())){
            cmbStatus.setSelectedIndex(0);
        }else if ("配送".equals(beanOrder.getOrd_status())){
            cmbStatus.setSelectedIndex(1);
        }else if ("送达".equals(beanOrder.getOrd_status())){
            cmbStatus.setSelectedIndex(2);
        }else if ("退货".equals(beanOrder.getOrd_status())){
            cmbStatus.setSelectedIndex(3);
        }

        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(cmbStatus);
        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(100, 100);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            String statu = this.cmbStatus.getSelectedItem().toString();
            order=new BeanOrder();
            try {
                order.setOrd_status(statu);
                order.setOrd_id(order_id);
                (new OrderManager()).ModfOrderStatu(order_id,statu);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.order=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanOrder getOrder() {
        return order;
    }

}
