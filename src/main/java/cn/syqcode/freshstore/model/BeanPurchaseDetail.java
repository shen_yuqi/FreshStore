package cn.syqcode.freshstore.model;

import java.io.Serializable;

//采购详细
public class BeanPurchaseDetail {
    private static final long serialVersionUID = 1L;
    private int no;
    private String purchase_id;
    private String goods_id;
    private double purchase_quantity;
    private String purchase_status;

    public int getNo() {
        return no;
    }

    public String getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(String purchase_id) {
        this.purchase_id = purchase_id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public double getPurchase_quantity() {
        return purchase_quantity;
    }

    public void setPurchase_quantity(double purchase_quantity) {
        this.purchase_quantity = purchase_quantity;
    }

    public String getPurchase_status() {
        return purchase_status;
    }

    public void setPurchase_status(String purchase_status) {
        this.purchase_status = purchase_status;
    }
}
