package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.CouponManager;
import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.control.GoodsManager;
import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmCouponManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加");
    private JButton btnDelete = new JButton("删除");
    private JButton btnNew = new JButton("刷新");

    private Object tblTitle[]= BeanCoupon.tableTitles;
    private Object tblData[][];
    List<BeanCoupon> coupons=null;
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable couponTable=new JTable(tablmod);

    private void reloadCouponTable() throws BaseException {
        coupons=(new CouponManager()).loadAllGroupId();
        tblData =new Object[coupons.size()][BeanCoupon.tableTitles.length];
        for(int i=0;i<coupons.size();i++){
            for (int j=0;j<BeanCoupon.tableTitles.length;j++){
                tblData[i][j]=coupons.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.couponTable.validate();
        this.couponTable.repaint();
    }


    public FrmCouponManager(Frame f, String s, boolean b) throws BaseException {
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnDelete);
        toolBar.add(btnNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadCouponTable();
        this.getContentPane().add(new JScrollPane(this.couponTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmCouponManager_Add dlg=new FrmCouponManager_Add(this,"添加优惠券",true);
            dlg.setVisible(true);
            if(dlg.getCoupon()!=null){//刷新表格
                try {
                    this.reloadCouponTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnDelete){
            int i=this.couponTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择要删除的优惠券","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除该优惠券吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                int id=Integer.parseInt(couponTable.getValueAt(i,0).toString());
                try {
                    (new CouponManager()).delCoupon(id);
                    this.reloadCouponTable();
                } catch (BaseException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadCouponTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
