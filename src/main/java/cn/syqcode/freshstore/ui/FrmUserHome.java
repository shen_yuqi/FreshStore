package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.util.BaseException;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class FrmUserHome extends JFrame implements ActionListener{
    private static final long serialVersionUID = 1L;
    private JMenuBar menubar=new JMenuBar();
    private JMenu menu_home = new JMenu("首页");
    private JMenu menu_user=new JMenu("个人信息");
    private JMenu menu_order=new JMenu("我的订单");
    private JMenu menu_discount=new JMenu("优惠活动");

    private JMenuItem menuItem_showMenu = new JMenuItem("菜谱推荐");
    private JMenuItem menuItem_showGoods = new JMenuItem("选购商品");
    private JMenuItem menuItem_showVIPGoods = new JMenuItem("VIP专享");

    private JMenuItem menuItem_addr = new JMenuItem("收货地址");
    private JMenuItem menuItem_modifyPwd = new JMenuItem("修改密码");

    private JMenuItem menuItem_allOrder = new JMenuItem("全部订单");
    private JMenuItem menuItem_takeOrder = new JMenuItem("购物车");
    private JMenuItem menuItem_waitToSend = new JMenuItem("正在备货");
    private JMenuItem menuItem_waitToReceive = new JMenuItem("正在配送");
    private JMenuItem menuItem_waitToEvaluation = new JMenuItem("等待评价");
    private JMenuItem menuItem_evaluation = new JMenuItem("我的评价");

    private JMenuItem menuItem_coupon = new JMenuItem("优惠券");
    private JMenuItem menuItem_discount = new JMenuItem("折扣信息");
    private JMenuItem menuItem_salesPro = new JMenuItem("限时促销");


    private JPanel statusBar = new JPanel();


    public FrmUserHome() throws IOException {
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
        this.setTitle("生鲜超网");
        //菜单
        this.menu_home.add(this.menuItem_showMenu); this.menuItem_showMenu.addActionListener(this);
        this.menu_home.add(this.menuItem_showGoods); this.menuItem_showGoods.addActionListener(this);
        this.menu_home.add(this.menuItem_showVIPGoods); this.menuItem_showVIPGoods.addActionListener(this);

        this.menu_user.add(this.menuItem_addr); this.menuItem_addr.addActionListener(this);
        this.menu_user.add(this.menuItem_modifyPwd); this.menuItem_modifyPwd.addActionListener(this);

        this.menu_order.add(this.menuItem_allOrder); this.menuItem_allOrder.addActionListener(this);
        this.menu_order.add(this.menuItem_takeOrder); this.menuItem_takeOrder.addActionListener(this);
        this.menu_order.add(this.menuItem_waitToSend); this.menuItem_waitToSend.addActionListener(this);
        this.menu_order.add(this.menuItem_waitToReceive); this.menuItem_waitToReceive.addActionListener(this);
        this.menu_order.add(this.menuItem_waitToEvaluation); this.menuItem_waitToEvaluation.addActionListener(this);
        this.menu_order.add(this.menuItem_evaluation); this.menuItem_evaluation.addActionListener(this);

        this.menu_discount.add(this.menuItem_coupon); this.menuItem_coupon.addActionListener(this);
        this.menu_discount.add(this.menuItem_discount); this.menuItem_discount.addActionListener(this);
        this.menu_discount.add(this.menuItem_salesPro); this.menuItem_salesPro.addActionListener(this);

        menubar.add(menu_home);
        menubar.add(menu_user);
        menubar.add(menu_order);
        menubar.add(menu_discount);
        this.setJMenuBar(menubar);

        //状态栏
        statusBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel label=new JLabel("您好!");//修改成   您好！+登陆用户名
        statusBar.add(label);
        this.getContentPane().add(statusBar,BorderLayout.SOUTH);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==menuItem_showMenu){
            FrmUserMenuManager dlg = null;
            try {
                dlg = new FrmUserMenuManager(this,"菜谱推荐",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_showGoods){
            FrmUserGoodsManager dlg = null;
            try {
                dlg = new FrmUserGoodsManager(this,"选购商品",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_showVIPGoods){
            FrmUserVIPGoodsManager dlg = null;
            try {
                dlg = new FrmUserVIPGoodsManager(this,"VIP专享",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_addr){
            FrmUserAddrManager dlg = null;
            try {
                dlg = new FrmUserAddrManager(this,"收货地址",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_modifyPwd){
            FrmUserModfPwdManager dlg = null;
            try {
                dlg = new FrmUserModfPwdManager(this,"修改密码",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_allOrder){
            FrmUserAllOrderManager dlg = null;
            try {
                dlg = new FrmUserAllOrderManager(this,"全部订单",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_takeOrder){
            FrmUserTakeOrderManager dlg = null;
            try {
                dlg = new FrmUserTakeOrderManager(this,"购物车",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_waitToSend){
            FrmUserWaitToSendManager dlg = null;
            try {
                dlg = new FrmUserWaitToSendManager(this,"正在备货",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_waitToReceive){
            FrmUserWaitToReceiveManager dlg = null;
            try {
                dlg = new FrmUserWaitToReceiveManager(this,"正在配送",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_waitToEvaluation){
            FrmUserWaitToEvaluationManager dlg = null;
            try {
                dlg = new FrmUserWaitToEvaluationManager(this,"等待评价",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_evaluation){
            FrmUserEvaluationManager dlg = null;
            try {
                dlg = new FrmUserEvaluationManager(this,"我的评价",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_coupon){
            FrmUserCouponManager dlg = null;
            try {
                dlg = new FrmUserCouponManager(this,"我的优惠券",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_discount){
            FrmUserDiscountManager dlg = null;
            try {
                dlg = new FrmUserDiscountManager(this,"满折活动",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_salesPro){
            FrmUserSalesProManager dlg = null;
            try {
                dlg = new FrmUserSalesProManager(this,"限时促销",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
    }
}
