package cn.syqcode.freshstore.model;

import java.io.Serializable;

public class BeanFreshType {
    private static final long serialVersionUID = 1L;
    private String type_id;
    private String type_name;

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    private String type_desc;
    private long deleteflag;

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getType_desc() {
        return type_desc;
    }

    public void setType_desc(String type_desc) {
        this.type_desc = type_desc;
    }
}
