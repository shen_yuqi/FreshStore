package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.model.BeanSalesPro;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserSalesProManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("购买商品");

    private Object tblTitle[]= BeanDiscount.tableTitles;
    private Object tblData[][];
    List<BeanSalesPro> salesPros=null;
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable salesProTable=new JTable(tablmod);

    private void reloadDiscountTable() throws BaseException {
        salesPros= FreshStoreUtil.salesProManager.loadAll();
        tblData =new Object[salesPros.size()][BeanSalesPro.tableTitles.length];
        for(int i=0;i<salesPros.size();i++){
            for (int j=0;j<BeanDiscount.tableTitles.length;j++){
                tblData[i][j]=salesPros.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.salesProTable.validate();
        this.salesProTable.repaint();
    }
    public FrmUserSalesProManager(Frame f, String s, boolean b) throws BaseException {
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadDiscountTable();
        this.getContentPane().add(new JScrollPane(this.salesProTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this.btnAdd){
            int i=this.salesProTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FrmUserSalesProManager_BuyGoods dlg=new FrmUserSalesProManager_BuyGoods(this,"购买商品",true,salesProTable.getValueAt(i, 1).toString());
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
