package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.ICutManager;
import cn.syqcode.freshstore.model.BeanCut;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;

public class CutManager implements ICutManager {
    @Override
    public BeanCut searchByOrderID(String ord_id) throws BaseException {
        BeanCut beanCut=new BeanCut();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_cut where ord_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,ord_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()){
                beanCut.setCut1(rs.getDouble(2));
                beanCut.setCut2(rs.getDouble(3));
                beanCut.setCut3(rs.getDouble(4));
                beanCut.setCut4(rs.getDouble(5));
                beanCut.setCut(rs.getDouble(6));
            }else {
                beanCut=null;
            }

        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return beanCut;
    }
}
