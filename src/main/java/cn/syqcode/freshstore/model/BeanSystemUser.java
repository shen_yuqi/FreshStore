package cn.syqcode.freshstore.model;

import java.io.Serializable;

public class BeanSystemUser {
    private static final long serialVersionUID = 1L;
    public static BeanSystemUser currentLoginUser=null;
    private String sys_id;
    private String sys_name;
    private String sys_password;

    public String getSys_id() {
        return sys_id;
    }

    public void setSys_id(String sys_id) {
        this.sys_id = sys_id;
    }

    public String getSys_name() {
        return sys_name;
    }

    public void setSys_name(String sys_name) {
        this.sys_name = sys_name;
    }

    public String getSys_password() {
        return sys_password;
    }

    public void setSys_password(String sys_password) {
        this.sys_password = sys_password;
    }
}
