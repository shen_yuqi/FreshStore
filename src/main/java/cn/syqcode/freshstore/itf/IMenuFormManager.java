package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.MenuForm;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IMenuFormManager {
    public List<MenuForm> loadAll() throws BaseException;

    public void addMenuForm(MenuForm menuForm) throws BaseException;

    public void ModfMenuForm(MenuForm menuForm) throws BaseException;

    public void delMenuForm(int no) throws BaseException;

    public List<MenuForm> loadAllByMenuId(String id) throws  BaseException;

    public MenuForm searchByNo(int no) throws BaseException;
}
