package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.model.OrderDetail;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserWaitToEvaluationManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    //private JButton btnSearch = new JButton("查询");
    private JButton btnEval = new JButton("评价");

    //private JTextField edtSearch = new JTextField(15);

    private Object tblTitle[]=new OrderDetail().tableTitles;
    private Object tblData[][];

    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable orderTable=new JTable(tablmod);
    List<OrderDetail> orderDetails=null;

    private void reloadOrderDetailTable() throws BaseException {
        List<String> ord_id= FreshStoreUtil.orderManager.searchOrderIdByUserId("送达", BeanUser.currentLoginUser.getUser_id());
        int cnt=0;
        if (ord_id.size()==0){
            tblData =new Object[0][this.tblTitle.length];
        }
        else {
            int sum=0;
            for (int k=0;k<ord_id.size();k++){
                orderDetails=FreshStoreUtil.orderManager.showOrderDetailNoEval(ord_id.get(k));
                sum=sum+orderDetails.size();
            }
            tblData =new Object[sum][this.tblTitle.length];
            for (int k=0;k<ord_id.size();k++){
               // System.out.println(ord_id.size());
                orderDetails=FreshStoreUtil.orderManager.showOrderDetailNoEval(ord_id.get(k));
                //tblData =new Object[orderDetails.size()][this.tblTitle.length];
                for(int i=cnt;i<cnt+orderDetails.size();i++){
                    //System.out.println(cnt+orderDetails.size());
                    for (int j=0;j<this.tblTitle.length;j++) {
                        System.out.println(j);
                        tblData[i][j] = orderDetails.get(i-cnt).getCell(j);
                    }

                }
                cnt=cnt+orderDetails.size();
            }
        }

        tablmod.setDataVector(tblData,tblTitle);
        this.orderTable.validate();
        this.orderTable.repaint();
    }

    private final JScrollPane scroll = new JScrollPane();

    public FrmUserWaitToEvaluationManager(Frame f, String s, boolean b) throws BaseException{
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        //edtSearch.setText("<-请输入订单编号->");
        //toolBar.add(edtSearch);
        //toolBar.add(btnSearch);
        toolBar.add(btnEval);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadOrderDetailTable();
        this.getContentPane().add(new JScrollPane(this.orderTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

      //  this.btnSearch.addActionListener(this);
        this.btnEval.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
//        if (e.getSource()==this.btnSearch){
//            try {
//                this.reloadOrderTableById(this.edtSearch.getText());
//            } catch (BaseException ex) {
//                ex.printStackTrace();
//            }
//        }
        if (e.getSource()==this.btnEval){
            int i=this.orderTable.getSelectedRow();
            FrmUserWaitToEvaluationManager_Eval dlg = null;
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择评价的商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            dlg = new FrmUserWaitToEvaluationManager_Eval(this,"评价",true,Integer.parseInt(orderTable.getValueAt(i, 0).toString()));
            dlg.setVisible(true);
            try {
                this.reloadOrderDetailTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
