package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.itf.ISalesProManager;
import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.model.BeanSalesPro;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SalesProManager implements ISalesProManager {
    @Override
    public List<BeanSalesPro> loadAll() throws BaseException {
        List<BeanSalesPro> result= new ArrayList<BeanSalesPro>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_salespro where salespro_enddate > ?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanSalesPro b=new BeanSalesPro();
                b.setSalespro_id(rs.getString(1));
                b.setGoods_id(rs.getString(2));
                b.setSalespro_price(rs.getDouble(3));
                b.setSalespro_quantity(rs.getDouble(4));
                b.setSalespro_startdate(rs.getLong(5));
                b.setSalespro_enddate(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addSalesPro(BeanSalesPro beanSalesPro) throws BaseException {
        if ("".equals(beanSalesPro.getGoods_id())){
            throw new BusinessException("请输入促销的商品编码");
        }
        if (beanSalesPro.getSalespro_price()<=0){
            throw new BusinessException("请输入促销价格");
        }
        if (beanSalesPro.getSalespro_quantity()<=0){
            throw new BusinessException("请输入促销数量");
        }
        if (beanSalesPro.getSalespro_startdate()==0){
            throw new BusinessException("请输入促销开始日期。");
        }
        if (beanSalesPro.getSalespro_enddate()==0){
            throw new BusinessException("请输入促销结束日期。");
        }
        if (FreshStoreUtil.goodsManager.searchGoodsById(beanSalesPro.getGoods_id())==null){
            throw new BusinessException("该商品不存在");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into tbl_salespro values (?,?,?,?,?,?)";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,beanSalesPro.getGoods_id());
            pst.setDouble(3,beanSalesPro.getSalespro_price());
            pst.setDouble(4,beanSalesPro.getSalespro_quantity());
            pst.setLong(5,beanSalesPro.getSalespro_startdate());
            pst.setLong(6,beanSalesPro.getSalespro_enddate());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void ModfSalesPro(BeanSalesPro beanSalesPro) throws BaseException {
        if ("".equals(beanSalesPro.getGoods_id())){
            throw new BusinessException("请输入促销的商品编码");
        }
        if (beanSalesPro.getSalespro_price()<=0){
            throw new BusinessException("请输入促销价格");
        }
        if (beanSalesPro.getSalespro_quantity()<=0){
            throw new BusinessException("请输入促销数量");
        }
        if (beanSalesPro.getSalespro_startdate()==0){
            throw new BusinessException("请输入促销开始日期。");
        }
        if (beanSalesPro.getSalespro_enddate()==0){
            throw new BusinessException("请输入促销结束日期。");
        }
        if (FreshStoreUtil.goodsManager.searchGoodsById(beanSalesPro.getGoods_id())==null){
            throw new BusinessException("该商品不存在");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_salespro set salespro_price=?,salespro_quantity=?,salespro_startdate=?,salespro_enddate=? where salespro_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setDouble(1,beanSalesPro.getSalespro_price());
            pst.setDouble(2,beanSalesPro.getSalespro_quantity());
            pst.setLong(3,beanSalesPro.getSalespro_startdate());
            pst.setLong(4,beanSalesPro.getSalespro_enddate());
            pst.setString(5,beanSalesPro.getSalespro_id());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    //直接删除，商品促销需要自己链表判断
    @Override
    public void delSalesPro(String salespro_id) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_salespro set salespro_enddate=? where salespro_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,salespro_id);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public BeanSalesPro searchBySalesProId(String salesPro_id) throws BaseException {
        BeanSalesPro b= new BeanSalesPro();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_salespro where salespro_enddate > ? and salespro_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,salesPro_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()) {
                b.setSalespro_id(rs.getString(1));
                b.setGoods_id(rs.getString(2));
                b.setSalespro_price(rs.getDouble(3));
                b.setSalespro_quantity(rs.getDouble(4));
                b.setSalespro_startdate(rs.getLong(5));
                b.setSalespro_enddate(rs.getLong(6));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return b;
    }
}
