package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.model.Evaluation;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserEvaluationManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnSearch = new JButton("查询");
    private JButton btnDel = new JButton("删除");
    private JButton btnNew = new JButton("刷新");
    private JTextField edtSearch = new JTextField(15);

    private Object tblTitle[]=new Evaluation().tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable evaluationTable=new JTable(tablmod);
    List<Evaluation> evaluations=null;

    private void reloadEvaluationTable() throws BaseException {
        evaluations = FreshStoreUtil.evaluationManager.loadAll();
        tblData =new Object[evaluations.size()][this.tblTitle.length];
        for(int i=0;i<evaluations.size();i++){
            for (int j=0;j<this.tblTitle.length;j++) {
                tblData[i][j] = evaluations.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.evaluationTable.validate();
        this.evaluationTable.repaint();
    }

    private void reloadEvaluationTableByUserID(String userid) throws BaseException {
        evaluations = FreshStoreUtil.evaluationManager.loadByUserId(userid);
        if (evaluations==null){
            throw new BaseException("为查询到该用户的评价");
        }
        tblData =new Object[evaluations.size()][this.tblTitle.length];
        for(int i=0;i<evaluations.size();i++){
            for (int j=0;j<this.tblTitle.length;j++) {
                tblData[i][j] = evaluations.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.evaluationTable.validate();
        this.evaluationTable.repaint();
    }

    public FrmUserEvaluationManager(Frame f, String s, boolean b) throws BaseException{
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
//        edtSearch.setText("<-请输入用户编号->");
//        toolBar.add(edtSearch);
       // toolBar.add(btnSearch);
        toolBar.add(btnDel);
        toolBar.add(btnNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);

        this.reloadEvaluationTableByUserID(BeanUser.currentLoginUser.getUser_id());
        this.getContentPane().add(new JScrollPane(this.evaluationTable), BorderLayout.CENTER);

        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnNew.addActionListener(this);
//        this.btnSearch.addActionListener(this);
        this.btnDel.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this.btnSearch){
            try {
                this.reloadEvaluationTableByUserID(String.valueOf(edtSearch.getText()));
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnDel){
            int i=this.evaluationTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择一条评价","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FreshStoreUtil.evaluationManager.delEvaluation(Integer.parseInt(this.evaluationTable.getValueAt(i,0).toString()));
                this.reloadEvaluationTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadEvaluationTableByUserID(BeanUser.currentLoginUser.getUser_id());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
