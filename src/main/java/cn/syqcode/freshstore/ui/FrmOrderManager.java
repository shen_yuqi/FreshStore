package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.MenuManager;
import cn.syqcode.freshstore.control.OrderManager;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmOrderManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnModf = new JButton("修改订单状态");
    private JButton btnSearchById = new JButton("查询");
    private JButton btnNew = new JButton("刷新");
    private JButton btnSearchByStatus = new JButton("查询");

    private JComboBox cmbSearch= new JComboBox(new String[] {"下单", "配送", "送达","退货"});

    private JTextField edtSearch = new JTextField(15);

    private Object tblTitle[]=new BeanOrder().tableTitles;
    private Object tblData[][];

    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable orderTable=new JTable(tablmod);
    List<BeanOrder> orders=null;

    private void reloadOrderTable() throws BaseException {
        orders = (new OrderManager()).loadAll();
        tblData =new Object[orders.size()][this.tblTitle.length];
        for(int i=0;i<orders.size();i++){
            for (int j=0;j<this.tblTitle.length;j++) {
                tblData[i][j] = orders.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.orderTable.validate();
        this.orderTable.repaint();
    }

    private void reloadOrderTableById(String id) throws BaseException {
        BeanOrder orders = (new OrderManager()).searchOrder(id);
        if (orders==null) return;
        tblData =new Object[1][this.tblTitle.length];
        for(int i=0;i<1;i++){
            for (int j=0;j<this.tblTitle.length;j++) {
                tblData[i][j] = orders.getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.orderTable.validate();
        this.orderTable.repaint();
    }

    private void reloadOrderTableByStatus(String status) throws BaseException {
        orders = (new OrderManager()).loadByStatus(status);
        tblData =new Object[orders.size()][this.tblTitle.length];
        for(int i=0;i<orders.size();i++){
            for (int j=0;j<this.tblTitle.length;j++) {
                tblData[i][j] = orders.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.orderTable.validate();
        this.orderTable.repaint();
    }


    private final JScrollPane scroll = new JScrollPane();

    public FrmOrderManager(Frame f, String s, boolean b) throws BaseException{
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnModf);
        edtSearch.setText("<-请输入订单编号->");
        toolBar.add(edtSearch);
        toolBar.add(btnSearchById);
        toolBar.add(cmbSearch);
        toolBar.add(btnSearchByStatus);
        toolBar.add(btnNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadOrderTable();
        this.getContentPane().add(new JScrollPane(this.orderTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnModf.addActionListener(this);
        this.btnSearchById.addActionListener(this);
        this.btnSearchByStatus.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnModf){
            FrmOrderManager_Modf dlg= null;
            int i=this.orderTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择需要修改的订单","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            String id=orderTable.getValueAt(i, 0).toString();
            try {
                dlg = new FrmOrderManager_Modf(this,"修改配送信息",true,id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
            if(dlg.getOrder()!=null){//刷新表格
                try {
                    this.reloadOrderTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if (e.getSource()==this.btnSearchById){
            try {
                this.reloadOrderTableById(this.edtSearch.getText());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnSearchByStatus){
            try {
                this.reloadOrderTableByStatus(this.cmbSearch.getSelectedItem().toString());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadOrderTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
