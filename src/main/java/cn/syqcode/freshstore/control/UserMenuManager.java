package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IUserMenuManager;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserMenuManager implements IUserMenuManager {
    @Override
    public List<BeanMenu> loadAll() throws BaseException {
        List<BeanMenu> result= new ArrayList<BeanMenu>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_menu where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanMenu b=new BeanMenu();
                b.setMenu_id(rs.getString(1));
                b.setMenu_name(rs.getString(2));
                b.setMenu_material(rs.getString(3));
                b.setMenu_step(rs.getString(4));
                b.setMenu_picture(rs.getString(5));
                b.setDeleteflag(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanMenu> searchByName(String name) throws BaseException {
        List<BeanMenu> result= new ArrayList<BeanMenu>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_menu where deleteflag=0 and menu_name='%"+name+"%'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanMenu b=new BeanMenu();
                b.setMenu_id(rs.getString(1));
                b.setMenu_name(rs.getString(2));
                b.setMenu_material(rs.getString(3));
                b.setMenu_step(rs.getString(4));
                b.setMenu_picture(rs.getString(5));
                b.setDeleteflag(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> showGoods(String menu_id) throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT b.*\n" +
                    "FROM menuform a,tbl_goods b,tbl_menu c\n" +
                    "WHERE a.goods_id=b.goods_id AND a.menu_id=c.menu_id AND c.menu_id = ? AND a.deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,menu_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }


}
