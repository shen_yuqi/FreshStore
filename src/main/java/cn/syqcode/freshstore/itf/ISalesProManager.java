package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanSalesPro;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface ISalesProManager {
    public List<BeanSalesPro> loadAll() throws BaseException;

    public void addSalesPro(BeanSalesPro beanSalesPro) throws BaseException;

    public void ModfSalesPro(BeanSalesPro beanSalesPro) throws BaseException;

    public void delSalesPro(String salespro_id) throws BaseException;

    public BeanSalesPro searchBySalesProId(String salesPro_id) throws BaseException;
}
