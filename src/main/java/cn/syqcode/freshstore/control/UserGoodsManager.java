package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IUserGoodsManager;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserGoodsManager implements IUserGoodsManager {
    @Override
    public List<BeanGoods> loadAll() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> loadBySalesPro() throws BaseException {
        return null;
    }

    @Override
    public List<BeanGoods> loadByPriceDesc() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0 order by goods_price DESC";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> loadByPriceAsc() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0 order by goods_price ASC";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> loadByType(String type_name) throws BaseException {
        List<BeanGoods> result = new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT b.type_id\n" +
                    "FROM tbl_goods a,tbl_freshtype b\n" +
                    "WHERE a.type_id=b.type_id AND b.type_name=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,type_name);
            java.sql.ResultSet rs=pst.executeQuery();
            rs.next();
            String id=rs.getString(1);

            sql="select * from tbl_goods where type_id=? and deleteflag=0";
            pst=conn.prepareStatement(sql);
            pst.setString(1,id);
            rs=pst.executeQuery();
            while (rs.next()) {
                BeanGoods bg=new BeanGoods();
                bg.setGoods_id(rs.getString(1));
                bg.setType_id(rs.getString(2));
                bg.setGoods_name(rs.getString(3));
                bg.setGoods_price(rs.getDouble(4));
                bg.setGoods_vipprice(rs.getDouble(5));
                bg.setGoods_quantity(rs.getDouble(6));
                bg.setGoods_specs(rs.getString(7));
                bg.setGoods_desc(rs.getString(8));
                result.add(bg);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> searchByName(String name) throws BaseException {
        List<BeanGoods> result = new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_goods where goods_name='%"+name+"%'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while (rs.next()) {
                BeanGoods bg=new BeanGoods();
                bg.setGoods_id(rs.getString(1));
                bg.setType_id(rs.getString(2));
                bg.setGoods_name(rs.getString(3));
                bg.setGoods_price(rs.getDouble(4));
                bg.setGoods_vipprice(rs.getDouble(5));
                bg.setGoods_quantity(rs.getDouble(6));
                bg.setGoods_specs(rs.getString(7));
                bg.setGoods_desc(rs.getString(8));
                result.add(bg);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanMenu> showMenu(BeanGoods beanGoods) throws BaseException {
        List<BeanMenu> result= new ArrayList<BeanMenu>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_menu where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanMenu b=new BeanMenu();
                b.setMenu_id(rs.getString(1));
                b.setMenu_name(rs.getString(2));
                b.setMenu_material(rs.getString(3));
                b.setMenu_step(rs.getString(4));
                b.setMenu_picture(rs.getString(5));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
