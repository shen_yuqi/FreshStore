package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;

public class FrmPurchaseDetailManager_Detail extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnCancel = new JButton("关闭");

    private JLabel labelFalse = new JLabel("经查询，没有此采购单号，请重新输入！");
    private JLabel labelPurchaseId = new JLabel();
    private JLabel lableGoodsId = new JLabel();
    private JLabel labelGoodsName = new JLabel();
    private JLabel labelGoodsPrice = new JLabel();
    private JLabel labelGoodsVip = new JLabel();
    private JLabel labelSum = new JLabel();
    private JLabel labelEmail = new JLabel();

    private int flag=0;

    public FrmPurchaseDetailManager_Detail(FrmPurchaseDetailManager f, String s, boolean b, String id) throws BaseException {
        super(f, s, b);
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();

            String sql="SELECT b.purchase_id,a.goods_id,a.goods_name,a.goods_price,a.goods_vipprice,b.purchase_quantity,b.purchase_status\n" +
                    "FROM tbl_goods a,purchase_detail b\n" +
                    "WHERE a.goods_id = b.goods_id and  b.purchase_id='"+id+"'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()){
                labelPurchaseId.setText("购物单号："+rs.getString(1));
                lableGoodsId.setText("商品编号："+rs.getString(2));
                labelGoodsName.setText("商品名称："+rs.getString(3));
                labelGoodsPrice.setText("商品单价："+rs.getString(4));
                labelGoodsVip.setText("会员价："+rs.getString(5));
                labelSum.setText("进货量:"+rs.getString(6));
                labelEmail.setText("状态:"+rs.getString(7));

                toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
                toolBar.add(btnCancel);
                this.getContentPane().add(toolBar, BorderLayout.SOUTH);
                workPane.add(labelPurchaseId);
                workPane.add(lableGoodsId);
                workPane.add(labelGoodsName);
                workPane.add(labelGoodsPrice);
                workPane.add(labelGoodsVip);
                workPane.add(labelSum);
                workPane.add(labelEmail);

                workPane.setLayout(null);

                labelPurchaseId.setLocation(20, 20);
                lableGoodsId.setLocation(20, 40);
                labelGoodsName.setLocation(20, 60);
                labelGoodsPrice.setLocation(20, 80);
                labelGoodsVip.setLocation(20, 100);
                labelSum.setLocation(20,120);
                labelEmail.setLocation(20,140);

                labelPurchaseId.setSize(300, 14);
                lableGoodsId.setSize(110, 14);
                labelGoodsName.setSize(110, 14);
                labelGoodsPrice.setSize(110, 14);
                labelGoodsVip.setSize(110, 14);
                labelSum.setSize(110,14);
                labelEmail.setSize(110,14);

                this.getContentPane().add(workPane, BorderLayout.CENTER);
                this.setSize(350, 250);
                // 屏幕居中显示
                double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
                double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
                this.setLocation((int) (width - this.getWidth()) / 2,
                        (int) (height - this.getHeight()) / 2);

                this.validate();

                btnCancel.addActionListener(this);
                this.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });
            }else {
                flag=1;
                toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
                toolBar.add(btnCancel);
                this.getContentPane().add(toolBar, BorderLayout.SOUTH);

                workPane.add(labelFalse);

                this.getContentPane().add(workPane, BorderLayout.CENTER);
                this.setSize(300, 100);
                // 屏幕居中显示
                double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
                double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
                this.setLocation((int) (width - this.getWidth()) / 2,
                        (int) (height - this.getHeight()) / 2);

                this.validate();

                btnCancel.addActionListener(this);
                this.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btnCancel) {
            this.setVisible(false);
        }
    }
}
