package cn.syqcode.freshstore.model;

import java.io.Serializable;

public class BeanMenu {
    public static final String[] tableTitles={"菜谱编号","菜谱名称","菜谱用料","菜谱步骤","菜谱图片"};
    private static final long serialVersionUID = 1L;
    private String menu_id;
    private String menu_name;
    private String menu_material;
    private String menu_step;
    private String menu_picture;
    private long deleteflag;

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_material() {
        return menu_material;
    }

    public void setMenu_material(String menu_material) {
        this.menu_material = menu_material;
    }

    public String getMenu_step() {
        return menu_step;
    }

    public void setMenu_step(String menu_step) {
        this.menu_step = menu_step;
    }

    public String getMenu_picture() {
        return menu_picture;
    }

    public void setMenu_picture(String menu_picture) {
        this.menu_picture = menu_picture;
    }

    public String getCell(int col){
        if(col==0) return menu_id;
        else if(col==1) return menu_name;
        else if(col==2) return menu_material;
        else if(col==3) return menu_step;
        else if (col==4) return menu_picture;
        else return "";
    }
}
