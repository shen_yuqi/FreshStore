package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.OrderManager;
import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserWaitToReceiveManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnSearch = new JButton("查询");
    private JButton btnNew = new JButton("刷新");
    private JTextField edtSearch = new JTextField(15);

    private Object tblTitle[]=new BeanOrder().tableTitles;
    private Object tblData[][];

    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable orderTable=new JTable(tablmod);
    List<BeanOrder> orders=null;

    private void reloadOrderTableByStatus(String status,String id) throws BaseException {
       // orders = (new OrderManager()).loadByStatus(status);
        orders = FreshStoreUtil.orderManager.loadByStatusUserID(status,id);
        tblData =new Object[orders.size()][this.tblTitle.length];
        for(int i=0;i<orders.size();i++){
            for (int j=0;j<this.tblTitle.length;j++) {
                tblData[i][j] = orders.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.orderTable.validate();
        this.orderTable.repaint();
    }

    private void reloadOrderTableById(String id) throws BaseException {
        BeanOrder orders = (new OrderManager()).searchOrder(id);
        try{
            if (orders==null) return;
            if (!"配送".equals(orders.getOrd_status())){
                throw new BaseException("查无此订单");
            }else {
                tblData =new Object[1][this.tblTitle.length];
                for(int i=0;i<1;i++){
                    for (int j=0;j<this.tblTitle.length;j++) {
                        tblData[i][j] = orders.getCell(j);
                    }
                }
            }
            tablmod.setDataVector(tblData,tblTitle);
            this.orderTable.validate();
            this.orderTable.repaint();
        }catch (BaseException e1){
            JOptionPane.showMessageDialog(null,  e1.getMessage(),"提示",JOptionPane.ERROR_MESSAGE);
        }
    }

    private final JScrollPane scroll = new JScrollPane();

    public FrmUserWaitToReceiveManager(Frame f, String s, boolean b) throws BaseException{
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        edtSearch.setText("<-请输入订单编号->");
        toolBar.add(edtSearch);
        toolBar.add(btnSearch);
        toolBar.add(btnNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadOrderTableByStatus("配送", BeanUser.currentLoginUser.getUser_id());
        this.getContentPane().add(new JScrollPane(this.orderTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnNew.addActionListener(this);
        this.btnSearch.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this.btnSearch){
            try {
                this.reloadOrderTableById(String.valueOf(edtSearch.getText()));
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadOrderTableByStatus("配送", BeanUser.currentLoginUser.getUser_id());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
