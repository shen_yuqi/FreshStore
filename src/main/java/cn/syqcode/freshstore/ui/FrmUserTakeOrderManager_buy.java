package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanCut;
import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FrmUserTakeOrderManager_buy extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("下单");
    private JButton btnCancel = new JButton("关闭");

    private JLabel labelOldMoney = new JLabel();
    private JLabel lableVipCut = new JLabel();
    private JLabel labelSalesPro = new JLabel();
    private JLabel labelDiscount = new JLabel();
    private JLabel labelCoupon = new JLabel();
    private JLabel labelCut = new JLabel();
    private JLabel labelNewMoney = new JLabel();
    private JLabel labelDate =new JLabel("送达时间:");

    private JTextField edtDate =new JTextField(10);

    private int flag=0;
    BeanOrder bo=null;

    public FrmUserTakeOrderManager_buy(FrmUserTakeOrderManager f, String s, boolean b, BeanOrder beanOrder) throws BaseException {
        super(f, s, b);
        bo=beanOrder;
        FreshStoreUtil.orderManager.buyOrder(beanOrder);
        BeanCut beanCut=FreshStoreUtil.cutManager.searchByOrderID(beanOrder.getOrd_id());

        labelOldMoney.setText("原价："+(double) Math.round(beanOrder.getOrd_oldmoney() * 100) / 100);
        lableVipCut.setText("会员优惠："+(double) Math.round(beanCut.getCut1() * 100) / 100);
        labelSalesPro.setText("限时促销优惠："+(double) Math.round(beanCut.getCut2() * 100) / 100);
        labelDiscount.setText("折扣优惠："+(double) Math.round(beanCut.getCut3() * 100) / 100);
        labelCoupon.setText("优惠券："+(double) Math.round(beanCut.getCut4() * 100) / 100);
        labelCut.setText("总共优惠:"+(double) Math.round(beanCut.getCut() * 100) / 100);
        labelNewMoney.setText("实付:"+(double) Math.round(beanOrder.getOrd_newmoney() * 100) / 100);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelOldMoney);
        workPane.add(lableVipCut);
        workPane.add(labelSalesPro);
        workPane.add(labelDiscount);
        workPane.add(labelCoupon);
        workPane.add(labelCut);
        workPane.add(labelNewMoney);
        workPane.add(labelDate);
        workPane.add(edtDate);

        workPane.setLayout(null);
        labelOldMoney.setLocation(20, 20);
        lableVipCut.setLocation(20, 40);
        labelSalesPro.setLocation(20, 60);
        labelDiscount.setLocation(20, 80);
        labelCoupon.setLocation(20, 100);
        labelCut.setLocation(20,120);
        labelNewMoney.setLocation(20,140);
        labelDate.setLocation(20,160);edtDate.setLocation(90,160);

        labelOldMoney.setSize(300, 14);
        lableVipCut.setSize(110, 14);
        labelSalesPro.setSize(110, 14);
        labelDiscount.setSize(110, 14);
        labelCoupon.setSize(110, 14);
        labelCut.setSize(110,14);
        labelNewMoney.setSize(110,14);
        labelDate.setSize(70,14);edtDate.setSize(70,20);
        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(170, 280);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        btnOk.addActionListener(this);
        btnCancel.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this.btnOk){
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long time=0;
                if (!"".equals(this.edtDate.getText())){
                    Date date = null;
                    try {
                        date = simpleDateFormat.parse(this.edtDate.getText());
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                    long ts = date.getTime();
                    time = ts;
                    bo.setOrd_time(time);
                    FreshStoreUtil.orderManager.buyOrder(bo);
                }else {
                    throw new BaseException("请输入送达时间");
                }

                FreshStoreUtil.orderManager.ModfOrderStatu(bo.getOrd_id(),"下单");
                FrmUserTakeOrderManager_buy_Addr dlg=new FrmUserTakeOrderManager_buy_Addr(this,"选择收货地址",true,bo.getOrd_id());
                dlg.setVisible(true);
                JOptionPane.showMessageDialog(null,  "已完成下单","提示",JOptionPane.INFORMATION_MESSAGE);
                this.setVisible(false);
            } catch (BaseException ex) {
                JOptionPane.showMessageDialog(null,  ex.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }else if (e.getSource() == this.btnCancel) {
            this.setVisible(false);
        }
    }
}
