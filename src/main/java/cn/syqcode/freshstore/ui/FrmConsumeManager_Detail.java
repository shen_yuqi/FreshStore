package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.OrderManager;
import cn.syqcode.freshstore.control.PurchaseDetail;
import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class FrmConsumeManager_Detail extends JDialog implements ActionListener {
    private Object tblTitle[]={"用户编码","原始金额","结算金额","使用优惠券编号","要求送达时间","配送地址编号","订单状态"};
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable consumeDetailTable=new JTable(tablmod);
    List<BeanOrder> orders=null;
    String userid;
    private void reloadconsumeDetailTable(String id) throws BaseException {
        try{
            //orders = (new OrderManager()).loadAll();
            orders = FreshStoreUtil.orderManager.loadByUserID(id);
            tblData =new Object[orders.size()][7];
            for(int i=0;i<orders.size();i++){
                tblData[i][0]=orders.get(i).getUser_id();
                tblData[i][1]=orders.get(i).getOrd_oldmoney();
                tblData[i][2]=orders.get(i).getOrd_newmoney();
                tblData[i][3]=orders.get(i).getCoupon_id();
                tblData[i][4]=orders.get(i).getOrd_time();
                tblData[i][5]=orders.get(i).getAddr_id();
                tblData[i][6]=orders.get(i).getOrd_status();
            }
            tablmod.setDataVector(tblData,tblTitle);
            this.consumeDetailTable.validate();
            this.consumeDetailTable.repaint();
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public FrmConsumeManager_Detail(FrmConsumeManager f, String s, boolean b, String id) throws BaseException {
        super(f, s, b);
        userid = id;
        //提取现有数据
        this.reloadconsumeDetailTable(id);
        this.getContentPane().add(new JScrollPane(this.consumeDetailTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
