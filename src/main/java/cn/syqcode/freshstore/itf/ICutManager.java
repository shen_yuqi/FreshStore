package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanCut;
import cn.syqcode.freshstore.util.BaseException;

public interface ICutManager {
    public BeanCut searchByOrderID(String ord_id) throws BaseException;
}
