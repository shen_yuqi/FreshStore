package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.model.OrderDetail;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IOrderManager {
    public List<BeanOrder> loadAll() throws BaseException;

    public BeanOrder searchOrder(String order_id)  throws BaseException;

    public List<BeanOrder> loadByUserID(String user_id) throws BaseException;

    public List<BeanOrder> loadByStatus(String statu) throws BaseException;

    public List<BeanOrder> loadByStatusUserID(String statu,String user_id) throws BaseException;

    public void delOrder(BeanOrder beanOrder) throws BaseException;

    public void ModfOrderStatu(String ord_id,String statu) throws BaseException;

    public List<OrderDetail> showOrderDetail(String ord_id) throws BaseException;

    public OrderDetail ModfOrderDetail(OrderDetail orderDetail) throws BaseException;

    public void delOrderDetail(int no) throws BaseException;

    public List<String> searchOrderIdByUserId(String status,String user_id) throws BaseException;

    public List<OrderDetail> showOrderDetailNoEval(String ord_id) throws BaseException;

    public void buyOrder(BeanOrder beanOrder) throws BaseException;

    public void ModfOrderAddr(String addr_id,String ord_id) throws BaseException;

}
