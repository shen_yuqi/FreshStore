package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IFreshTypeManager;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FreshTypeManager implements IFreshTypeManager {
    @Override
    public List<BeanFreshType> loadAll() throws BaseException {
        List<BeanFreshType> result=new ArrayList<BeanFreshType>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_freshtype where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanFreshType ft=new BeanFreshType();
                ft.setType_id(rs.getString(1));
                ft.setType_name(rs.getString(2));
                ft.setType_desc(rs.getString(3));
                ft.setDeleteflag(rs.getLong(4));
                result.add(ft);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addFreshType(BeanFreshType beanFreshType) throws BaseException {
        if("".equals(beanFreshType.getType_name())){
            throw new BusinessException("请输入类别名称");
        }
        if("".equals(beanFreshType.getType_desc())){
            throw new BusinessException("请对生鲜类别进行描述");
        }

        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql = "select * from tbl_freshtype where deleteflag=0 and type_name = '"+beanFreshType.getType_name()+"'";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if(rs.next()) throw new BusinessException("类别已存在");
            rs.close();
            st.close();
            sql = "insert into tbl_freshtype values(?,?,?,0)";
            PreparedStatement pst=conn.prepareStatement(sql);
            //生成唯一用户编码
            UUID uuid = UUID.randomUUID();
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,beanFreshType.getType_name());
            pst.setString(3,beanFreshType.getType_desc());
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void ModfFreshType(BeanFreshType beanFreshType,String oldName) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select * from tbl_freshtype where deleteflag=0 and type_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,beanFreshType.getType_id());
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next()) throw new BusinessException("该类别不存在");
            rs.close();
            pst.close();
            sql = "select * from tbl_freshtype where deleteflag=0 and type_name = '"+beanFreshType.getType_name()+"' and type_name not like '"+oldName+"'";
            Statement st = conn.createStatement();
            rs = st.executeQuery(sql);
            if(rs.next()) throw new BusinessException("类别已存在");
            rs.close();
            st.close();
            sql="update tbl_freshtype set type_name = ? , type_desc=? where deleteflag=0 and type_id=?";
            pst=conn.prepareStatement(sql);
            pst.setString(1, beanFreshType.getType_name());
            pst.setString(2, beanFreshType.getType_desc());
            pst.setString(3,beanFreshType.getType_id());
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void delFreshType(String type_id) throws BaseException {
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select * from tbl_goods where deleteflag=0 and type_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,type_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if(!rs.next())
            {
                sql="update tbl_freshtype set deleteflag=? where type_id=?";
                pst=conn.prepareStatement(sql);
                pst.setLong(1,System.currentTimeMillis());
                pst.setString(2, type_id);
                pst.execute();
                pst.close();
            }
            else
            {
                throw new BusinessException("该类别下有商品信息，无法删除");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public String searchTypeById(String id) throws BaseException {
        String result=null;
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select type_name from tbl_freshtype where deleteflag=0 and type_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,id);
            java.sql.ResultSet rs=pst.executeQuery();
            if(rs.next())
            {
                result=rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public String searchTypeIdByName(String name) throws BaseException {
        String result=null;
        Connection conn=null;
        try {
            conn=DBUtil.getConnection();
            String sql="select type_id from tbl_freshtype where deleteflag=0 and type_name=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,name);
            java.sql.ResultSet rs=pst.executeQuery();
            if(rs.next())
            {
                result=rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
