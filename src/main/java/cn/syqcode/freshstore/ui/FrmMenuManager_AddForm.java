package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.MenuFormManager;
import cn.syqcode.freshstore.control.MenuManager;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.model.MenuForm;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmMenuManager_AddForm extends JDialog implements ActionListener {
    String id;
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加");
    private JButton btnModf = new JButton("修改");
    private JButton btnDelete = new JButton("删除");
    private Object tblTitle[]=new MenuForm().tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable menusTable=new JTable(tablmod);
    List<MenuForm> menuForms=null;
    private void reloadMenuFormTable(String id) throws BaseException{
        List<MenuForm> menuForms=(new MenuFormManager()).loadAllByMenuId(id);
        tblData =new Object[menuForms.size()][MenuForm.tableTitles.length];
        for(int i=0;i<menuForms.size();i++){
            for (int j=0;j<MenuForm.tableTitles.length;j++){
                tblData[i][j]=menuForms.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.menusTable.validate();
        this.menusTable.repaint();
    }
    public FrmMenuManager_AddForm(FrmMenuManager f, String s, boolean b, String menu_id) throws BaseException{
        super(f,s,b);
        id=menu_id;
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnModf);
        toolBar.add(btnDelete);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadMenuFormTable(menu_id);
        this.getContentPane().add(new JScrollPane(this.menusTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(400, 300);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnModf.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmMenuManager_AddForm_Add dlg=new FrmMenuManager_AddForm_Add(this,"添加商品至菜谱推荐表",true);
            dlg.setVisible(true);
            if(dlg.getMenuForm()!=null){//刷新表格
                try {
                    this.reloadMenuFormTable(id);
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnModf){
            FrmMenuManager_AddForm_Modf dlg= null;
            int i=this.menusTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择菜谱","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            int no=Integer.parseInt(menusTable.getValueAt(i, 0).toString());
            try {
                dlg = new FrmMenuManager_AddForm_Modf(this,"修改菜谱信息",true,no);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
            if(dlg.getMenuForm()!=null){//刷新表格
                try {
                    this.reloadMenuFormTable(id);
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnDelete){
            int i=this.menusTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择菜谱","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除该菜谱中的商品吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                List<MenuForm> menuForms= null;
                int no=Integer.parseInt(this.menusTable.getValueAt(i,0).toString());
                try {
                    (new MenuFormManager()).delMenuForm(no);
                    this.reloadMenuFormTable(id);
                } catch (BaseException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
