package cn.syqcode.freshstore.model;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.util.BaseException;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderDetail {
    public static final String[] tableTitles={"订单序号","商品名称","数量","单价","总价"};

    private static final long serialVersionUID = 1L;
    private int no;
    private String goods_id;
    private String ord_id;
    private String discount_id;
    private double orddetail_quantity;
    private double goods_price;
    private double discount_detail;
    private long deleteflag;

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public OrderDetail() {
    }

    public int getNo() {
        return no;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getOrd_id() {
        return ord_id;
    }

    public void setOrd_id(String ord_id) {
        this.ord_id = ord_id;
    }

    public String getDiscount_id() {
        return discount_id;
    }

    public void setDiscount_id(String discount_id) {
        this.discount_id = discount_id;
    }

    public double getOrddetail_quantity() {
        return orddetail_quantity;
    }

    public void setOrddetail_quantity(double orddetail_quantity) {
        this.orddetail_quantity = orddetail_quantity;
    }

    public double getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(double goods_price) {
        this.goods_price = goods_price;
    }

    public double getDiscount_detail() {
        return discount_detail;
    }

    public void setDiscount_detail(double discount_detail) {
        this.discount_detail = discount_detail;
    }

    public String getCell(int col) throws BaseException {
        if(col==0) return String.valueOf(no);
        else if(col==1) return FreshStoreUtil.goodsManager.searchGoodsById(goods_id).getGoods_name();
        else if(col==2) return String.valueOf(orddetail_quantity);
        else if(col==3) return String.valueOf(goods_price);
        else if (col==4) return String.valueOf(goods_price*orddetail_quantity);
        else return "";
    }

}
