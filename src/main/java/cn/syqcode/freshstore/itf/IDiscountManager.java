package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IDiscountManager {
    public List<BeanDiscount> loadAll() throws BaseException;

    public void addDiscount(BeanDiscount beanDiscount) throws BaseException;

    public void ModfDiscount(BeanDiscount beanDiscount) throws BaseException;

    public void delDiscount(String id) throws BaseException;

    public BeanDiscount searchByDiscountId(String id) throws BaseException;
}
