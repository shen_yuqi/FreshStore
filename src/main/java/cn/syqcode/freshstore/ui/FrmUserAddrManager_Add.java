package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanAddr;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmUserAddrManager_Add extends JDialog implements ActionListener {
    private BeanAddr addr=null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelUser = new JLabel("姓名：");
    private JLabel labelPhone = new JLabel("电话:");
    private JLabel labelProvince = new JLabel("省:");
    private JLabel labelCity = new JLabel("市:");
    private JLabel labelArea = new JLabel("区:");
    private JLabel labelAddr =new JLabel("地址：");

    private JTextField edtUser = new JTextField(22);
    private JTextField edtPhone = new JTextField(22);
    private JTextField edtProvince = new JTextField(22);
    private JTextField edtCity = new JTextField(22);
    private JTextField edtArea = new JTextField(22);
    private JTextField edtAddr = new JTextField(22);

    public FrmUserAddrManager_Add(JDialog f, String s, boolean b) {
        super(f, s, b);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelUser);
        workPane.add(edtUser);
        workPane.add(labelPhone);
        workPane.add(edtPhone);
        workPane.add(labelProvince);
        workPane.add(edtProvince);
        workPane.add(labelCity);
        workPane.add(edtCity);
        workPane.add(labelArea);
        workPane.add(edtArea);
        workPane.add(labelAddr);
        workPane.add(edtAddr);

        workPane.setLayout(null);

        labelUser.setLocation(20, 20); edtUser.setLocation(80,20);
        labelPhone.setLocation(20, 40); edtPhone.setLocation(80,40);
        labelProvince.setLocation(20, 60); edtProvince.setLocation(80,60);
        labelCity.setLocation(20, 80); edtCity.setLocation(80,80);
        labelArea.setLocation(20,100);edtArea.setLocation(80,100);
        labelAddr.setLocation(20,120);edtAddr.setLocation(80,120);

        labelUser.setSize(100, 14); edtUser.setSize(200,20);
        labelPhone.setSize(100, 14); edtPhone.setSize(200,20);
        labelProvince.setSize(100, 14);edtProvince.setSize(200,20);
        labelCity.setSize(100, 14);edtCity.setSize(200,20);
        labelArea.setSize(100,14);edtArea.setSize(200,20);
        labelAddr.setSize(100,14);edtAddr.setSize(200,20);


        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(320, 250);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            addr = new BeanAddr();
            addr.setUser_id(BeanUser.currentLoginUser.getUser_id());
            addr.setAddr_people(edtUser.getText());
            addr.setAddr_phone(edtPhone.getText());
            addr.setAddr_province(edtProvince.getText());
            addr.setAddr_city(edtCity.getText());
            addr.setAddr_area(edtArea.getText());
            addr.setAddr_addr(edtAddr.getText());
            try {
                FreshStoreUtil.userAddrManager.addAddr(addr);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.addr=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanAddr getAddr(){return addr;}
}
