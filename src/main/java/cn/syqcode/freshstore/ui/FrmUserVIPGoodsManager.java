package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.control.GoodsManager;
import cn.syqcode.freshstore.control.MenuManager;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.model.BeanMenu;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserVIPGoodsManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    //private JButton btnSearch = new JButton("查询");;
    private JButton btnMenu = new JButton("菜谱查询");
    private JButton btnNew = new JButton("刷新");
    private JButton btnBuy = new JButton("购买商品");


    private Object tblTitle[]={"商品编号","商品名称","商品类别","商品单价","会员价","规格","描述","总销量","库存量"};
    private Object tblData[][];
    List<BeanGoods> goods=null;

    private Object tblTitle1[]={"菜谱编号","菜谱名称","菜谱用料","菜谱步骤","菜谱图片"};
    private Object tblData1[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable goodsTable=new JTable(tablmod);

    private final JScrollPane scroll = new JScrollPane();

    private void reloadGoodsTable() throws BaseException {
        goods=(new GoodsManager()).loadAllVIPGoods();
        tblData =new Object[goods.size()][9];
        for(int i=0;i<goods.size();i++){
            tblData[i][0]=goods.get(i).getGoods_id();
            tblData[i][1]=goods.get(i).getGoods_name();
            tblData[i][2]=(new FreshTypeManager()).searchTypeById(goods.get(i).getType_id());
            tblData[i][3]=goods.get(i).getGoods_price();
            tblData[i][4]=goods.get(i).getGoods_vipprice();
            tblData[i][5]=goods.get(i).getGoods_specs();
            tblData[i][6]=goods.get(i).getGoods_desc();
            tblData[i][7]=(new GoodsManager()).countSales(goods.get(i).getGoods_id());
            tblData[i][8]=goods.get(i).getGoods_quantity();
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.goodsTable.validate();
        this.goodsTable.repaint();
    }

    private void reloadMenuTable(String goods_id) throws BaseException{
        List<BeanMenu> beanMenus=(new MenuManager()).loadByGoodsId(goods_id);
        tblData1 =new Object[beanMenus.size()][5];
        for(int i=0;i<beanMenus.size();i++){
            tblData1[i][0]=beanMenus.get(i).getMenu_id();
            tblData1[i][1]=beanMenus.get(i).getMenu_name();
            tblData1[i][2]=beanMenus.get(i).getMenu_material();
            tblData1[i][3]=beanMenus.get(i).getMenu_step();
            tblData1[i][4]=beanMenus.get(i).getMenu_picture();
        }
        tablmod.setDataVector(tblData1,tblTitle1);
        this.goodsTable.validate();
        this.goodsTable.repaint();
    }
    public FrmUserVIPGoodsManager(Frame f, String s, boolean b) throws BaseException {
        super(f, s, b);

        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));

        //toolBar.add(btnSearch);
        toolBar.add(btnMenu);
        toolBar.add(btnBuy);
        toolBar.add(btnNew);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadGoodsTable();
        this.getContentPane().add(new JScrollPane(this.goodsTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        //this.btnSearch.addActionListener(this);
        this.btnMenu.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.btnBuy.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
         if (e.getSource()==this.btnMenu){
            int i=this.goodsTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            String id=goodsTable.getValueAt(i, 0).toString();
            try {
                this.reloadMenuTable(id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnNew){
            try {
                this.reloadGoodsTable();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.btnBuy){
            int i=this.goodsTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FrmUserVIPGoodsManager_BuyGoods dlg=new FrmUserVIPGoodsManager_BuyGoods(this,"购买商品",true,goodsTable.getValueAt(i, 0).toString());
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
