package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IDiscountManager;
import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DiscountManager implements IDiscountManager {
    @Override
    public List<BeanDiscount> loadAll() throws BaseException {
        List<BeanDiscount> result= new ArrayList<BeanDiscount>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_discount where discount_enddate>?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanDiscount b=new BeanDiscount();
                b.setDiscount_id(rs.getString(1));
                b.setDiscount_content(rs.getString(2));
                b.setDiscount_limits(rs.getDouble(3));
                b.setDiscount_detail(rs.getDouble(4));
                b.setDiscount_startdate(rs.getLong(5));
                b.setDiscount_enddate(rs.getLong(6));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addDiscount(BeanDiscount beanDiscount) throws BaseException {
        if ("".equals(beanDiscount.getDiscount_content())){
            throw new BusinessException("请输入满折信息内容。");
        }
        if (beanDiscount.getDiscount_limits()<=0){
            throw new BusinessException("请输入正确的满折适用商品数量。");
        }
        if (beanDiscount.getDiscount_detail()<=0||beanDiscount.getDiscount_detail()>1){
            throw new BusinessException("请输入正确的折扣。");
        }
        if (beanDiscount.getDiscount_startdate()==0){
            throw new BusinessException("请输入满折优惠开始的日期。");
        }
        if (beanDiscount.getDiscount_enddate()==0){
            throw new BusinessException("请输入满折优惠结束的日期。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into tbl_discount values (?,?,?,?,?,?)";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,beanDiscount.getDiscount_content());
            pst.setDouble(3,beanDiscount.getDiscount_limits());
            pst.setDouble(4,beanDiscount.getDiscount_detail());
            pst.setLong(5,beanDiscount.getDiscount_startdate());
            pst.setLong(6,beanDiscount.getDiscount_enddate());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void ModfDiscount(BeanDiscount beanDiscount) throws BaseException {
        if ("".equals(beanDiscount.getDiscount_content())){
            throw new BusinessException("请输入满折信息内容。");
        }
        if (beanDiscount.getDiscount_limits()<=0){
            throw new BusinessException("请输入正确的满折适用商品数量。");
        }
        if (beanDiscount.getDiscount_detail()<=0||beanDiscount.getDiscount_detail()>1){
            throw new BusinessException("请输入正确的折扣。");
        }
        if (beanDiscount.getDiscount_startdate()==0){
            throw new BusinessException("请输入满折优惠开始的日期。");
        }
        if (beanDiscount.getDiscount_enddate()==0){
            throw new BusinessException("请输入满折优惠结束的日期。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_discount set discount_content=?,discount_limits=?,discount_detail=?,discount_startdate=?,discount_enddate=? where discount_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,beanDiscount.getDiscount_content());
            pst.setDouble(2,beanDiscount.getDiscount_limits());
            pst.setDouble(3,beanDiscount.getDiscount_detail());
            pst.setLong(4,beanDiscount.getDiscount_startdate());
            pst.setLong(5,beanDiscount.getDiscount_enddate());
            pst.setString(6,beanDiscount.getDiscount_id());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void delDiscount(String id) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_discount set discount_enddate=? where discount_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,id);
            pst.execute();
            pst.close();
            sql = "update discount set discount_enddate=? where discount_id=?";
            pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,id);
            pst.execute();
            pst.close();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public BeanDiscount searchByDiscountId(String id) throws BaseException {
        BeanDiscount b= new BeanDiscount();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_discount where discount_enddate>? and discount_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                b.setDiscount_id(rs.getString(1));
                b.setDiscount_content(rs.getString(2));
                b.setDiscount_limits(rs.getDouble(3));
                b.setDiscount_detail(rs.getDouble(4));
                b.setDiscount_startdate(rs.getLong(5));
                b.setDiscount_enddate(rs.getLong(6));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return b;
    }
}
