package cn.syqcode.freshstore.model;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.util.BaseException;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Evaluation {
    public static final String[] tableTitles={"序号","用户编号","商品名称","评价","星级","图片","评价日期"};
    private static final long serialVersionUID = 1L;
    private int no;
    private String user_id;
    private String goods_id;
    private String evaluation_content;
    private long evaluation_date;
    private  String evaluation_rank;
    private String evaluation_picture;
    private long deleteflag;

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    public int getNo() {
        return no;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEvaluation_content() {
        return evaluation_content;
    }

    public void setEvaluation_content(String evaluation_content) {
        this.evaluation_content = evaluation_content;
    }

    public long getEvaluation_date() {
        return evaluation_date;
    }

    public void setEvaluation_date(long evaluation_date) {
        this.evaluation_date = evaluation_date;
    }

    public String getEvaluation_rank() {
        return evaluation_rank;
    }

    public void setEvaluation_rank(String evaluation_rank) {
        this.evaluation_rank = evaluation_rank;
    }

    public String getEvaluation_picture() {
        return evaluation_picture;
    }

    public void setEvaluation_picture(String evaluation_picture) {
        this.evaluation_picture = evaluation_picture;
    }

    public String getCell(int col) throws BaseException {
        if(col==0) return String.valueOf(no);
        else if(col==1) return user_id;
        else if (col==2) return FreshStoreUtil.goodsManager.searchGoodsById(goods_id).getGoods_name();
        else if (col==3) return evaluation_content;
        else if (col==4) return evaluation_rank;
        else if (col==5) return evaluation_picture;
        else if (col==6) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(evaluation_date);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else return "";
    }

}
