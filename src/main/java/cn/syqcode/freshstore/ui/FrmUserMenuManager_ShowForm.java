package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.MenuFormManager;
import cn.syqcode.freshstore.model.MenuForm;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserMenuManager_ShowForm extends JDialog implements ActionListener {
    String id;
    private JPanel toolBar = new JPanel();
    private JButton btnShow = new JButton("查看相关商品");
    private JButton btnNew = new JButton("刷新");
    private Object tblTitle[]=new MenuForm().tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable menusTable=new JTable(tablmod);
    List<MenuForm> menuForms=null;
    private void reloadMenuFormTable(String id) throws BaseException {
        List<MenuForm> menuForms=(new MenuFormManager()).loadAllByMenuId(id);
        tblData =new Object[menuForms.size()][MenuForm.tableTitles.length];
        for(int i=0;i<menuForms.size();i++){
            for (int j=0;j<MenuForm.tableTitles.length;j++){
                tblData[i][j]=menuForms.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.menusTable.validate();
        this.menusTable.repaint();
    }


    public FrmUserMenuManager_ShowForm(FrmUserMenuManager f, String s, boolean b, String menu_id) throws BaseException{
        super(f,s,b);
        id=menu_id;
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnShow);
        toolBar.add(btnNew);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadMenuFormTable(menu_id);
        this.getContentPane().add(new JScrollPane(this.menusTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(400, 300);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnShow.addActionListener(this);
        this.btnNew.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnShow){
            int i=this.menusTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择菜谱","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FrmUserMenuManager_ShowForm_showGoods dlg=new FrmUserMenuManager_ShowForm_showGoods(this,"商品详细",true,Integer.parseInt(menusTable.getValueAt(i, 0).toString()));
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if(e.getSource()==this.btnNew){
            try {
                this.reloadMenuFormTable(id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
