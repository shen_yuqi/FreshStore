package cn.syqcode.freshstore.model;

import java.io.Serializable;

public class BeanUser {
    private static final long serialVersionUID = 1L;
    public static BeanUser currentLoginUser=null;
    private String user_id;
    private String user_name;
    private String user_sex;
    private String user_password;
    private String user_phone;
    private String user_email;

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    private String user_city;
    private long user_regdate;
    private String user_isvip;
    private long user_vipend;
    private long deleteflag;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_city() {
        return user_city;
    }

    public void setUser_city(String user_city) {
        this.user_city = user_city;
    }

    public long getUser_regdate() {
        return user_regdate;
    }

    public void setUser_regdate(long user_regdate) {
        this.user_regdate = user_regdate;
    }

    public String getUser_isvip() {
        return user_isvip;
    }

    public void setUser_isvip(String user_isvip) {
        this.user_isvip = user_isvip;
    }

    public long getUser_vipend() {
        return user_vipend;
    }

    public void setUser_vipend(long user_vipend) {
        this.user_vipend = user_vipend;
    }
}
