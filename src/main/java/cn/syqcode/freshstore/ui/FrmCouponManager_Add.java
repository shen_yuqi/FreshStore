package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.CouponManager;
import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.control.GoodsManager;
import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FrmCouponManager_Add extends JDialog implements ActionListener {
    private BeanCoupon coupon=null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelName = new JLabel("内容：");
    private JLabel lablePrice = new JLabel("适用金额：");
    private JLabel labelCut = new JLabel("免减金额：");
    private JLabel labelStart = new JLabel("开始日期：");
    private JLabel labelEnd = new JLabel("结束日期:");

    private JTextField edtGoodsName = new JTextField(20);
    private JTextField edtPrice = new JTextField(20);
    private JTextField edtCut = new JTextField(20);
    private JTextField edtStart = new JTextField(20);
    private JTextField edtEnd = new JTextField(20);

    public FrmCouponManager_Add(JDialog f, String s, boolean b) {
        super(f,s,b);
        edtPrice.setText("0");
        edtCut.setText("0");

        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelName);
        workPane.add(edtGoodsName);
        workPane.add(lablePrice);
        workPane.add(edtPrice);
        workPane.add(labelCut);
        workPane.add(edtCut);
        workPane.add(labelStart);
        workPane.add(edtStart);
        workPane.add(labelEnd);
        workPane.add(edtEnd);

        workPane.setLayout(null);

        labelName.setLocation(20, 20); edtGoodsName.setLocation(100,20);
        lablePrice.setLocation(20, 40); edtPrice.setLocation(100,40);
        labelCut.setLocation(20, 60); edtCut.setLocation(100,60);
        labelStart.setLocation(20, 80); edtStart.setLocation(100,80);
        labelEnd.setLocation(20,100);edtEnd.setLocation(100,100);

        labelName.setSize(70, 14); edtGoodsName.setSize(200,20);
        lablePrice.setSize(70, 14); edtPrice.setSize(200,20);
        labelCut.setSize(70, 14);edtCut.setSize(200,20);
        labelStart.setSize(70, 14);edtStart.setSize(200,20);
        labelEnd.setSize(70,14);edtEnd.setSize(200,20);
        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(350, 270);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            coupon=new BeanCoupon();
            String content = this.edtGoodsName.getText();
            double price = Double.parseDouble(this.edtPrice.getText());
            double cut = Double.parseDouble(this.edtCut.getText());
            long start=0;
            if (!"".equals(this.edtStart.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtStart.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                start = ts;
            }
            long end = 0;
            if (!"".equals(this.edtEnd.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtEnd.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                end = ts;
            }

            coupon.setCoupon_content(content);
            coupon.setCoupon_limits(price);
            coupon.setCoupon_cut(cut);
            coupon.setCoupon_startdate(start);
            coupon.setCoupon_enddate(end);
            try {
                (new CouponManager()).addCoupon(coupon);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.coupon=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanCoupon getCoupon(){return coupon;}

}
