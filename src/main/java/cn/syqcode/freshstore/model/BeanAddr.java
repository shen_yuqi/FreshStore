package cn.syqcode.freshstore.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BeanAddr {
    public static final String[] tableTitles={"地址编号","收货人","手机号码","省","市","区","地址"};

    private static final long serialVersionUID = 1L;
    private String addr_id;
    private String user_id;
    private String addr_province;
    private String addr_city;
    private String addr_area;
    private String addr_addr;
    private String addr_people;
    private String addr_phone;
    private long deleteflag;

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getAddr_id() {
        return addr_id;
    }

    public void setAddr_id(String addr_id) {
        this.addr_id = addr_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAddr_province() {
        return addr_province;
    }

    public void setAddr_province(String addr_province) {
        this.addr_province = addr_province;
    }

    public String getAddr_city() {
        return addr_city;
    }

    public void setAddr_city(String addr_city) {
        this.addr_city = addr_city;
    }

    public String getAddr_area() {
        return addr_area;
    }

    public void setAddr_area(String addr_area) {
        this.addr_area = addr_area;
    }

    public String getAddr_addr() {
        return addr_addr;
    }

    public void setAddr_addr(String addr_addr) {
        this.addr_addr = addr_addr;
    }

    public String getAddr_people() {
        return addr_people;
    }

    public void setAddr_people(String addr_people) {
        this.addr_people = addr_people;
    }

    public String getAddr_phone() {
        return addr_phone;
    }

    public void setAddr_phone(String addr_phone) {
        this.addr_phone = addr_phone;
    }

    public String getCell(int col){
        if (col==0) return addr_id;
        else if(col==1) return addr_people;
        else if(col==2) return addr_phone;
        else if(col==3) return addr_province;
        else if(col==4) return addr_city;
        else if (col==5) return addr_area;
        else if (col==6) return addr_addr;
        else return "";
    }
}
