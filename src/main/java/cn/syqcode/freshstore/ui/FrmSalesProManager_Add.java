package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.model.BeanSalesPro;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FrmSalesProManager_Add extends JDialog implements ActionListener {
    private BeanSalesPro salesPro =null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelGoodsId = new JLabel("商品编号：");
    private JLabel lablePrice = new JLabel("促销价格：");
    private JLabel labelSum = new JLabel("促销数量：");
    private JLabel labelStart = new JLabel("开始日期：");
    private JLabel labelEnd = new JLabel("结束日期:");

    private JTextField edtGoodsId = new JTextField(20);
    private JTextField edtPrice = new JTextField(20);
    private JTextField edtSum = new JTextField(20);
    private JTextField edtStart = new JTextField(20);
    private JTextField edtEnd = new JTextField(20);

    public FrmSalesProManager_Add(JDialog f, String s, boolean b) {
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelGoodsId);
        workPane.add(edtGoodsId);
        workPane.add(lablePrice);
        workPane.add(edtPrice);
        workPane.add(labelSum);
        workPane.add(edtSum);
        workPane.add(labelStart);
        workPane.add(edtStart);
        workPane.add(labelEnd);
        workPane.add(edtEnd);

        workPane.setLayout(null);

        labelGoodsId.setLocation(20, 20); edtGoodsId.setLocation(100,20);
        lablePrice.setLocation(20, 40); edtPrice.setLocation(100,40);
        labelSum.setLocation(20, 60); edtSum.setLocation(100,60);
        labelStart.setLocation(20, 80); edtStart.setLocation(100,80);
        labelEnd.setLocation(20,100);edtEnd.setLocation(100,100);

        labelGoodsId.setSize(70, 14); edtGoodsId.setSize(200,20);
        lablePrice.setSize(70, 14); edtPrice.setSize(200,20);
        labelSum.setSize(70, 14);edtSum.setSize(200,20);
        labelStart.setSize(70, 14);edtStart.setSize(200,20);
        labelEnd.setSize(70,14);edtEnd.setSize(200,20);
        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(350, 270);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            salesPro=new BeanSalesPro();
            String goods_id = this.edtGoodsId.getText();
            double price=0;
            if (!"".equals(this.edtPrice.getText())){
                price = Double.parseDouble(this.edtPrice.getText());
            }
            double sum=0;
            if(!"".equals(this.edtSum.getText())){
                sum = Double.parseDouble(this.edtSum.getText());
            }
            long start=0;
            if (!"".equals(this.edtStart.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtStart.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                start = ts;
            }
            long end = 0;
            if (!"".equals(this.edtEnd.getText())){
                Date date = null;
                try {
                    date = simpleDateFormat.parse(this.edtEnd.getText());
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                long ts = date.getTime();
                end = ts;
            }

            salesPro.setGoods_id(goods_id);
            salesPro.setSalespro_price(price);
            salesPro.setSalespro_quantity(sum);
            salesPro.setSalespro_startdate(start);
            salesPro.setSalespro_enddate(end);
            try {
                FreshStoreUtil.salesProManager.addSalesPro(salesPro);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.salesPro=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    public BeanSalesPro getSalesPro(){return salesPro;}
}
