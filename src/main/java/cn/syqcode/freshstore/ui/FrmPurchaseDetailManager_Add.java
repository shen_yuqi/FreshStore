package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.PurchaseDetail;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmPurchaseDetailManager_Add extends JDialog implements ActionListener {
    BeanPurchaseDetail pd = new BeanPurchaseDetail();
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("下单");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelGoodsId = new JLabel("商品编码：");
    private JLabel lableSum = new JLabel("订货数量：");

    private JTextField edtGoodsId = new JTextField(22);
    private JTextField edtSum = new JTextField(22);

    public FrmPurchaseDetailManager_Add(JDialog f, String s, boolean b) {
        super(f, s, b);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelGoodsId);
        workPane.add(edtGoodsId);
        workPane.add(lableSum);
        workPane.add(edtSum);

        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(320, 140);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btnCancel)
            this.setVisible(false);
        else if (e.getSource() == this.btnOk) {
            String goods_id = this.edtGoodsId.getText();
            String sum = this.edtSum.getText();
            if (!"".equals(goods_id)){
                pd.setGoods_id(goods_id);
            }else {
                try {
                    throw new BusinessException("请输入商品编号");
                } catch (BusinessException ex) {
                    ex.printStackTrace();
                }
            }
            try{
                if (!"".equals(sum)){
                    pd.setPurchase_quantity(Double.parseDouble(sum));
                }else{
                    throw new  BusinessException("请输入商品数量");
                }
            }catch (BusinessException e1){
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {
                (new PurchaseDetail()).addPurchaseDetail(pd);
                this.setVisible(false);
            } catch (BaseException e1) {
                pd=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(), "错误", JOptionPane.ERROR_MESSAGE);
            }

        }
    }
    public BeanPurchaseDetail getPurchase() {
        return pd;
    }
}

