package cn.syqcode.freshstore.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BeanSalesPro {
    public static final String[] tableTitles={"促销编号","商品编号","促销价格","促销数量","起始日期","结束日期"};
    private static final long serialVersionUID = 1L;
    private String salespro_id;
    private String goods_id;
    private double salespro_price;
    private double salespro_quantity;
    private long salespro_startdate;
    private long salespro_enddate;

    public String getSalespro_id() {
        return salespro_id;
    }

    public void setSalespro_id(String salespro_id) {
        this.salespro_id = salespro_id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public double getSalespro_price() {
        return salespro_price;
    }

    public void setSalespro_price(double salespro_price) {
        this.salespro_price = salespro_price;
    }

    public double getSalespro_quantity() {
        return salespro_quantity;
    }

    public void setSalespro_quantity(double salespro_quantity) {
        this.salespro_quantity = salespro_quantity;
    }

    public long getSalespro_startdate() {
        return salespro_startdate;
    }

    public void setSalespro_startdate(long salespro_startdate) {
        this.salespro_startdate = salespro_startdate;
    }

    public long getSalespro_enddate() {
        return salespro_enddate;
    }

    public void setSalespro_enddate(long salespro_enddate) {
        this.salespro_enddate = salespro_enddate;
    }

    public String getCell(int col){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(col==0) return salespro_id;
        else if (col==1) return goods_id;
        else if(col==2) return String.valueOf(salespro_price);
        else if(col==3) return String.valueOf(salespro_quantity);
        else if (col==4) {
            Date date = new Date(salespro_startdate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else if (col==5){
            Date date = new Date(salespro_enddate);
            String res = simpleDateFormat.format(date);
            return res;
        }
        else return "";
    }

}
