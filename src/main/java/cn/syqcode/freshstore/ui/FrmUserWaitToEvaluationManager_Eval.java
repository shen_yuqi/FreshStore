package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.*;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmUserWaitToEvaluationManager_Eval extends JDialog implements ActionListener {
    private Evaluation evaluation =null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();

    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelEval = new JLabel("评价：");
    private JLabel lableRenk = new JLabel("星级：");
    private JLabel labelPicture = new JLabel("照片：");

    private JComboBox cmbRenk= new JComboBox(new String[] {"1星", "2星", "3星","4星","5星"});

    private JTextField edtEval = new JTextField(20);
    private JTextField edtPicture = new JTextField(20);

    int no;

    public FrmUserWaitToEvaluationManager_Eval(JDialog f, String s, boolean b,int no) {
        super(f,s,b);
        this.no=no;
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);

        workPane.add(labelEval);
        workPane.add(edtEval);
        workPane.add(labelPicture);
        workPane.add(edtPicture);
        workPane.add(lableRenk);
        workPane.add(cmbRenk);

        this.getContentPane().add(workPane, BorderLayout.CENTER);

        this.setSize(270, 170);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            OrderDetail orderDetail=null;
            try {
                orderDetail=FreshStoreUtil.orderDetailManager.searchOrderDetailByNo(no);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            evaluation=new Evaluation();
            String eval = this.edtEval.getText();
            String picture=this.edtPicture.getText();

            evaluation.setEvaluation_picture(picture);
            evaluation.setEvaluation_content(eval);
            evaluation.setGoods_id(orderDetail.getGoods_id());
            evaluation.setUser_id(BeanUser.currentLoginUser.getUser_id());
            evaluation.setEvaluation_rank(cmbRenk.getSelectedItem().toString());

            try {
                FreshStoreUtil.evaluationManager.addEvaluation(evaluation);
                this.setVisible(false);
            } catch (BaseException e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
