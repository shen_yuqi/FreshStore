package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.Discount;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserDiscountManager_showGoods extends JDialog implements ActionListener {
    String id;
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加至购物车");

    private Object tblTitle[]= Discount.tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable discountTable=new JTable(tablmod);
    List<Discount> discountForms=null;
    private void reloadDiscountTable(String id) throws BaseException {
        discountForms= FreshStoreUtil.discountGoodsManager.loadByDiscountId(id);
        tblData =new Object[discountForms.size()][Discount.tableTitles.length];
        for(int i=0;i<discountForms.size();i++){
            for (int j=0;j<Discount.tableTitles.length;j++){
                tblData[i][j]=discountForms.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.discountTable.validate();
        this.discountTable.repaint();
    }
    public FrmUserDiscountManager_showGoods(FrmUserDiscountManager f, String s, boolean b, String discount_id) throws BaseException{
        super(f,s,b);
        id=discount_id;
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadDiscountTable(discount_id);
        this.getContentPane().add(new JScrollPane(this.discountTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(400, 300);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this.btnAdd){
            int i=this.discountTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择商品","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                FrmUserDiscountManager_showGoods_BuyGoods dlg=new FrmUserDiscountManager_showGoods_BuyGoods(this,"购买商品",true,discountTable.getValueAt(i, 1).toString());
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
