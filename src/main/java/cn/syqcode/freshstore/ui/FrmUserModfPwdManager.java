package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrmUserModfPwdManager extends JDialog implements ActionListener {
    private BeanUser user = null;

    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelPwd1 = new JLabel("新密码:");
    private JLabel labelPwd2 = new JLabel("新密码:");

    private JPasswordField edtPwd1 = new JPasswordField(22);
    private JPasswordField edtPwd2 = new JPasswordField(22);

    public FrmUserModfPwdManager(FrmUserHome f, String s, boolean b) throws BaseException {
        super(f, s, b);

        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelPwd1);
        workPane.add(edtPwd1);
        workPane.add(labelPwd2);
        workPane.add(edtPwd2);

        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(300, 150);
        // 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.btnOk.addActionListener(this);
        this.btnCancel.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btnCancel) {
            this.setVisible(false);
            return;
        } else if (e.getSource() == this.btnOk) {
            try {
                String pwd1 = new String(this.edtPwd1.getPassword());
                String pwd2 = new String(this.edtPwd2.getPassword());
                FreshStoreUtil.userManager.ModfPwd(BeanUser.currentLoginUser.getUser_id(), pwd1, pwd2);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            this.setVisible(false);
        }
    }
}
