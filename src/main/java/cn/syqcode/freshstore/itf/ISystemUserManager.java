package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanSystemUser;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface ISystemUserManager {
    public BeanSystemUser login(String name , String pwd) throws BaseException;

    public List<BeanSystemUser> loadAll() throws BaseException;

    public boolean AddSystemUser(BeanSystemUser beanSystemUser) throws BaseException;

    public boolean ModfSystemUser(BeanSystemUser beanSystemUser) throws BaseException;

    public boolean DelSystemUser(BeanSystemUser beanSystemUser) throws BaseException;
}
