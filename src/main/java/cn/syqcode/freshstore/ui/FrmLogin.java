package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.model.BeanSystemUser;
import cn.syqcode.freshstore.model.BeanUser;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;


public class FrmLogin extends JDialog implements ActionListener{
    BufferedImage img= ImageIO.read(new File("C:\\Users\\King\\IdeaProjects\\FreshStore\\img\\home.jpg"));
    private JPanel toolBar = new JPanel();
    private JPanel toolImg = new JPanel();
    private JLabel labelImg = new JLabel(new ImageIcon(img));
    private JButton btnUserLogin = new JButton("用户登录");
    private JButton btnSystemLogin = new JButton("管理员登录");
    private JButton btnRegister = new JButton("注册");

    public FrmLogin(Frame f, String s, boolean b) throws IOException {
        super(f, s, b);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolImg.add(labelImg);
        this.getContentPane().add(toolImg, BorderLayout.NORTH);
        toolBar.add(this.btnUserLogin);
        toolBar.add(this.btnSystemLogin);
        toolBar.add(this.btnRegister);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);

        this.setSize(370, 300);// 屏幕居中显示
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        btnUserLogin.addActionListener(this);
        btnSystemLogin.addActionListener(this);
        btnRegister.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btnUserLogin) {
            FrmUserLogin dlg=new FrmUserLogin(this,"用户登录",true);
            dlg.setVisible(true);
            if(BeanUser.currentLoginUser!=null){
                this.setVisible(false);
                try {
                    new FrmUserHome();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } else if (e.getSource() == this.btnSystemLogin) {
            FrmSystemUserLogin dlg=new FrmSystemUserLogin(this,"管理员登录",true);
            dlg.setVisible(true);
            if(BeanSystemUser.currentLoginUser!=null){
                this.setVisible(false);
                try {
                    new FrmSystemUserHome();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } else if(e.getSource()==this.btnRegister){
            FrmRegisterManager dlg=new FrmRegisterManager(this,"注册",true);
            dlg.setVisible(true);
        }
    }

}
