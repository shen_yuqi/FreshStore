package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.Discount;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IDiscountGoodsManager {
    public List<Discount> loadAll() throws BaseException;

    public List<Discount> loadByDiscountId(String id) throws BaseException;

    public void delDiscountGoods(int no) throws BaseException;

    public void addDiscountGoods(Discount discount) throws BaseException;

    public int searchNoById(String goods_id,String discount_id) throws BaseException;
}
