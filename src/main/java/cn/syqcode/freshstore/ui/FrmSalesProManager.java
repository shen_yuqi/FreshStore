package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.model.BeanSalesPro;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmSalesProManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加");
    private JButton btnDelete = new JButton("删除");
    private JButton btnModf = new JButton("修改");

    private Object tblTitle[]= BeanSalesPro.tableTitles;
    private Object tblData[][];
    List<BeanSalesPro> salesPros=null;
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable salesProTable=new JTable(tablmod);

    private void reloadDiscountTable() throws BaseException {
        salesPros= FreshStoreUtil.salesProManager.loadAll();
        tblData =new Object[salesPros.size()][BeanSalesPro.tableTitles.length];
        for(int i=0;i<salesPros.size();i++){
            for (int j=0;j<BeanDiscount.tableTitles.length;j++){
                tblData[i][j]=salesPros.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.salesProTable.validate();
        this.salesProTable.repaint();
    }
    public FrmSalesProManager(Frame f, String s, boolean b) throws BaseException {
        super(f,s,b);
        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnDelete);
        toolBar.add(btnModf);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadDiscountTable();
        this.getContentPane().add(new JScrollPane(this.salesProTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.btnModf.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmSalesProManager_Add dlg=new FrmSalesProManager_Add(this,"添加满折信息",true);
            dlg.setVisible(true);
            if(dlg.getSalesPro()!=null){//刷新表格
                try {
                    this.reloadDiscountTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnDelete){
            int i=this.salesProTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择要删除的促销活动","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除该促销活动吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                String id=salesProTable.getValueAt(i,0).toString();
                try {
                    FreshStoreUtil.salesProManager.delSalesPro(String.valueOf(id));
                    this.reloadDiscountTable();
                } catch (BaseException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        else if (e.getSource()==this.btnModf){
            int i=this.salesProTable.getSelectedRow();
            FrmSalesProManager_Modf dlg= null;
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择促销活动","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                dlg = new FrmSalesProManager_Modf(this,"修改满折信息",true,salesProTable.getValueAt(i, 0).toString());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
            if(dlg.getSalesPro()!=null){//刷新表格
                try {
                    this.reloadDiscountTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
