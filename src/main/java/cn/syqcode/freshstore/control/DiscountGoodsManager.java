package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.itf.IDiscountGoodsManager;
import cn.syqcode.freshstore.itf.IDiscountManager;
import cn.syqcode.freshstore.model.BeanDiscount;
import cn.syqcode.freshstore.model.Discount;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DiscountGoodsManager implements IDiscountGoodsManager {
    @Override
    public List<Discount> loadAll() throws BaseException {
        List<Discount> result= new ArrayList<Discount>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM discount where discount_enddate>?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                Discount b=new Discount();
                b.setNo(rs.getInt(1));
                b.setGoods_id(rs.getString(2));
                b.setDiscount_id(rs.getString(3));
                b.setDiscount_startdate(rs.getLong(4));
                b.setDiscount_enddate(rs.getLong(5));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<Discount> loadByDiscountId(String id) throws BaseException {
        List<Discount> result= new ArrayList<Discount>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM discount where discount_enddate>? and discount_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                Discount b=new Discount();
                b.setNo(rs.getInt(1));
                b.setGoods_id(rs.getString(2));
                b.setDiscount_id(rs.getString(3));
                b.setDiscount_startdate(rs.getLong(4));
                b.setDiscount_enddate(rs.getLong(5));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void delDiscountGoods(int no) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update discount set discount_enddate=? where no=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setInt(2,no);
            pst.execute();
            pst.close();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void addDiscountGoods(Discount discount) throws BaseException {
        if ("".equals(discount.getDiscount_id())){
            throw new BusinessException("请输入商品编码。");
        }
        if(FreshStoreUtil.goodsManager.searchGoodsById(discount.getGoods_id())==null){
            throw new BusinessException("该商品不存在。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from discount where goods_id=? and discount_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,discount.getGoods_id());
            pst.setString(2,discount.getDiscount_id());
            java.sql.ResultSet rs=pst.executeQuery();
            if(rs.next()) throw new BusinessException("该商品已经在此满折活动中了。");
            rs.close();
            pst.close();

            sql="insert into discount(goods_id,discount_id,discount_startdate,discount_enddate) values (?,?,?,?)";
            pst=conn.prepareStatement(sql);
            pst.setString(1,discount.getGoods_id());
            pst.setString(2,discount.getDiscount_id());
            pst.setLong(3,discount.getDiscount_startdate());
            pst.setLong(4,discount.getDiscount_enddate());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public int searchNoById(String goods_id,String discount_id) throws BaseException {
        int result;
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select no from discount where goods_id=? and discount_id =?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,goods_id);
            pst.setString(2,discount_id);
            java.sql.ResultSet rs=pst.executeQuery();
            rs.next();
            result=rs.getInt(1);
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }


}
