package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.control.PurchaseDetail;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.model.BeanPurchaseDetail;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FrmPurchaseDetailManager_Modf extends JDialog implements ActionListener {
    private BeanPurchaseDetail pd=new BeanPurchaseDetail();;


    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("确定");
    private JButton btnCancel = new JButton("取消");

    private JLabel labelSum = new JLabel("采购数量：");
    private JLabel lableStatus = new JLabel("    状态: ");

    private JTextField edtSum = new JTextField(8);
    private JComboBox cmbStatus= new JComboBox(new String[] {"下单", "在途", "入库"});

    public FrmPurchaseDetailManager_Modf(JDialog f, String s, boolean b, String id) throws DbException {
        super(f, s, b);
        pd.setPurchase_id(id);
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from purchase_detail where purchase_id = '"+id+"'";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            edtSum.setText(rs.getString(4));
            if("下单".equals(rs.getString(5))){
                cmbStatus.setSelectedIndex(0);
            }else if ("在途".equals(rs.getString(5))){
                cmbStatus.setSelectedIndex(1);
            }else if ("入库".equals(rs.getString(5))){
                cmbStatus.setSelectedIndex(2);
            }

            toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
            toolBar.add(this.btnOk);
            toolBar.add(btnCancel);
            this.getContentPane().add(toolBar, BorderLayout.SOUTH);
            workPane.add(labelSum);
            workPane.add(edtSum);
            workPane.add(lableStatus);
            workPane.add(cmbStatus);

            this.getContentPane().add(workPane, BorderLayout.CENTER);
            this.setSize(100, 150);
            // 屏幕居中显示
            double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
            double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
            this.setLocation((int) (width - this.getWidth()) / 2,
                    (int) (height - this.getHeight()) / 2);

            this.validate();
            this.btnOk.addActionListener(this);
            this.btnCancel.addActionListener(this);
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnCancel) {
            this.setVisible(false);
            return;
        }
        else if(e.getSource()==this.btnOk){
            String sum = this.edtSum.getText();
            String statu = this.cmbStatus.getSelectedItem().toString();

            String goods_id= null;
            try {
                goods_id = FreshStoreUtil.purchaseDetail.searchGoodsIdById(pd.getPurchase_id());
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            pd.setGoods_id(goods_id);
            pd.setPurchase_quantity(Double.parseDouble(sum));
            pd.setPurchase_status(statu);

            try {
                (new PurchaseDetail()).modfPurchaseDetail(pd);
                this.setVisible(false);
            } catch (BaseException e1) {
                this.pd=null;
                JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
            }
        }

    }
    public BeanPurchaseDetail getPurchase() {
        return pd;
    }
}
