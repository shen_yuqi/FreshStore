package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanOrder;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IUserOrderManager {
    public List<BeanOrder> loadAll(String user_id) throws BaseException;

    public List<BeanOrder> searchByStatus(String user_id,String status) throws BaseException;

    public BeanOrder searchByStatus2(String user_id, String status) throws BaseException;

    public void ModfOrderStatus(String ord_id,String statu) throws BaseException;

    public void ModfOrder(BeanOrder beanOrder) throws BaseException;

    public boolean addGoods(String goods_id,String user_id,double buysum) throws BaseException;

    public void addOrder(String user_id) throws BaseException;

}
