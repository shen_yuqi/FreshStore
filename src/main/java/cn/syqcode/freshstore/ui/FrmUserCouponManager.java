package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.control.CouponManager;
import cn.syqcode.freshstore.model.BeanAddr;
import cn.syqcode.freshstore.model.BeanCoupon;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmUserCouponManager extends JDialog implements ActionListener {
    private Object tblTitle[]= BeanCoupon.tableTitles;
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable couponTable=new JTable(tablmod);
    List<BeanCoupon> coupons=null;
    private void reloadCouponTable() throws BaseException {
        coupons=(new CouponManager()).loadAllGroupId();
        tblData =new Object[coupons.size()][BeanCoupon.tableTitles.length];
        for(int i=0;i<coupons.size();i++){
            for (int j=0;j<BeanCoupon.tableTitles.length;j++){
                tblData[i][j]=coupons.get(i).getCell(j);
            }
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.couponTable.validate();
        this.couponTable.repaint();
    }

    public FrmUserCouponManager(Frame f, String s, boolean b) throws BaseException {
        this.reloadCouponTable();
        this.getContentPane().add(new JScrollPane(this.couponTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
