package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.util.BaseException;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class FrmSystemUserHome extends JFrame implements ActionListener{
    private static final long serialVersionUID = 1L;
    private JMenuBar menubar=new JMenuBar();
    private JMenu menu_user = new JMenu("用户管理");
    private JMenu menu_goods=new JMenu("商品管理");
    private JMenu menu_order=new JMenu("订单管理");
    private JMenu menu_discount=new JMenu("优惠管理");
    private JMenu menu_purchase = new JMenu("采购管理");

    private JMenuItem menuItem_user = new JMenuItem("用户管理");
    private JMenuItem menuItem_consume = new JMenuItem("消费情况");

    private JMenuItem menuItem_type = new JMenuItem("类别管理");
    private JMenuItem menuItem_goods = new JMenuItem("商品管理");
    private JMenuItem menuItem_evaluation = new JMenuItem("评价管理");
    private JMenuItem menuItem_menu = new JMenuItem("菜谱管理");

    private JMenuItem menuItem_order = new JMenuItem("订单管理");

    private JMenuItem menuItem_coupon = new JMenuItem("优惠券");
    private JMenuItem menuItem_discount = new JMenuItem("满折优惠");
    private JMenuItem menuItem_salespro = new JMenuItem("促销活动");

    private JMenuItem menuItem_purchase = new JMenuItem("采购管理");

    private JPanel statusBar = new JPanel();

    public FrmSystemUserHome() throws IOException {
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
        this.setTitle("生鲜超网管理员系统");
        //菜单
        this.menu_user.add(this.menuItem_user); this.menuItem_user.addActionListener(this);
        this.menu_user.add(this.menuItem_consume); this.menuItem_consume.addActionListener(this);

        this.menu_order.add(this.menuItem_order);this.menuItem_order.addActionListener(this);

        this.menu_discount.add(this.menuItem_coupon);this.menuItem_coupon.addActionListener(this);
        this.menu_discount.add(this.menuItem_discount);this.menuItem_discount.addActionListener(this);
        this.menu_discount.add(this.menuItem_salespro);this.menuItem_salespro.addActionListener(this);

        this.menu_purchase.add(this.menuItem_purchase);this.menuItem_purchase.addActionListener(this);
        
        this.menu_goods.add(this.menuItem_type); this.menuItem_type.addActionListener(this);
        this.menu_goods.add(this.menuItem_goods); this.menuItem_goods.addActionListener(this);
        this.menu_goods.add(this.menuItem_evaluation); this.menuItem_evaluation.addActionListener(this);
        this.menu_goods.add(this.menuItem_menu); this.menuItem_menu.addActionListener(this);

        menubar.add(menu_user);
        menubar.add(menu_goods);
        menubar.add(menu_order);
        menubar.add(menu_discount);
        menubar.add(menu_purchase);
        this.setJMenuBar(menubar);

        //状态栏
        statusBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel label=new JLabel("您好!");//修改成   您好！+登陆用户名
        statusBar.add(label);
        this.getContentPane().add(statusBar,BorderLayout.SOUTH);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.menuItem_user){
            FrmUserManager dlg=new FrmUserManager(this,"用户管理",true);
            dlg.setVisible(true);
        }
        else if(e.getSource()==this.menuItem_consume){
            FrmConsumeManager dlg= null;
            try {
                dlg = new FrmConsumeManager(this,"消费情况",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if(e.getSource()==this.menuItem_type){
            FrmTypeManager dlg= null;
            try {
                dlg = new FrmTypeManager(this,"商品类别管理",true);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if(e.getSource()==this.menuItem_goods){
            FrmGoodsManager dlg= null;
            try {
                dlg = new FrmGoodsManager(this,"商品管理",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if (e.getSource()==menuItem_evaluation){
            FrmEvaluationManager dlg = null;
            try {
                dlg = new FrmEvaluationManager(this,"评价管理",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }
        else if(e.getSource()==this.menuItem_menu){
            FrmMenuManager dlg = null;
            try {
                dlg = new FrmMenuManager(this,"菜谱管理",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);

        }
        else if (e.getSource()==this.menuItem_order){
            try {
                FrmOrderManager dlg= new FrmOrderManager(this,"订单管理",true);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.menuItem_coupon){
            try {
                FrmCouponManager dlg= new FrmCouponManager(this,"优惠券管理",true);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.menuItem_discount){
            try {
                FrmDiscountManager dlg= new FrmDiscountManager(this,"满折管理",true);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.menuItem_salespro){
            try {
                FrmSalesProManager dlg= new FrmSalesProManager(this,"满折管理",true);
                dlg.setVisible(true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==this.menuItem_purchase){
            FrmPurchaseDetailManager dlg = null;
            try {
                dlg = new FrmPurchaseDetailManager(this,"采购管理",true);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
            dlg.setVisible(true);
        }

    }
}
