package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmTypeManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnAdd = new JButton("添加生鲜类别");
    private JButton btnModf = new JButton("修改生鲜类别");
    private JButton btnDelete = new JButton("删除生鲜类别");

    private Object tblTitle[]={"类别编号","类别名称","类别描述"};
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable typeTable=new JTable(tablmod);
    List<BeanFreshType> types=null;
    private void reloadTypeTable() throws BaseException {
        try{
            types = (new FreshTypeManager()).loadAll();
            tblData =new Object[types.size()][3];
            for(int i=0;i<types.size();i++){
                tblData[i][0]=types.get(i).getType_id();
                tblData[i][1]=types.get(i).getType_name();
                tblData[i][2]=types.get(i).getType_desc();
            }
            tablmod.setDataVector(tblData,tblTitle);
            this.typeTable.validate();
            this.typeTable.repaint();
        }catch (BaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public FrmTypeManager(Frame f, String s, boolean b) throws BaseException {
        super(f, s, b);

        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnAdd);
        toolBar.add(btnModf);
        toolBar.add(btnDelete);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadTypeTable();
        this.getContentPane().add(new JScrollPane(this.typeTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(400, 300);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnAdd.addActionListener(this);
        this.btnModf.addActionListener(this);
        this.btnDelete.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.btnAdd){
            FrmTypeManager_AddType dlg=new FrmTypeManager_AddType(this,"添加生鲜类别",true);
            dlg.setVisible(true);
            if(dlg.getFreshType()!=null){//刷新表格
                try {
                    this.reloadTypeTable();
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else if(e.getSource()==this.btnModf){
            int i=this.typeTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择生鲜类别","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            List<BeanFreshType> users= null;
            try {
                users = (new FreshTypeManager()).loadAll();
                String id=users.get(i).getType_id();
                FrmTypeManager_ModfType dlg=new FrmTypeManager_ModfType(this,"修改生鲜类别",true,id);
                dlg.setVisible(true);
                if(dlg.getFreshType()!=null){//刷新表格
                    try {
                        this.reloadTypeTable();
                    } catch (BaseException ex) {
                        ex.printStackTrace();
                    }
                }
            } catch (BaseException ex) {
                ex.printStackTrace();
            }

        }
        else if(e.getSource()==this.btnDelete){
            int i=this.typeTable.getSelectedRow();
            if(i<0) {
                JOptionPane.showMessageDialog(null,  "请选择生鲜类别","提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(JOptionPane.showConfirmDialog(this,"确定删除该生鲜类别吗？","确认",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                List<BeanFreshType> types= null;
                try {
                    types = (new FreshTypeManager()).loadAll();
                    String id=types.get(i).getType_id();
                    try {
                        (new FreshTypeManager()).delFreshType(id);
                        this.reloadTypeTable();
                    } catch (BaseException e1) {
                        JOptionPane.showMessageDialog(null, e1.getMessage(),"错误",JOptionPane.ERROR_MESSAGE);
                    }
                } catch (BaseException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
