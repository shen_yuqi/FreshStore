package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.control.FreshTypeManager;
import cn.syqcode.freshstore.control.GoodsManager;
import cn.syqcode.freshstore.model.BeanFreshType;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.util.BaseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class FrmGoodsManager_Search extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JButton btnSales = new JButton("销量降序");
    private JButton btnQuantity = new JButton("库存升序");
    private JButton btnSearchByID = new JButton("查询");
    private JButton btnSearchByType = new JButton("查询");

    private JTextField edtSearchByID = new JTextField(10);
    private JComboBox cmbType= new JComboBox();

    private Object tblTitle[]={"商品编号","商品名称","商品类别","商品单价","会员价","规格","描述","总销量","库存量"};
    private Object tblData[][];
    DefaultTableModel tablmod=new DefaultTableModel();
    private JTable goodsTable=new JTable(tablmod);
    List<BeanGoods> goods=null;

    private void reloadGoodsTable() throws BaseException {
        goods=(new GoodsManager()).loadAll();
        tblData =new Object[goods.size()][9];
        for(int i=0;i<goods.size();i++){
            tblData[i][0]=goods.get(i).getGoods_id();
            tblData[i][1]=goods.get(i).getGoods_name();
            tblData[i][2]=(new FreshTypeManager()).searchTypeById(goods.get(i).getType_id());
            tblData[i][3]=goods.get(i).getGoods_price();
            tblData[i][4]=goods.get(i).getGoods_vipprice();
            tblData[i][5]=goods.get(i).getGoods_specs();
            tblData[i][6]=goods.get(i).getGoods_desc();
            tblData[i][7]=(new GoodsManager()).countSales(goods.get(i).getGoods_id());
            tblData[i][8]=goods.get(i).getGoods_quantity();
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.goodsTable.validate();
        this.goodsTable.repaint();
    }

    private void reloadGoodsTableBySales() throws BaseException{
        goods=(new GoodsManager()).loadBySales();
        tblData =new Object[goods.size()][9];
        for(int i=0;i<goods.size();i++){
            tblData[i][0]=goods.get(i).getGoods_id();
            tblData[i][1]=goods.get(i).getGoods_name();
            tblData[i][2]=(new FreshTypeManager()).searchTypeById(goods.get(i).getType_id());
            tblData[i][3]=goods.get(i).getGoods_price();
            tblData[i][4]=goods.get(i).getGoods_vipprice();
            tblData[i][5]=goods.get(i).getGoods_specs();
            tblData[i][6]=goods.get(i).getGoods_desc();
            tblData[i][7]=(new GoodsManager()).countSales(goods.get(i).getGoods_id());
            tblData[i][8]=goods.get(i).getGoods_quantity();
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.goodsTable.validate();
        this.goodsTable.repaint();
    }

    private void reloadGoodsTableByQuantity() throws BaseException{
        goods=(new GoodsManager()).loadByQuantity();
        tblData =new Object[goods.size()][9];
        for(int i=0;i<goods.size();i++){
            tblData[i][0]=goods.get(i).getGoods_id();
            tblData[i][1]=goods.get(i).getGoods_name();
            tblData[i][2]=(new FreshTypeManager()).searchTypeById(goods.get(i).getType_id());
            tblData[i][3]=goods.get(i).getGoods_price();
            tblData[i][4]=goods.get(i).getGoods_vipprice();
            tblData[i][5]=goods.get(i).getGoods_specs();
            tblData[i][6]=goods.get(i).getGoods_desc();
            tblData[i][7]=(new GoodsManager()).countSales(goods.get(i).getGoods_id());
            tblData[i][8]=goods.get(i).getGoods_quantity();
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.goodsTable.validate();
        this.goodsTable.repaint();
    }

    private void reloadGoodsTableById(String id) throws BaseException{
        try{
            BeanGoods goods=(new GoodsManager()).searchGoodsById(id);
            if (goods==null){
                throw new BaseException("经查询，没有此商品。");
            }
            tblData =new Object[1][9];
            for(int i=0;i<1;i++){
                tblData[i][0]=goods.getGoods_id();
                tblData[i][1]=goods.getGoods_name();
                tblData[i][2]=(new FreshTypeManager()).searchTypeById(goods.getType_id());
                tblData[i][3]=goods.getGoods_price();
                tblData[i][4]=goods.getGoods_vipprice();
                tblData[i][5]=goods.getGoods_specs();
                tblData[i][6]=goods.getGoods_desc();
                tblData[i][7]=(new GoodsManager()).countSales(goods.getGoods_id());
                tblData[i][8]=goods.getGoods_quantity();
            }
            tablmod.setDataVector(tblData,tblTitle);
            this.goodsTable.validate();
            this.goodsTable.repaint();
        }catch (BaseException e1){
            JOptionPane.showMessageDialog(null,  e1.getMessage(),"提示",JOptionPane.ERROR_MESSAGE);
            return;
        }

    }

    private void reloadGoodsTableByType(String type_name) throws BaseException{
        goods=(new GoodsManager()).loadByType(type_name);
        tblData =new Object[goods.size()][9];
        for(int i=0;i<goods.size();i++){
            tblData[i][0]=goods.get(i).getGoods_id();
            tblData[i][1]=goods.get(i).getGoods_name();
            tblData[i][2]=(new FreshTypeManager()).searchTypeById(goods.get(i).getType_id());
            tblData[i][3]=goods.get(i).getGoods_price();
            tblData[i][4]=goods.get(i).getGoods_vipprice();
            tblData[i][5]=goods.get(i).getGoods_specs();
            tblData[i][6]=goods.get(i).getGoods_desc();
            tblData[i][7]=(new GoodsManager()).countSales(goods.get(i).getGoods_id());
            tblData[i][8]=goods.get(i).getGoods_quantity();
        }
        tablmod.setDataVector(tblData,tblTitle);
        this.goodsTable.validate();
        this.goodsTable.repaint();
    }

    public FrmGoodsManager_Search(FrmGoodsManager f, String s, boolean b) throws BaseException {
        super(f, s, b);

        //获取类别名称
        List<BeanFreshType> types=null;
        try {
            types = (new FreshTypeManager()).loadAll();
            String[] strTypes=new String[types.size()+1];
            strTypes[0]="";
            for(int i=0;i<types.size();i++) {
                strTypes[i+1]=types.get(i).getType_name();
            }
            cmbType=new JComboBox(strTypes);
        } catch (BaseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        toolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        toolBar.add(btnSales);
        toolBar.add(btnQuantity);
        toolBar.add(edtSearchByID);
        toolBar.add(btnSearchByID);
        toolBar.add(cmbType);
        toolBar.add(btnSearchByType);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        //提取现有数据
        this.reloadGoodsTable();
        this.getContentPane().add(new JScrollPane(this.goodsTable), BorderLayout.CENTER);

        // 屏幕居中显示
        this.setSize(800, 600);
        double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        this.setLocation((int) (width - this.getWidth()) / 2,
                (int) (height - this.getHeight()) / 2);

        this.validate();

        this.btnSales.addActionListener(this);
        this.btnQuantity.addActionListener(this);
        this.btnSearchByID.addActionListener(this);
        this.btnSearchByType.addActionListener(this);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==btnSales){
            try {
                this.reloadGoodsTableBySales();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==btnQuantity){
            try {
                this.reloadGoodsTableByQuantity();
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==btnSearchByID){
            String id=this.edtSearchByID.getText();
            try {
                this.reloadGoodsTableById(id);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
        else if (e.getSource()==btnSearchByType){
            String name=this.cmbType.getSelectedItem().toString();
            try {
                this.reloadGoodsTableByType(name);
            } catch (BaseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
