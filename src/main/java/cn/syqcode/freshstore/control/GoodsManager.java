package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IGoodsManager;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GoodsManager implements IGoodsManager {
    @Override
    public List<BeanGoods> loadAll() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void addGoods(BeanGoods beanGoods) throws BaseException {
        if ("".equals(beanGoods.getGoods_name())){
            throw new BusinessException("请输入商品名称");
        }
        if (beanGoods.getType_id()==null){
            throw new BusinessException("请选择商品类型");
        }
        if(beanGoods.getGoods_price()<=0){
            throw new BusinessException("请输入商品单价");
        }
        if(beanGoods.getGoods_vipprice()<=0){
            throw new BusinessException("请输入商品会员价");
        }
        if ("".equals(beanGoods.getGoods_specs())){
            throw new BusinessException("请输入商品的规格");
        }
        if ("".equals(beanGoods.getGoods_desc())){
            throw new BusinessException("请输入商品的描述");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into tbl_goods values (?,?,?,?,?,?,?,?,0)";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            UUID uuid = UUID.randomUUID();
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,beanGoods.getType_id());
            pst.setString(3,beanGoods.getGoods_name());
            pst.setDouble(4,beanGoods.getGoods_price());
            pst.setDouble(5,beanGoods.getGoods_vipprice());
            pst.setDouble(6,beanGoods.getGoods_quantity());
            pst.setString(7,beanGoods.getGoods_specs());
            pst.setString(8,beanGoods.getGoods_desc());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    //框架里判断
    @Override
    public void ModfGoods(BeanGoods beanGoods) throws BaseException {
        if ("".equals(beanGoods.getGoods_name())){
            throw new BusinessException("请输入商品名称");
        }
        if (beanGoods.getType_id()==null){
            throw new BusinessException("请选择商品类型");
        }
        if(beanGoods.getGoods_price()<=0){
            throw new BusinessException("请输入商品单价");
        }
        if(beanGoods.getGoods_vipprice()<=0){
            throw new BusinessException("请输入商品会员价");
        }
        if ("".equals(beanGoods.getGoods_specs())){
            throw new BusinessException("请输入商品的规格");
        }
        if ("".equals(beanGoods.getGoods_desc())){
            throw new BusinessException("请输入商品的描述");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_goods set goods_price=?,goods_vipprice=?,goods_quantity=?,goods_specs=?,goods_desc=?,goods_name=?,type_id=? where goods_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setDouble(1,beanGoods.getGoods_price());
            pst.setDouble(2,beanGoods.getGoods_vipprice());
            pst.setDouble(3,beanGoods.getGoods_quantity());
            pst.setString(4,beanGoods.getGoods_specs());
            pst.setString(5,beanGoods.getGoods_desc());
            pst.setString(6,beanGoods.getGoods_name());
            pst.setString(7,beanGoods.getType_id());
            pst.setString(8,beanGoods.getGoods_id());

            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void delGoods(String goods_id) throws BaseException {
        BeanGoods beanGoods = (new GoodsManager()).searchGoodsById(goods_id);
        if (beanGoods.getGoods_quantity()>0){
            throw new BusinessException("该商品还有库存，无法删除。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_goods set deleteflag=? where goods_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,beanGoods.getGoods_id());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }

    }

    @Override
    public BeanGoods searchGoodsById(String goods_id) throws BaseException {
        BeanGoods result = null;
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_goods where goods_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,goods_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if(rs.next()) {
                result=new BeanGoods();
                result.setGoods_id(rs.getString(1));
                result.setType_id(rs.getString(2));
                result.setGoods_name(rs.getString(3));
                result.setGoods_price(rs.getDouble(4));
                result.setGoods_vipprice(rs.getDouble(5));
                result.setGoods_quantity(rs.getDouble(6));
                result.setGoods_specs(rs.getString(7));
                result.setGoods_desc(rs.getString(8));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }


    @Override
    public double countSales(String goods_id) throws BaseException {
        double result=0;
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT sum(a.orddetail_quantity)\n" +
                    "FROM orderdetail a,tbl_order b\n" +
                    "WHERE a.ord_id=b.ord_id AND a.goods_id = ? AND b.ord_status NOT LIKE '退货' AND b.ord_status NOT LIKE '未下单'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,goods_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if(rs.next()) {
                result = rs.getDouble(1);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> loadBySales() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT c.*\n" +
                    "FROM orderdetail a,tbl_order b,tbl_goods c\n" +
                    "WHERE a.ord_id=b.ord_id AND b.ord_status NOT LIKE '退货' AND c.goods_id=a.goods_id AND c.deleteflag=0\n" +
                    "GROUP BY a.goods_id\n" +
                    "ORDER BY sum(a.orddetail_quantity) DESC";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> loadByQuantity() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0 order by goods_quantity ASC";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> loadByQuantityDesc() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0 order by goods_quantity DESC";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }


    @Override
    public List<BeanGoods> loadByType(String type_name) throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        String id = (new FreshTypeManager()).searchTypeIdByName(type_name);
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0 and type_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> searchGoodsByName(String name) throws BaseException {
        List<BeanGoods> result = new ArrayList<>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_goods where goods_name like '%"+name+"%'";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while (rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public List<BeanGoods> loadAllVIPGoods() throws BaseException {
        List<BeanGoods> result= new ArrayList<BeanGoods>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_goods where deleteflag=0 and goods_price>goods_vipprice";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanGoods b=new BeanGoods();
                b.setGoods_id(rs.getString(1));
                b.setType_id(rs.getString(2));
                b.setGoods_name(rs.getString(3));
                b.setGoods_price(rs.getDouble(4));
                b.setGoods_vipprice(rs.getDouble(5));
                b.setGoods_quantity(rs.getDouble(6));
                b.setGoods_specs(rs.getString(7));
                b.setGoods_desc(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }


}
