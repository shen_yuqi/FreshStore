package cn.syqcode.freshstore.control;

import cn.syqcode.freshstore.itf.IUserAddrManager;
import cn.syqcode.freshstore.model.BeanAddr;
import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.util.BaseException;
import cn.syqcode.freshstore.util.BusinessException;
import cn.syqcode.freshstore.util.DBUtil;
import cn.syqcode.freshstore.util.DbException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserAddrManager implements IUserAddrManager {
    @Override
    public List<BeanAddr> loadAll() throws BaseException {
        List<BeanAddr> result= new ArrayList<BeanAddr>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_Addr where deleteflag=0";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanAddr b=new BeanAddr();
                b.setAddr_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setAddr_province(rs.getString(3));
                b.setAddr_city(rs.getString(4));
                b.setAddr_area(rs.getString(5));
                b.setAddr_addr(rs.getString(6));
                b.setAddr_people(rs.getString(7));
                b.setAddr_phone(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }

    @Override
    public void ModfAddr(BeanAddr beanAddr) throws BaseException {
        if ("".equals(beanAddr.getAddr_province())){
            throw new BusinessException("请输入省份。");
        }
        if ("".equals(beanAddr.getAddr_city())){
            throw new BusinessException("请输入市。");
        }
        if ("".equals(beanAddr.getAddr_area())){
            throw new BusinessException("请输入区。");
        }
        if ("".equals(beanAddr.getAddr_addr())){
            throw new BusinessException("请输入地址。");
        }
        if ("".equals(beanAddr.getAddr_people())){
            throw new BusinessException("请输入收货人姓名。");
        }
        if ("".equals(beanAddr.getAddr_phone())||!Validator.isMobile(beanAddr.getAddr_phone())){
            throw new BusinessException("请输入正确的收货人电话。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="update tbl_addr set addr_province=?,addr_city=?,addr_area=?,addr_addr=?,addr_people=?,addr_phone=? where addr_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,beanAddr.getAddr_province());
            pst.setString(2,beanAddr.getAddr_city());
            pst.setString(3,beanAddr.getAddr_area());
            pst.setString(4,beanAddr.getAddr_addr());
            pst.setString(5,beanAddr.getAddr_people());
            pst.setString(6,beanAddr.getAddr_phone());
            pst.setString(7,beanAddr.getAddr_id());
          //  System.out.println(beanAddr.getAddr_people()+"  "+beanAddr.getAddr_id());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void addAddr(BeanAddr beanAddr) throws BaseException {
        if ("".equals(beanAddr.getAddr_province())){
            throw new BusinessException("请输入省份。");
        }
        if ("".equals(beanAddr.getAddr_city())){
            throw new BusinessException("请输入市。");
        }
        if ("".equals(beanAddr.getAddr_area())){
            throw new BusinessException("请输入区。");
        }
        if ("".equals(beanAddr.getAddr_addr())){
            throw new BusinessException("请输入地址。");
        }
        if ("".equals(beanAddr.getAddr_people())){
            throw new BusinessException("请输入收货人姓名。");
        }
        if ("".equals(beanAddr.getAddr_phone())||!Validator.isMobile(beanAddr.getAddr_phone())){
            throw new BusinessException("请输入正确的收货人电话。");
        }
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="insert into tbl_addr values (?,?,?,?,?,?,?,?,0)";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            String id = UUID.randomUUID().toString();
            pst.setString(1,id);
            pst.setString(2,beanAddr.getUser_id());
            pst.setString(3,beanAddr.getAddr_province());
            pst.setString(4,beanAddr.getAddr_city());
            pst.setString(5,beanAddr.getAddr_area());
            pst.setString(6,beanAddr.getAddr_addr());
            pst.setString(7,beanAddr.getAddr_people());
            pst.setString(8,beanAddr.getAddr_phone());
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public void delAddr(String addr_id) throws BaseException {
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="select * from tbl_order where addr_id=? and (ord_status='下单' or ord_status='配送')";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,addr_id);
            java.sql.ResultSet rs=pst.executeQuery();
            if (rs.next()){
                throw new BusinessException("该地址还有未完成的订单，无法删除");
            }
            sql="update tbl_addr set deleteflag=? where addr_id=?";
            pst=conn.prepareStatement(sql);
            pst.setLong(1,System.currentTimeMillis());
            pst.setString(2,addr_id);
            pst.execute();
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    }

    @Override
    public BeanAddr searchById(String addr_id) throws BaseException {
        BeanAddr b= new BeanAddr();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_Addr where deleteflag=0 and addr_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,addr_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                b.setAddr_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setAddr_province(rs.getString(3));
                b.setAddr_city(rs.getString(4));
                b.setAddr_area(rs.getString(5));
                b.setAddr_addr(rs.getString(6));
                b.setAddr_people(rs.getString(7));
                b.setAddr_phone(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return b;
    }

    @Override
    public List<BeanAddr> loadByUserID(String user_id) throws BaseException {
        List<BeanAddr> result= new ArrayList<BeanAddr>();
        Connection conn=null;
        try {
            conn= DBUtil.getConnection();
            String sql="SELECT * FROM tbl_Addr where deleteflag=0 and user_id=?";
            java.sql.PreparedStatement pst=conn.prepareStatement(sql);
            pst.setString(1,user_id);
            java.sql.ResultSet rs=pst.executeQuery();
            while(rs.next()) {
                BeanAddr b=new BeanAddr();
                b.setAddr_id(rs.getString(1));
                b.setUser_id(rs.getString(2));
                b.setAddr_province(rs.getString(3));
                b.setAddr_city(rs.getString(4));
                b.setAddr_area(rs.getString(5));
                b.setAddr_addr(rs.getString(6));
                b.setAddr_people(rs.getString(7));
                b.setAddr_phone(rs.getString(8));
                b.setDeleteflag(rs.getLong(9));
                result.add(b);
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DbException(e);
        }
        finally{
            if(conn!=null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return result;
    }
}
