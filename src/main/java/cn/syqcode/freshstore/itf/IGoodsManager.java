package cn.syqcode.freshstore.itf;

import cn.syqcode.freshstore.model.BeanGoods;
import cn.syqcode.freshstore.util.BaseException;

import java.util.List;

public interface IGoodsManager {
    public List<BeanGoods> loadAll() throws BaseException;

    public void addGoods(BeanGoods beanGoods) throws BaseException;

    public void ModfGoods(BeanGoods beanGoods) throws BaseException;

    public void delGoods(String goods_id) throws BaseException;

    public BeanGoods searchGoodsById(String goods_id) throws BaseException;

    public double countSales(String goods_id) throws BaseException;

    public List<BeanGoods> loadBySales() throws BaseException;

    public List<BeanGoods> loadByQuantity() throws BaseException;

    public List<BeanGoods> loadByQuantityDesc() throws BaseException;

    public List<BeanGoods> loadByType(String type_name) throws BaseException;

    public List<BeanGoods> searchGoodsByName(String name) throws BaseException;

    public List<BeanGoods> loadAllVIPGoods() throws BaseException;
}
