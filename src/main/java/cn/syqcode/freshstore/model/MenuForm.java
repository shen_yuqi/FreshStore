package cn.syqcode.freshstore.model;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.util.BaseException;

import java.io.Serializable;

public class MenuForm {
    public static final String[] tableTitles={"序号","商品编号","商品名称","描述"};
    private static final long serialVersionUID = 1L;
    private int no;
    private String goods_id;
    private String menu_id;
    private String remarks;
    private long deleteflag;

    public void setNo(int no) {
        this.no = no;
    }

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    public int getNo() {
        return no;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCell(int col) throws BaseException {
        if(col==0) return String.valueOf(no);
        else if(col==1) return goods_id;
        else if (col==2) return FreshStoreUtil.goodsManager.searchGoodsById(goods_id).getGoods_name();
        else if (col==3) return getRemarks();
        else return "";
    }
}
