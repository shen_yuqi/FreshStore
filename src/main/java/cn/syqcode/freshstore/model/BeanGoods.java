package cn.syqcode.freshstore.model;

import java.io.Serializable;

public class BeanGoods {
    private static final long serialVersionUID = 1L;
    private String goods_id;
    private String type_id;
    private String salespro_id;
    private String goods_name;
    private double goods_price;
    private double goods_quantity;
    private double goods_vipprice;
    private String goods_specs;
    private String goods_desc;
    private long deleteflag;

    public long getDeleteflag() {
        return deleteflag;
    }

    public void setDeleteflag(long deleteflag) {
        this.deleteflag = deleteflag;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getSalespro_id() {
        return salespro_id;
    }

    public void setSalespro_id(String salespro_id) {
        this.salespro_id = salespro_id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public double getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(double goods_price) {
        this.goods_price = goods_price;
    }

    public double getGoods_quantity() {
        return goods_quantity;
    }

    public void setGoods_quantity(double goods_quantity) {
        this.goods_quantity = goods_quantity;
    }

    public double getGoods_vipprice() {
        return goods_vipprice;
    }

    public void setGoods_vipprice(double goods_vipprice) {
        this.goods_vipprice = goods_vipprice;
    }

    public String getGoods_specs() {
        return goods_specs;
    }

    public void setGoods_specs(String goods_specs) {
        this.goods_specs = goods_specs;
    }

    public String getGoods_desc() {
        return goods_desc;
    }

    public void setGoods_desc(String goods_desc) {
        this.goods_desc = goods_desc;
    }
}
