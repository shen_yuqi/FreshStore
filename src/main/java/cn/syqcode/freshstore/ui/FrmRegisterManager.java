package cn.syqcode.freshstore.ui;

import cn.syqcode.freshstore.FreshStoreUtil;
import cn.syqcode.freshstore.model.BeanUser;
import cn.syqcode.freshstore.util.BaseException;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrmRegisterManager extends JDialog implements ActionListener {
    private JPanel toolBar = new JPanel();
    private JPanel workPane = new JPanel();
    private JButton btnOk = new JButton("注册");
    private JButton btnCancel = new JButton("取消");
    private JRadioButton man = new JRadioButton("男");
    private JRadioButton women = new JRadioButton("女");
    private JRadioButton isVip = new JRadioButton("是");
    private JRadioButton noVip = new JRadioButton("否");

    private JLabel labelUser = new JLabel("用户：   ");
    private JLabel lableSex = new JLabel("性别:   ");
    private JLabel labelPwd = new JLabel("密码：   ");
    private JLabel labelPwd2 = new JLabel("密码：   ");
    private JLabel labelPhone = new JLabel("联系方式:");
    private JLabel labelEmail = new JLabel("电子邮箱:");
    private JLabel labelCity = new JLabel("所在城市:");
    private JLabel labelVip = new JLabel("是否加入会员:");

    private JTextField edtUserId = new JTextField(22);
    //private JTextField edtSex = new JTextField(20);
    private JTextField edtPhone = new JTextField(22);
    private JTextField edtEmail = new JTextField(22);
    private JTextField edtCity = new JTextField(22);
    //private JTextField edtVip = new JTextField(20);
    private JPasswordField edtPwd = new JPasswordField(22);
    private JPasswordField edtPwd2 = new JPasswordField(22);
    public FrmRegisterManager(Dialog f, String s, boolean b) {
        super(f, s, b);
        toolBar.setLayout(new FlowLayout(FlowLayout.CENTER));
        toolBar.add(this.btnOk);
        toolBar.add(btnCancel);
        this.getContentPane().add(toolBar, BorderLayout.SOUTH);
        workPane.add(labelUser);
        workPane.add(edtUserId);
        workPane.add(labelPwd);
        workPane.add(edtPwd);
        workPane.add(labelPwd2);
        workPane.add(edtPwd2);
        workPane.add(labelPhone);
        workPane.add(edtPhone);
        workPane.add(labelEmail);
        workPane.add(edtEmail);
        workPane.add(labelCity);
        workPane.add(edtCity);
        workPane.add(lableSex);
        ButtonGroup group1 = new ButtonGroup();
        group1.add(man);
        group1.add(women);
        workPane.add(man);
        workPane.add(women);
        workPane.add(labelVip);
        ButtonGroup group2 = new ButtonGroup();
        group2.add(isVip);
        group2.add(noVip);
        workPane.add(isVip);
        workPane.add(noVip);
        this.getContentPane().add(workPane, BorderLayout.CENTER);
        this.setSize(320, 280);
        this.btnCancel.addActionListener(this);
        this.btnOk.addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btnCancel)
            this.setVisible(false);
        else if (e.getSource() == this.btnOk) {
            BeanUser b=new BeanUser();
            String userid = this.edtUserId.getText();
            String pwd1 = new String(this.edtPwd.getPassword());
            String pwd2 = new String(this.edtPwd2.getPassword());
            //String sex = this.edtSex.getText();
            String phone = this.edtPhone.getText();
            String city = this.edtCity.getText();
            String email = this.edtEmail.getText();

            b.setUser_city(city);
            b.setUser_email(email);
            b.setUser_password(pwd1);
            b.setUser_phone(phone);
            b.setUser_name(userid);

            if(this.man.isSelected()){
                b.setUser_sex("男");
            }else if(this.women.isSelected()){
                b.setUser_sex("女");
            }

            if(this.isVip.isSelected()){
                b.setUser_isvip("是");
            }else if(this.noVip.isSelected()){
                b.setUser_isvip("否");
            }

            try {
                BeanUser user = FreshStoreUtil.userManager.reg(b,pwd2);
                this.setVisible(false);
            } catch (BaseException e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage(), "错误", JOptionPane.ERROR_MESSAGE);
                return;
            }

        }
    }
}
